<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::any('/home', 'Site\ApiController@index');
Route::any('/category/{slug}', 'Site\ApiController@categoryList');
Route::any('/detail/{slug}', 'Site\ApiController@postDetail');
Route::any('/page/{slug}', 'Site\ApiController@pageDetail');
Route::any('/about-us', 'Site\ApiController@aboutUs');
Route::any('/team-members', 'Site\ApiController@Ourteam');
Route::any('/team-member/{slug}', 'Site\ApiController@teamDetail');
Route::any('/training-list', 'Site\ApiController@trainingList');
Route::any('/training/{slug}', 'Site\ApiController@trainingDetail');
Route::any('/contact-us','Site\ApiController@contactUs');
Route::any('/training-seminar', 'Site\ApiController@trainingSeminar');
Route::any('/postcontact', 'Site\ApiController@postContact');
Route::any('login', 'Site\ApiController@login');
Route::any('/newsletter', 'Site\ApiController@newsLetter');
Route::any('post-training-seminar', 'Site\ApiController@postTrainingEnrollment');
Route::any('logout', 'Site\ApiController@logout');
Route::any('register', 'Site\ApiController@register');
Route::any('forgotpassword', 'Site\ApiController@forgotPassword');
Route::any('otp', 'Site\ApiController@verifyOtp');
Route::any('resetpassword', 'Site\ApiController@resetPassword');
Route::any('updatepassword', 'Site\ApiController@updatePassword');

//Route::middleware('auth:api')->get('/user', function (Request $request) {
  //  return $request->user();
//});
