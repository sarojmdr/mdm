<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Site'], function (){
	Route::get('/','HomeController@index' )->name('index');
	Route::get('/service', 'HomeController@service')->name('service.list');
	Route::get('/service/{slug}', 'HomeController@serviceDetail')->name('service');
	Route::get('/projects', 'HomeController@project')->name('projects');
	Route::get('/project/all', 'HomeController@allProjectList')->name('all.project.list');
	Route::get('/project/{slug}', 'HomeController@projectDetail')->name('project.detail');
	Route::get('/posts', 'HomeController@newsList')->name('news.list');
	Route::get('/posts/{slug}', 'HomeController@newsDetail')->name('news.detail');
	Route::get('/category/{slug}', 'HomeController@newsCategoryList')->name('news.category');
	Route::get('/faq', 'HomeController@faqList')->name('faq');
	Route::get('/testimonial', 'HomeController@testimonialList')->name('testimonial');
	Route::get('/about-us', 'HomeController@aboutUs')->name('aboutus');
	Route::get('/contact-us', 'HomeController@contactUs')->name('contactus');
	Route::post('/post-contact-info', 'HomeController@postContactInfo')->name('post.contact_info');
	Route::post('/post-user-service', 'HomeController@postUserService')->name('post.user_service');
	Route::get('/teams', 'HomeController@teamList')->name('teams');
	Route::post('/newsletter', 'HomeController@newsLetter')->name('newsletter');
	Route::post('/search', 'HomeController@getSearchData')->name('searchdata');
	Route::get('/search', 'HomeController@getSearchDataPage')->name('searchdatapage');


	// unused
	Route::get('/page/{slug}', 'HomeController@pageDetail')->name('page.detail');

	Route::get('/content/{slug}', 'HomeController@contentDetail')->name('content.detail');
	Route::get('/client-profile/{slug}', 'HomeController@clientProfileDetail')->name('client_profile.detail');

	Route::get('/detail/{slug}', 'HomeController@postDetail')->name('post.detail');


	Route::get('/get-started', 'HomeController@getStarted')->name('getstarted');
	Route::post('/post-get-started', 'HomeController@postGetStarted')->name('post.get_started');
});






// Admin Web Route
Route::group(['prefix' => 'mdm-admin','namespace' => 'admin'], function (){
	Route::get('login', 'AdminLoginController@login')->name('login.page');
	Route::get('newsletter/{id}', 'AdminNewsletterController@show')->name('newsletterdetail');
	Route::post('login', 'AdminLoginController@loginCheck')->name('logincheck');
});

Route::group(['prefix' => 'u-admin', 'namespace' => 'admin', 'middleware'   => ['adminlogincheck','roles']], function (){
	Route::get('registeruser', 'AdminLoginController@userRegister')->name('user.create');
	Route::post('registeruser', 'AdminLoginController@userRegisterData')->name('userregister');
	Route::get('dashboard', 'AdminDashboardController@dashboard')->name('dashboard');
	Route::get('user/list', 'AdminLoginController@adminUserList')->name('user.list');
	Route::get('user/{id}/edit', 'AdminLoginController@editUser')->name('user.edit');
	Route::any('updateuser/{id}', 'AdminLoginController@updateuser')->name('user.update');
	Route::get('user/delete/{id}', ['as' => 'user.delete', 'uses' => 'AdminLoginController@deleteUser']);
	Route::any('logout', 'AdminLoginController@logout')->name('logout');

	Route::any('updateuserprofile/{id}', 'AdminProfileController@updateuser')->name('userprofile.update');
	Route::get('userprofile/{id}/edituserprofile', 'AdminProfileController@editUserProfile')->name('userprofile.editprofile');

	Route::get('success-login', 'AdminSiteSettingController@successLogin')->name('successlogin');
	Route::get('fail-login', 'AdminSiteSettingController@failLogin')->name('faillogin');
	Route::get('menu', 'AdminMenuController@index')->name('menu');

	// User Group
	Route::resource('usergroup', 'AdminGroupController');
	Route::get('usergroup/delete/{id}', ['as' => 'usergroup.delete', 'uses' => 'AdminGroupController@destroy']);

	// Role Access
	Route::resource('role-access', 'AdminRoleAccessController');
	Route::get('role-access/delete/{id}', ['as' => 'role-access.delete', 'uses' => 'AdminRoleAccessController@destroy']);
    Route::get('roleChangeAccess/{allowId}/{id}','AdminRoleAccessController@changeAccess');
    Route::get('setting','AdminSiteSettingController@setting')->name('setting');
    Route::post('setting-update','AdminSiteSettingController@updateSetting')->name('update.setting');

    // others
	Route::get('medialibrary', 'AdminDashboardController@mediaLibrary')->name('medialibrary');
	Route::any('ajax/drag-drop-sorting', 'AdminAjaxController@postDragDropSorting')->name('ajax.sorting');
	Route::any('module-url','AdminAjaxController@moduleUrl')->name('moduleUrl');


	// contra-admin

    // slider
	Route::resource('slider', 'AdminSliderController');
	Route::get('slider/delete/{id}', ['as' => 'slider.delete', 'uses' => 'AdminSliderController@destroy']);

	// Service
	Route::resource('service', 'AdminServiceController');
	Route::get('service/delete/{id}', ['as' => 'service.delete', 'uses' => 'AdminServiceController@destroy']);

    // Web Service
	Route::resource('web_services', 'AdminWebServiceController');
	Route::get('web_services/delete/{id}', ['as' => 'web_services.delete', 'uses' => 'AdminWebServiceController@destroy']);

    // Working Process
	Route::resource('working_process', 'AdminWorkingProcessController');
	Route::get('working_process/delete/{id}', ['as' => 'working_process.delete', 'uses' => 'AdminWorkingProcessController@destroy']);

	// Home Icon
	Route::resource('homeicon', 'AdminHomeIconController');
	Route::get('homeicon/delete/{id}', ['as' => 'homeicon.delete', 'uses' => 'AdminHomeIconController@destroy']);

	// about_content
	Route::resource('about_content', 'AboutContentController');
	Route::get('about_content/delete/{id}', ['as' => 'about_content.delete', 'uses' => 'AboutContentController@destroy']);

	// testimonial
	Route::resource('testimonial', 'TestimonialController');
	Route::get('testimonial/delete/{id}', ['as' => 'testimonial.delete', 'uses' => 'TestimonialController@destroy']);

    // client
	Route::resource('client', 'ClientController');
	Route::get('client/delete/{id}', ['as' => 'client.delete', 'uses' => 'ClientController@destroy']);

	// teams
	Route::resource('team', 'TeamController');
	Route::get('team/delete/{id}', ['as' => 'team.delete', 'uses' => 'TeamController@destroy']);

	// // service
	// Route::resource('service', 'ServiceController');
	// Route::get('service/delete/{id}', ['as' => 'service.delete', 'uses' => 'ServiceController@destroy']);

	//You should Know in Service
	Route::resource('service-wise-point', 'ServiceWisePointController');
	Route::get('service-wise-point/delete/{id}', ['as' => 'service-wise-point.delete', 'uses' => 'ServiceWisePointController@destroy']);

	// project category
	Route::resource('project-category', 'AdminProjectCategoryController');
	Route::get('project-category/delete/{id}', ['as' => 'project-category.delete', 'uses' => 'AdminProjectCategoryController@destroy']);

	// project
	Route::resource('project', 'AdminProjectController');
	Route::get('project/delete/{id}', ['as' => 'project.delete', 'uses' => 'AdminProjectController@destroy']);

	// author
	Route::resource('author', 'AdminAuthorController');
	Route::get('author/delete/{id}', ['as' => 'author.delete', 'uses' => 'AdminAuthorController@destroy']);

	// category
	Route::resource('category', 'AdminCategoryController');
	Route::get('category/delete/{id}', ['as' => 'category.delete', 'uses' => 'AdminCategoryController@destroy']);

	// posts
	Route::resource('posts', 'AdminPostsController');
	Route::get('posts/delete/{id}', ['as' => 'posts.delete', 'uses' => 'AdminPostsController@destroy']);






	// Partner
	Route::resource('partner', 'PartnerController');
	Route::get('partner/delete/{id}', ['as' => 'partner.delete', 'uses' => 'PartnerController@destroy']);






	// Contact
	Route::resource('contact', 'AdminContactController');
	Route::get('contact/delete/{id}', ['as' => 'contact.delete', 'uses' => 'AdminContactController@destroy']);

	// Pages
	Route::resource('pages', 'AdminPagesController');
	Route::get('pages/delete/{id}', ['as' => 'pages.delete', 'uses' => 'AdminPagesController@destroy']);

	// FAQ
	Route::resource('faq', 'AdminFaqController');
	Route::get('faq/delete/{id}', ['as' => 'faq.delete', 'uses' => 'AdminFaqController@destroy']);







	// upto here

	// newsletter
	// Route::get('newsletter/email-list', ['as' => 'newsletter.emaillist', 'uses' => 'AdminNewsletterController@emailList']);

	Route::get('newsletter/resendmail/{id}', ['as' => 'newsletter.resendmail', 'uses' => 'AdminNewsletterController@resendMail']);

	Route::get('newsletter/status-change/{id}', 'AdminNewsletterController@newsletterStatusUpdate');

	// newsletter subscription
	Route::resource('newsletter', 'AdminNewsletterController');
	Route::get('newsletter-subscription', 'AdminNewsletterController@subscriptionList')->name('newsletter.subscription');
	Route::get('newsletter-subscription/{id}', 'AdminNewsletterController@show')->name('newsletter-subscription.show');

	// use service
	Route::get('user-service', 'AdminUserServiceController@list')->name('user_service.index');
	Route::get('user-service/status-change/{id}', 'AdminUserServiceController@userServiceStatusUpdate');

});

