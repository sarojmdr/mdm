// cnit-js
$(document).ready(function () {
    var loc = window.location;
    if (loc.hash) {
        history.pushState({}, document.title, loc.pathname + loc.search);
    }

    // add user interested service

    $(".btn_save_contact").click(function (event) {
        $(".contact-sent-message").html("");
        event.preventDefault();
        $("#error_name").html("");
        // $('#error_subject').html('');
        $("#error_message").html("");
        $("#error_email").html("");
        $("#error_phoneno").html("");
        $("#error_address").html("");
        var formdata = $("#form_contact").serialize();
        console.log(formdata);
        var baseurl = $("#baseurl").val();
        var obj = $(this);

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: baseurl + "/post-user-service",
            type: "POST",
            data: formdata,
        }).always(function (resp) {
            console.log(resp);

            if (resp.error == true) {
                $(".contact-loading").addClass("d-none");
                if (resp.errors.name) {
                    $("#error_name").html(resp.errors.name[0]);
                }
                if (resp.errors.email) {
                    $("#error_email").html(resp.errors.email[0]);
                }
                //  if(resp.errors.subject){
                //   $('#error_subject').html(resp.errors.subject[0]);
                // }
                if (resp.errors.message) {
                    $("#error_message").html(resp.errors.message[0]);
                }
                if (resp.errors.phoneno) {
                    $("#error_phoneno").html(resp.errors.phoneno[0]);
                }
                if (resp.errors.address) {
                    $("#error_address").html(resp.errors.address[0]);
                }
            }
            if (resp.error == false) {
                window.location.href = resp.route_data;
            }
        });
    });
});

function onlyNumber(obj) {
    if (/\D/g.test(obj.value)) obj.value = obj.value.replace(/\D/g, "");
}

function newsletterEmail(e) {
    if (e && e.keyCode == 13) {
        event.preventDefault();
        $("#error_newsletter_email").html("");

        var email = $("#send-newsletter-email").val();
        var baseurl = $("#baseurl").val();

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: baseurl + "/newsletter",
            type: "POST",
            data: { email: email },
        }).always(function (resp) {
            console.log(resp);

            if (resp.error == true) {
                if (resp.errors) {
                    $("#error_newsletter_email").html(resp.errors);
                }
            }
            if (resp.error == false) {
                $("#email").val("");
                $("#success_newsletter").html(resp.message);
            }
        });
    }
}
