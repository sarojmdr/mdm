<?php

namespace App\Providers;

use App\Model\admin\AdminSetting;
use App\Model\site\Home;
use Harimayco\Menu\Facades\Menu;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // require_once __DIR__ . '/../Http/PrintHelper.php';
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('site.layouts.footer',function($view) {
            $footer_link = Home::getMenuList(12);


            // $menulist = array(
            //     'primarymenu'           => $primarymenu,
            //     'footermenu_1'          => $footermenu_1,
            //     'footermenu_2'          => $footermenu_2,
            // );
            $view->with('footer_link', $footer_link);



        });

         view()->composer('site.layouts.*',function($view) {
             $primarymenu =  AdminSetting::getPrimaryMenuListById(1);
                if (!empty($primarymenu)) {
                    foreach ($primarymenu as $k => $val) {
                        $child = AdminSetting::getChildMenuListById($val->id);
                        $primarymenu[$k]->child = $child;
                    }
                }
                // dd($primarymenu);
                $view->with('primarymenu', $primarymenu);
            });

            view()->composer('site.layouts.footer',function($view) {
                $footermenu =  AdminSetting::getPrimaryMenuListById(12);
                if (!empty($footermenu)) {
                    foreach ($footermenu as $k => $val) {
                        $child = AdminSetting::getChildMenuListById($val->id);
                        $footermenu[$k]->child = $child;
                    }
                }
                $view->with('footermenu', $footermenu);
            });

        // if($_SERVER['SERVER_NAME'] == 'cnit.com.np'  || $_SERVER['SERVER_NAME'] == 'www.cnit.com.np'){
        //     \URL::forceScheme('https');
        // }

        // if ($_SERVER['SERVER_NAME'] == 'cnit.com.np' || $_SERVER['SERVER_NAME'] == 'www.cnit.com.np' || $_SERVER['SERVER_NAME'] == 'dev.storepasal.com' || $_SERVER['SERVER_NAME'] == 'www.dev.storepasal.com') {
        //     URL::forceScheme('https');
        // }

        view()->composer('*',function($view) {
            $sitedetail = AdminSetting::findorfail('1');
            $view->with('sitedetail', $sitedetail);
        });
    }
}
