<?php

namespace App\Http\Controllers;

use App\Model\admin\AdminContact;
use App\Model\admin\GetStarted;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Mail;

class MailController extends Controller
{

   public static function sendNotifySellerMail($id) {
        $record = GetStarted::where('id',$id)->first();
        
        $data = array (
            'subject'               => 'New '.SITE_NAME.' Client',
            'record'                => $record,
        );
       
        Mail::send('email.send_notify_seller_mail', $data, function (Message $mail)use ($data) {
            $mail_from = env('MAIL_USERNAME', 'noreply.candidnepal@gmail.com');
            $mail->from($mail_from, SITE_NAME);
            $mail->to(MAIL_ADDRESS, MAIL_ADDRESS);
            $mail->subject($data['subject']);
            $mail->priority(3);
        });
        
    }

    public static function sendContactMail($id) {
        $record = AdminContact::where('id',$id)->first();
        
        $data = array (
            'subject'               => 'Contact From Client-'.SITE_NAME.'',
            'record'                => $record,
        );
       
        Mail::send('email.send_contact_from_client', $data, function (Message $mail)use ($data) {
            $mail_from = env('MAIL_USERNAME', 'noreply.candidnepal@gmail.com');
            $mail->from($mail_from, SITE_NAME);
            $mail->to(MAIL_ADDRESS, MAIL_ADDRESS);
            $mail->subject($data['subject']);
            $mail->priority(3);
        });
        // nivaj@cnit.com.np
        
    }
}