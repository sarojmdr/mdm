<?php

namespace App\Http\Controllers\Site;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MailController;
use App\Model\admin\AdminContact;
use App\Model\admin\AdminSetting;
use App\Model\admin\NewsLetter;
use App\Model\admin\TrainingParticipant;
use App\Model\site\Home;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ApiController extends Controller {

    public function index(){
        try {
            $sliderdata = [];
            $servicelistdata = [];
            $service_list_detail = [];
            $newslistdata = [];
            $news_list_detail = [];
            $insightslist_data = [];
            $insights_list_detail = [];
            $publicationlist_data = [];
            $publicationlist_detail = [];
            $partnerlist_data = [];
            $slider = Home::getSliderData();
            if (!empty($slider)) {
                foreach ($slider as $k => $val) {
                    $sliderdata[] = array(
                        'title'     => $val->title,
                        'image'     => getImageForMobileApp($val->image),
                    );
                }
            }

            $showhomepage = Home::getShowOnHomePage();
            if(!empty($showhomepage)){
                $showhomepage->description = strip_tags($showhomepage->description);
                $showhomepage->image = getImageForMobileApp($showhomepage->image);
            }
            $servicelist = Home::getCategoryDetailById(11);
            if (!empty($servicelist)) {
                $list = Home::getPostListByCategoryId($servicelist->id,$limit=4);
                if(!empty($list)){
                    foreach ($list as $k => $val) {
                        $content = str_replace('&nbsp;', '', str_replace('&rsquo;', "'", $val->description));
                        $info = preg_replace('/\r\n/i', '', strip_tags(htmlspecialchars_decode($content)));
                        $servicelistdata[] = array(
                            'title'          => $val->title, 
                            'description'    =>  $info, 
                            'slug'           => $val->slug, 
                            'published_date' => $val->published_date,
                            'image'          => getImageForMobileApp($val->image), 
                            'login_check'   => 0,
                        );
                    }
                }
                $service_list_detail[] = array(
                    'title'     => $servicelist->title, 
                    'sub_title' => $servicelist->sub_title, 
                    'slug'      => $servicelist->slug, 
                    'list'      => $servicelistdata, 
                );
            }

            $newslist = Home::getCategoryDetailById(10);
            if (!empty($newslist)) {
                $list = Home::getPostListByCategoryId($newslist->id,$limit=6);
                if(!empty($list)){
                    foreach ($list as $k => $val) {
                        $content = str_replace('&nbsp;', '', str_replace('&rsquo;', "'", $val->description));
                        $info =  preg_replace('/\r\n/i', '', strip_tags(htmlspecialchars_decode($content)));
                        $newslistdata[] = array(
                            'title'         => $val->title, 
                            'description'   => $info, 
                            'slug'          => $val->slug, 
                            'published_date' => $val->published_date,
                            'image'         => getImageForMobileApp($val->image), 
                            'login_check'   => 0,
                        );
                    }
                }
                $news_list_detail[] = array(
                    'title'     => $newslist->title, 
                    'sub_title' => $newslist->sub_title, 
                    'slug'      => $newslist->slug, 
                    'list'      => $newslistdata, 
                );
            }

            $insightslist = Home::getCategoryDetailById(13);
            if (!empty($insightslist)) {
                $list = Home::getPostListByCategoryId($insightslist->id,$limit=6);
                if(!empty($list)){
                    foreach ($list as $k => $val) {
                        $content = str_replace('&nbsp;', '', str_replace('&rsquo;', "'", $val->description));
                        $info =  preg_replace('/\r\n/i', '', strip_tags(htmlspecialchars_decode($content)));
                        $insightslist_data[] = array(
                            'title'         => $val->title, 
                            'description'   => $info, 
                            'slug'          => $val->slug, 
                            'published_date' => $val->published_date,
                            'image'         => getImageForMobileApp($val->image), 
                            'login_check'   => 0,
                        );
                    }
                }
                $insights_list_detail[] = array(
                    'title'     => $insightslist->title, 
                    'sub_title' => $insightslist->sub_title, 
                    'slug'      => $insightslist->slug, 
                    'list'      => $insightslist_data, 
                );
            }

            $publicationlist = Home::getCategoryDetailById(16);
            if(!empty($publicationlist)){
                $list = Home::getPostListByCategoryId($publicationlist->id, $limit=6);
                if(!empty($list)){
                    foreach ($list as $k => $val) {
                        $content = str_replace('&nbsp;', '', str_replace('&rsquo;', "'", $val->description));
                        $info =  preg_replace('/\r\n/i', '', strip_tags(htmlspecialchars_decode($content)));
                        $publicationlist_data[] = array(
                            'title'         => $val->title, 
                            'description'   => $info, 
                            'slug'          => $val->slug, 
                            'published_date' => $val->published_date,
                            'image'         => getImageForMobileApp($val->image), 
                            'login_check'   => 1,
                        );
                    }
                }
                $publicationlist_detail[] = array(
                    'title'     => $publicationlist->title, 
                    'sub_title' => $publicationlist->sub_title, 
                    'slug'      => $publicationlist->slug, 
                    'list'      => $publicationlist_data, 
                );
            }

            $partnerlist = Home::getPartnerList('2'); // 2 for client
            if(!empty($partnerlist)){
                foreach ($partnerlist as $k => $val) {
                    $partnerlist_data[] = array(
                                                'title' => $val->title, 
                                                'image' => getImageForMobileApp($val->image), 
                                            );
                }
            }
            $popupimage = Home::getPopUpImage();

            $result = array(
                'slider'                    => $sliderdata,
                'showhomepage'              => $showhomepage,
                'service_list_detail'       => $service_list_detail,
                'news_list_detail'          => $news_list_detail,
                'insights_list_detail'      => $insights_list_detail,
                'publicationlist_detail'    => $publicationlist_detail,
                'partnerlist_data'          => $partnerlist_data,
                'popupimage'                => $popupimage,
            );

            return responseData(S,DATA_FETCHED,200,$result);     
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }

       
       
    }

    public function categoryList($slug){
        try {
            $category = Home::getCategoryDetail($slug);
            $category_list = [];
            if (!empty($category)) {
                $list = Home::getCategoryList($category->id);
                if($category->id == 16){
                    $login_check = 1;
                }else{
                    $login_check = 0;
                }
                foreach ($list as $k => $val) {
                    // $content = str_replace('&nbsp;', '', str_replace('&rsquo;', "'", $val->description));
                    // $info =  preg_replace('/\r\n/i', '', strip_tags(htmlspecialchars_decode($content)));
                    $category_list[] = array(
                        'title'         => $val->title, 
                        'image'         => getImageForMobileApp($val->image), 
                        'slug'          => $val->slug, 
                        'published_date' => $val->published_date, 
                        'description'   => $val->description, 
                        'login_check'   => $login_check,
                    );
                }

                $category_data = array(
                    'title' => $category->title, 
                    'show_date' => $category->show_date, 
                    'list' => $category_list, 
                );
                $result = array(
                    'category_data'        => $category_data,
                );
                return responseData(S,DATA_FETCHED,200,$result);     
            } else {
                return responseData(F,SYSTEM_ERROR,422);  
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function postDetail($slug){
        try {
            $relatedpost = array();
            $recentpost = array();
            $data = Home::getPostDetail($slug);

            if (!empty($data)) {
                $detail['title'] = $data->title;
                // $content = str_replace('&nbsp;', '', str_replace('&rsquo;', "'", $data->description));
                // $info =  preg_replace('/\r\n/i', '', strip_tags(htmlspecialchars_decode($content)));
                $detail['description'] = $data->description;
                $detail['image'] = getImageForMobileApp($data->image);
                $detail['scan_pay'] = getImageForMobileApp($data->scan_pay);
                $detail['published_date'] = $data->published_date;
                $category = Home::getCategoryIdPostId($data->id);
                Home::updatePostsViewCount($data->id);
                if (!empty($category)){
                    $related_list = Home::getRelatedPostListByCategoryId($category->id,$data->id,5);
                    foreach ($related_list as $k => $val) {
                        $relatedpost[] = array(
                        'title' => $val->title, 
                        'slug' => $val->slug, 
                        'published_date' => $val->published_date, 
                        'show_date' => $category->show_date,
                        );
                    }
                }
                $recent_list = Home::getRecentPost($limit=5);
                foreach ($recent_list as $k => $val) {
                    $recentpost[] = array(
                        'title' => $val->title, 
                        'slug' => $val->slug, 
                        'published_date' => $val->published_date, 
                    );
                }
                if($category->slug == 'research'){
                    $login_check = 1;
                }else{
                    $login_check = 0;
                }
                $result = array(
                    'detail'        => $detail,
                    'relatedpost'   => $relatedpost,
                    'recentpost'    => $recentpost,
                    '$login_check'  => $login_check,
                );
                return responseData(S,DATA_FETCHED,200,$result);   
            }else{
                return responseData(F,SYSTEM_ERROR,422);  
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
        
    }

    public function pageDetail($slug){
        try {
            $detail = array();
            $data = Home::getPageDetail($slug);
            if (!empty($data)) {
                // Home::updatePageViewCount($data->id);
                $detail['title'] = $data->title;
                $detail['description'] = $data->description;
                $detail['image'] = getImageForMobileApp($data->image);
                $detail['published_date'] = $data->published_date;

                $result = array(
                    'detail'        => $detail,
                );
                return responseData(S,DATA_FETCHED,200,$result);
            } else{
                return responseData(F,SYSTEM_ERROR,422); 
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function aboutUs(){
        try {
            $showhomepage = Home::getPageDetail('about-us');
            $show_page_data['title'] = $showhomepage->title;
            $show_page_data['image'] = getImageForMobileApp($showhomepage->image);
            $show_page_data['description'] = removeTagsForDescription($showhomepage->description);
            $showhomepage_1 = Home::getPageDetail('who-we-are');
            $show_page_data_1['title'] = $showhomepage_1->title;
            $show_page_data_1['image'] = getImageForMobileApp($showhomepage_1->image);
            $show_page_data_1['description'] = removeTagsForDescription($showhomepage_1->description);
            $showhomepage_2 = Home::getPageDetail('how-we-work');
            $show_page_data_2['title'] = $showhomepage_2->title;
            $show_page_data_2['image'] = getImageForMobileApp($showhomepage_2->image);
            $show_page_data_2['description'] = removeTagsForDescription($showhomepage_2->description);
            $partnerlist = Home::getPartnerList('1'); // 1 for partners
            $partner_data = [];
            if(!empty($partnerlist)){
                foreach ($partnerlist as $k => $val) {
                    $partner_data[] = array(
                        'title' => $val->title, 
                        'image' => getImageForMobileApp($val->image), 
                    );
                }
            }
            $valueslist = Home::getOurCoreValues();
            $values_data = [];
            if(!empty($valueslist)){
                foreach ($valueslist as $k => $val) {
                    $values_data[] = array(
                        'title' => $val->title, 
                        'description' => removeTagsForDescription($val->description), 
                    );
                }
            }

            $result = array(
                'show_page_data'          => $show_page_data,
                'show_page_data_1'        => $show_page_data_1,
                'show_page_data_2'        => $show_page_data_2,
                'partner_data'           => $partner_data,
                'values_data'            => $values_data,
            );
            return responseData(S,DATA_FETCHED,200,$result);
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function ourTeam(){
        try {
            $categorylist = Home::getStaffCategory();
            $list = [];
            if (!empty($categorylist)) {
                foreach ($categorylist as $k => $val) {
                    $staff_array = [];
                    $stafflist = Home::getOurTeamListByCategoryId($val->id);
                    foreach ($stafflist as $k => $item) {
                        $staff_array[] = array(
                            'slug'          =>  $item->slug, 
                            'name'          =>  $item->name, 
                            'designation'   =>  $item->designation,
                            'image'         =>  getImageForMobileApp($item->image),
                        );
                    }
                    $list[] = array(
                        'title'     =>  $val->title, 
                        'stafflist' =>  $staff_array,
                    );
                }
            }
            // return $list;
            return $list;
            $result = array(
                'list'          => $categorylist,
            );
            return responseData(S,DATA_FETCHED,200,$result);
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
        
    }

    public function teamDetail($slug){
        try {
            $detail = Home::getTeamDetail($slug);
            if (!empty($detail)) {
                $data = [];
                $data['name'] = $detail->name;
                $data['image'] = getImageForMobileApp($detail->image);
                $data['designation'] = $detail->designation;
                $data['email'] = $detail->email;
                $data['phone'] = $detail->phone;
                $data['info'] = $detail->info;
                $staff_array = [];
                $stafflist = Home::getOurTeamListByCategoryId(1);
                foreach ($stafflist as $k => $item) {
                    $staff_array[] = array(
                        'slug'          =>  $item->slug, 
                        'name'          =>  $item->name, 
                    );
                }
                $result = array(
                    'detail'          => $data,
                    'stafflist'       => $staff_array,
                );
                return responseData(S,DATA_FETCHED,200,$result);
            }else{
                return responseData(F,SYSTEM_ERROR,422);  
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
        
    }

    public function trainingList(){
        try {
            $list = Home::getTrainingList();
            $array = [];
            foreach ($list as $k => $val) {
                $array[] = array(
                    'title' => $val->title, 
                    'image' => getImageForMobileApp($val->image), 
                    'slug' => $val->slug, 
                    'inserted_date' => $val->inserted_date, 
                    'description' => removeTagsForDescription($val->description), 
                );
            }
            $result = array(
                'list' => $array,
                );
            return responseData(S,DATA_FETCHED,200,$result);
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }   
    }

    public function trainingDetail($slug){
        try {
            $detail = Home::getTrainingDetail($slug);
            if (!empty($detail)) {
                $detail_array = [];
                $detail_array['title'] = $detail->title;
                $detail_array['program_detail'] = $detail->program_detail;
                $detail_array['image'] = getImageForMobileApp($detail->image);
                $detail_array['scan_pay'] = getImageForMobileApp($detail->scan_pay);
                $detail_array['description'] = $detail->description;
                $resourceperson = Home::getResourcePersonListById($detail->id);
                $resourceperson_array = [];
                foreach ($resourceperson as $k => $val) {
                    $resourceperson_array[] = array(
                        'title' => $val->title, 
                        'image' => getImageForMobileApp($val->image), 
                        'company_name' => $val->company_name, 
                        'position' => $val->position, 
                        'description' => removeTagsForDescription($val->description), 
                    );
                }
                $cordinator = Home::getCoordinatorListById($detail->id);
                foreach ($cordinator as $k => $val) {
                    $val->image = getImageForMobileApp($val->image);
                    $val->description = removeTagsForDescription($val->description);
                }
                $upcominglist = Home::getUpcomingTrainingList();
                $pasttrainingist = Home::getPastTrainingList();

                $result = array(
                    'detail'            => $detail_array,
                    'resourceperson'    => $resourceperson_array,
                    'cordinator'        => $cordinator,
                    'upcominglist'      => $upcominglist,
                    'pasttrainingist'   => $pasttrainingist,
                );
            return responseData(S,DATA_FETCHED,200,$result);
            }else{
                return responseData(F,SYSTEM_ERROR,422);  
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function trainingSeminar(){
        try {
            $detail = Home::getTrainingSeminar();
            if (!empty($detail)) {
                $detail_array = [];
                $detail_array['title'] = $detail->title;
                $detail_array['program_detail'] = $detail->program_detail;
                $detail_array['image'] = getImageForMobileApp($detail->image);
                $detail_array['scan_pay'] = getImageForMobileApp($detail->scan_pay);
                $detail_array['description'] = $detail->description;
                $resourceperson = Home::getResourcePersonListById($detail->id);
                $resourceperson_array = [];
                foreach ($resourceperson as $k => $val) {
                    $resourceperson_array[] = array(
                        'title' => $val->title, 
                        'image' => getImageForMobileApp($val->image), 
                        'company_name' => $val->company_name, 
                        'position' => $val->position, 
                        'description' => removeTagsForDescription($val->description), 
                    );
                }
                $cordinator = Home::getCoordinatorListById($detail->id);
                foreach ($cordinator as $k => $val) {
                    $val->image = getImageForMobileApp($val->image);
                    $val->description = removeTagsForDescription($val->description);
                }
                $upcominglist = Home::getUpcomingTrainingList();
                $pasttrainingist = Home::getPastTrainingList();

                $result = array(
                    'detail'            => $detail_array,
                    'resourceperson'    => $resourceperson_array,
                    'cordinator'        => $cordinator,
                    'upcominglist'      => $upcominglist,
                    'pasttrainingist'   => $pasttrainingist,
                );
                return responseData(S,DATA_FETCHED,200,$result);
            }else{
                $list = Home::getTrainingList();
                if (!empty($list)) {
                    $detail = $list[0];
                    $detail_array = [];
                    $detail_array['title'] = $detail->title;
                    $detail_array['program_detail'] = $detail->program_detail;
                    $detail_array['image'] = getImageForMobileApp($detail->image);
                    $detail_array['scan_pay'] = getImageForMobileApp($detail->scan_pay);
                    $detail_array['description'] = $detail->description;
                    $resourceperson = Home::getResourcePersonListById($detail->id);
                    $cordinator = Home::getCoordinatorListById($detail->id);
                    $upcominglist = Home::getUpcomingTrainingList();
                    $pasttrainingist = Home::getPastTrainingList();

                    $resourceperson_array = [];
                    foreach ($resourceperson as $k => $val) {
                        $resourceperson_array[] = array(
                            'title' => $val->title, 
                            'image' => getImageForMobileApp($val->image), 
                            'company_name' => $val->company_name, 
                            'position' => $val->position, 
                            'description' => removeTagsForDescription($val->description), 
                        );
                    }
                    foreach ($cordinator as $k => $val) {
                        $val->image = getImageForMobileApp($val->image);
                        $val->description = removeTagsForDescription($val->description);
                    }
                    $upcomingarray = [];
                    foreach ($upcominglist as $k => $val) {
                        $upcomingarray[] = array(
                            'slug' => $val->slug, 
                            'title' => $val->title, 
                        );
                    }
                    $result = array(
                        'detail'            => $detail_array,
                        'resourceperson'    => $resourceperson_array,
                        'cordinator'        => $cordinator,
                        'upcominglist'      => $upcomingarray,
                        'pasttrainingist'   => $pasttrainingist,
                    );
                    return responseData(S,DATA_FETCHED,200,$result);
                }else{
                    return view('errors.404');
                }

            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function contactUs(){
        try {
            $sitedetail = AdminSetting::select('contact_us_txt', 'address', 'phone_no', 'mobile_no', 'email', 'facebook', 'twitter', 'linkedin', 'instagram', 'map_detail')->get();
            $data = array(
                'sitedetail' => $sitedetail,
            );
            $result = array(
                'data'   => $data,
            ); 
            return responseData(S,DATA_FETCHED,200,$result);
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function postContact(Request $request){
        try {
            if (!empty($request->all())) {
                $validator = Validator::make($request->all(),[
                    'name'          => 'required',
                    'address'       => 'required',
                    'email'         => 'required',
                    'subject'       => 'required',
                    'message'       => 'required',
                    'phoneno'       => 'required',
                ]);
                if ($validator->passes()) {
                    $crud = new AdminContact;
                    $crud->name = $request->name;
                    $crud->email = $request->email;
                    $crud->address = $request->address;
                    $crud->phoneno = $request->phoneno;
                    $crud->ip_address = $request->ip();
                    $crud->message = $request->message;
                    $crud->inserted_date = date('Y-m-d H:i:s');
                    $crud->viewed = '0';
                    $crud->status = '1';
                    $crud->save();
                    // Store your user in database
                    $result = array(
                        'error'     => false,
                        'message'   => "Thank You for Contacting Us !!!",
                    );

                    return responseData(S,DATA_FETCHED,200,$result);
                }else{
                    $result = array(
                        'error'     => true,
                        'errors'    => $validator->errors()
                    );
                return responseData(F,SYSTEM_ERROR,422,$result);  
                }
            }else{
                $result = array(
                    'error'     => true,
                    'errors'    => 'Unauthorized Access',
                );
                return responseData(F,SYSTEM_ERROR,422,$result);  
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function login(Request $request)
    {
        try {
            if(!empty($request->all())){
                $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required|min:6',
                ]);
                if($validator->passes()){
                    $user = User::where('email', $request->email)->first();
                    if($user){
                        if(Hash::check($request->password, $user->password)){
                            if($user->status == 1){
                                // set api_token 
                                if ($user->api_token != '') {
                                    $apitoken = $user->api_token; 
                                }else{
                                    $apitoken = getApiToken($user->id);
                                }
                                User::find($user->id)->update([
                                    'api_token' => $apitoken,
                                ]);
                                $userdata = User::where('id', $user->id)
                                                ->select('id','name','email','mobileno','address')
                                                ->get();
                                $result = array(
                                    'api_token' => $apitoken, 
                                    'user'      => $userdata, 
                                    'message'   => 'Logged in Successfully', 
                                );
                                return responseData(S,DATA_FETCHED,200,$result);
                            }else{
                                $error = 'Your account has not been activated. Please check your mail.';
                                MailController::sendEmailVerification($user);
                                return responseData(F,$error, 422);
                            }
                        }else{
                            return responseData(F,'Login Credential, Incorrect !!!',422);
                        }
                    }else{
                        return responseData(F,'Login Credential, Incorrect !!!',422);
                    }
                }else{
                        $result = array(
                            'error'     => true,
                            'errors'    => $validator->errors()
                        );
                    return responseData(F,'Login Credential, Incorrect !!!',422,$result); 
                }
            }else{
                $result = array(
                    'error'     => true,
                    'errors'    => 'Unauthorized Access',
                );
                return responseData(F,'Unauthorized Access',422,$result);  
            }  
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function newsLetter(Request $request){
        try {
            if (!empty($request->all())) {
                $validator = Validator::make($request->all(),
                    [
                        'newslettermail'             => 'required|email|unique:tbl_newsletter_list,email',
                    ],
                    [
                        'newslettermail.unique'       => 'Email has already been registered',
                    ]
                );
                if ($validator->passes()) {
                    $newsletter = new NewsLetter;
                    $newsletter->email = $request->newslettermail;
                    $newsletter->status = '1';
                    $newsletter->save();
                    // Store your user in database
                    $result = array(
                        'error'     => false,
                        'message'   => "We've received your email. Thank You !!!",
                    );

                    return responseData(S,DATA_FETCHED,200,$result);
                }else{
                    $result = array(
                        'error'     => true,
                        'errors'    => $validator->errors()
                    );
                return responseData(F,'Input Required !!!',422, $result);
                }
            }else{
                $result = array(
                    'error'     => true,
                    'errors'    => 'Unauthorized Access',
                );
                return responseData(F,'Unauthorized Access !!!',422, $result);
            }   
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function postTrainingEnrollment(Request $request){
        try {
            if (!empty($request->all())) {
                $validator = Validator::make($request->all(),[
                    'fullname'          => 'required',
                    'emailaddress'      => 'required|email',
                    'mobileno'          => 'required',
                    'message_data'      => 'required',
                    'training_id'       => 'required',
                ]);
                if ($validator->passes()) {
                    $crud = new TrainingParticipant;
                    $crud->fullname = $request->fullname;
                    $crud->emailaddress = $request->emailaddress;
                    $crud->mobileno = $request->mobileno;
                    $crud->training_id = $request->training_id;
                    $crud->ip_address = $request->ip();
                    $crud->message_data = $request->message_data;
                    $crud->requested_date = date('Y-m-d H:i:s');
                    $crud->viewed = '0';
                    $crud->status = '1';
                    $crud->save();
                    // Store your user in database

                    // send mail as well

                    $result = array(
                        'error'     => false,
                        'message'   => "Thank You for Participating !!!",
                    );

                    return responseData(S,DATA_FETCHED,200,$result);
                }else{
                    $result = array(
                        'error'     => true,
                        'errors'    => $validator->errors()
                    );
                    return responseData(F,'Input Required',422, $result);
                }
            }else{
                $result = array(
                    'error'     => true,
                    'errors'    => 'Unauthorized Access',
                );
                return responseData(F,'Unauthorized Access',422, $result);
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());  
        }
    }

    public function register(Request $request){
        try {
            if(!empty($request->all())){
                $validator = Validator::make($request->all(), [
                            'name' => ['required', 'string', 'max:255'],
                            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                            'mobileno' => ['required', 'unique:users'],
                            'address' => ['required'],
                            'password' => ['required', 'min:6', 'confirmed'],
                            'terms_of_service' => ['required'],
                        ]);
                if($validator->passes()){
                    $user = User::create([
                        'name' => $request->name,
                        'email' => $request->email,
                        'mobileno' => $request->mobileno,
                        'address' => $request->address,
                        'password' => Hash::make($request->password),
                        'verify_token' => Str::random(40),
                    ]);
                    MailController::sendEmailVerification($user);
                    $userdata = User::where('id', $user->id)
                                                ->select('id','name','email','mobileno','address')
                                                ->get();
                    $result = array(
                        'message'   => EMAIL_VERIFICATION,
                        'user'      => $userdata, 
                    );
                    return responseData(S,DATA_FETCHED,200,$result);
                }else{
                    $result = array(
                        'error' => true, 
                        'errors' => $validator->errors()
                    );
                    return responseData(F,'Input Data Incorrect !!!',422,$result);
                }
            }else{
                return responseData(F,SYSTEM_ERROR,422);
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());
        }
    }

    public function verifyOtp(Request $request){
        try {
            if($request->remember_token != ''){
                $user = User::where('remember_token', $request->remember_token)->select('id', 'name', 'email', 'mobileno','address','otp_code')->first();
                if($user){
                    if($request->otp_code == $user->otp_code){
                        $result = array(
                            'user'          => $user,
                            'message'  => 'OTP verified', 
                        );
                        return responseData(S,DATA_FETCHED,200,$result);
                    }else{
                        return responseData(F,'Entered Incorrect OTP !!!',422);
                    }
                }else{
                    return responseData(F,'Invalid Token',422);
                }
            }else{                
                return responseData(F,SYSTEM_ERROR,422);
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());
        }
    }

    public function resetPassword(Request $request){
        try {
            if($request->remember_token != ''){
                $validator = Validator::make($request->all(), [
                    'password' => 'required|confirmed|min:6',
                ]);
                if($validator->passes()){
                    $user = User::where('remember_token', $request->remember_token)->first();
                    if($user){
                        User::findOrFail($user->id)->update([
                            'remember_token' => NULL,
                            'otp_code' => NULL,
                            'password' => Hash::make($request->password),
                        ]); 
                        $userdata = User::where('id', $user->id)
                                        ->select('id','name','email','mobileno','address')
                                        ->get();
                        $result = array(
                            'user'          => $userdata,
                            'message'       => 'Your password has been reset. Please Login with the new Password',
                        );
                        return responseData(S,DATA_FETCHED,200,$result);
                    }else{
                        return responseData(F,'Invalid Token',422);
                    }
                }else{
                     $result = array(
                        'error' => true, 
                        'errors' => $validator->errors()
                    );
                    return responseData(F,'Input Data Incorrect !!!',422,$result);
                }
            }else{                
                return responseData(F,SYSTEM_ERROR,422);
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());
        }
    }

    public function forgotPassword(Request $request){
        try {
            if(!empty($request->all())){
                $validator = Validator::make($request->all(), [
                            'email' => ['required', 'string', 'email', 'max:255'],
                        ]);
                if($validator->passes()){

                    $user = User::where('email', $request->email)->select('id','name','email','mobileno','address')->first();
                    if($user){
                        $token = Str::random(40);
                        $user->fill([
                            'remember_token' => $token,
                        ])->save(); 
                        MailController::sendOTP($user->id);
                        $result = array(
                            'message'           => 'We have e-mailed your OTP code!',
                            'user'              => $user,
                            'remember_token'    => $token,
                        );
                        return responseData(S,DATA_FETCHED,200,$result);
                    }else{
                        return responseData(F,'We can\'t find a user with that e-mail address.',422);
                    }
                }else{
                    $result = array(
                        'error' => true, 
                        'errors' => $validator->errors()
                    );
                    return responseData(F,'Credentials Incorrect !!!',422,$result);
                }
            }else{
                return responseData(F,SYSTEM_ERROR,422);
            }
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());
        }
    }

    public function logout(Request $request){
        try {
            if($request->api_token != ''){
                $api_token = $request->api_token;
                $user = User::where('api_token',$api_token)->first();
                if($user){
                    User::where('id', $user->id)->update(['api_token' => null]);
                    $result = array(
                        'message'   => 'Logged Out Successfully',
                    );
                    return responseData(S, 'Logged Out Successfully', 200, $result);
                }else{
                    return responseData(F, 'Unauthorized access',401);
                }
            }else{
                return responseData(F,SYSTEM_ERROR,422);
            }  
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());
        }
    }  

    public function updatePassword(Request $request){
        try {
            if(!empty($request->all())){
                $validator = Validator::make($request->all(), [
                    'old_password' => 'required|min:6',
                    'password' => 'required|confirmed|min:6|different:old_password',
                ]);
                    $api_token = $request->api_token;
                    $user = User::where('api_token', $api_token)->first();
                    if($user){
                        if($validator->passes()){
                            if(Hash::check($request->old_password, $user->password)){
                                $user->fill([
                                    'password' => bcrypt($request->password),
                                ])->save();
                                $userdata = User::where('id', $user->id)
                                                ->select('id','name','email','mobileno','address')
                                                ->get();
                                $result = array(
                                    'message' => 'Your password has been changed.', 
                                    'user' => $userdata
                                );
                                return responseData(S, DATA_FETCHED, 200, $result);
                            }else{
                                return responseData(F,'Your old password does not match with our records.',422);
                            }
                        }else{
                            $result = array(
                                'error' => true, 
                                'errors' => $validator->errors()
                            );
                            return responseData(F,'Credentials Incorrect !!!',422,$result);
                        }
                    }else{
                        return responseData(F,'Unauthorized access.',401);
                    }
            }else{
                return responseData(F,SYSTEM_ERROR,422);
            }  
        } catch (Exception $e) {
            return responseData(F,SYSTEM_ERROR,409,$e->getMessage());
        }
    }   
}