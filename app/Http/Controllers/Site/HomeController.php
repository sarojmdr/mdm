<?php

namespace App\Http\Controllers\Site;
use App\Http\Controllers\Controller;
use App\Model\admin\AdminContact;
use App\Model\admin\AdminPosts;
use App\Model\admin\AdminProject;
use App\Model\admin\AdminService;
use App\Model\admin\AdminServiceWisePoint;
use App\Model\admin\AdminUserService;
use App\Model\admin\NewsLetter;
use App\Model\site\Home;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PrintHelper;
use Session;

class HomeController extends Controller {

    public function index(){

        $home_icons = Home::getHomeIcon();
        $working_process = Home::getWorkingProcessList();
        $services = Home::getServiceList();
        $web_services = Home::getWebServiceList();
        $testimonials = Home::getTestimonialList();
        $blogs = Home::getPostList($limit=3);
        $projects = Home::getProjectDetail($limit=3);
        // $left_services = Home::getServiceDetail($limit=3, $offset=0);
        // $right_services = Home::getServiceDetail($limit=3, $offset=3);
        // $clients = Home::getClientDetail($limit=6);
        // $sliders = Home::getSliderDetail();
        // $project_categories = Home::getProjectCategory();

        // view count
        Home::incrementViewCount();



        $result = array(
            'page_header'               => 'Home',
            'home_icons'                => $home_icons,
            'working_process'           => $working_process,
            'web_services'              => $web_services,
            'testimonials'              => $testimonials,
            'blogs'                      => $blogs,
            'services'                  => $services,
            'projects'                  => $projects,
            // 'sliders'                   => $sliders,
            // 'left_services'             => $left_services,
            // 'right_services'            => $right_services,
            // 'clients'                   => $clients,
            // 'project_categories'        => $project_categories,
        );
        return view('site.home',$result);
    }

    // service detail page
    public function serviceList(){

        $service_list = Home::getServiceDetail($limit=0);

        if (!empty($service_list) > 0) {
            $result = array(
                'service_list'        => $service_list,
            );
            return view('site.single.servicelist', $result);
        } else{
            return view('errors.404');
        }
    }

    public function serviceDetail($slug){
        $service_list = Home::getServiceDetail($limit=0);
        $detail = AdminService::where('slug', $slug)->first();
        Home::updateServiceViewCount($detail->id);

        if (!empty($detail)) {
            $service_wise_point = AdminServiceWisePoint::where('service_id', $detail->id)->get();
            $result = array(
                'detail'                => $detail,
                'service_list'          => $service_list,
                'service_wise_point'    => $service_wise_point,
            );
            return view('site.single.servicedetail', $result);
        } else{
            return view('errors.404');
        }
    }

    public function project(){

        $projects = Home::getProjectDetail();
        $working_process = Home::getWorkingProcessList();

        if (!empty($projects)) {
            $result = array(
                'page_header'           => 'Projects',
                'projects'              => $projects,
                'working_process'       => $working_process,

            );
            return view('site.single.project', $result);
        } else{
            return view('errors.404');
        }
    }

    public function allProjectList(){

        $project_list = Home::getAllProjectDetail();

        if (!empty($project_list) > 0) {
            $project_categories = Home::getProjectCategory();
            $result = array(
                'project_list'              => $project_list,
                'project_categories'        => $project_categories,
            );
            return view('site.single.projectlist', $result);
        } else{
            return view('errors.404');
        }
    }

    public function projectDetail($slug){
        $detail = AdminProject::where('slug', $slug)
                    ->first();

        if (!empty($detail)) {
            $result = array(
                'detail'                => $detail,
            );
            return view('site.single.projectdetail', $result);
        } else{
            return view('errors.404');
        }
    }

    public function newsList(Request $request){
        $category_slug = isset($_GET['list']) ? $_GET['list'] : '';


        $blog_list = Home::getPostDetail($category_slug);

        if (!empty($blog_list)) {
            $recent_post = Home::getPostList($limit=5);
            $category = Home::getPostCategory();

            $result = array(
                'blog_list'                 => $blog_list,
                'recent_post'               => $recent_post,
                'category'                  => $category,
            );
            return view('site.single.bloglist', $result);
        } else{
            return view('errors.404');
        }
    }

    public function newsCategoryList($category_slug){


        $blog_list = Home::getPostDetailWithSlug($category_slug);

        if (!empty($blog_list)) {
            $recent_post = Home::getPostList($limit=5);
            $category = Home::getPostCategory();

            $result = array(
                'blog_list'                 => $blog_list,
                'recent_post'               => $recent_post,
                'category'                  => $category,
            );
            return view('site.single.bloglist', $result);
        } else{
            return view('errors.404');
        }
    }

    public function newsDetail($slug){

        $detail = Home::getPostDetailInfo($slug);
        if (!empty($detail)) {
            $recent_post = Home::getPostList($limit=5);
            $category = Home::getPostCategory();
            Home::updatePostsViewCount($detail->id);

            $result = array(
                'detail'        => $detail,
                'category'      => $category,
                'recent_post'   => $recent_post,
            );


            return view('site.single.blogdetail', $result);

        }else{
            return view('errors.404');
        }
    }



    public function faqList(){
        $list = Home::getFaqList();
        $recent_post = Home::getPostList($limit=5);
        $result = array(
            'page_header'           => 'FAQ List',
            'list'                  => $list,
            'recent_post'           => $recent_post,
        );
        // return $list;
        return view('site.single.faq', $result);
    }

    public function testimonialList(){
        $list = Home::getTestimonialList();
        $result = array(
            'page_header'           => 'Testimonial List',
            'list'                  => $list,
        );
        // return $list;
        return view('site.single.testimonial', $result);
    }

    public function aboutUs(){
        $detail =  Home::getPageDetail('about-us');
        $working_process = Home::getWorkingProcessList();
        $testimonials = Home::getTestimonialList();
        $news = Home::getPostList($limit=3);
        $projects = Home::getProjectDetail($limit=3);
        Home::updatePageViewCount($detail->id);

        $result = array(
            'page_header'           => 'About Us',
            'detail'                => $detail,
            'working_process'       => $working_process,
            'testimonials'          => $testimonials,
            'projects'              => $projects,
            'news'                  => $news,
        );
        // return $teamlist;
        return view('site.single.aboutus', $result);
    }

    public function service(){
        $detail =  Home::getPageDetail('service');
        $web_services = Home::getWebServiceList();
        $services = Home::getServiceList();

        $result = array(
            'page_header'           => 'Services',
            'detail'                => $detail,
            'web_services'          => $web_services,
            'services'              => $services,

        );
        // return $teamlist;
        return view('site.single.service', $result);
    }

    public function teamList(){
        $team_list = Home::getTeamList();

        $result = array(
            'page_header'           => 'Team List',
            'team_list'             => $team_list,
        );
        // return $teamlist;
        return view('site.single.team', $result);
    }

    public function contactUs(){
        $result = array(
                'page_header'        => 'Contact Us',
            );
        return view('site.single.contact_us', $result);

    }

    public function postContactInfo(Request $request){

        $this->validate($request, [
            'name'          => 'required',
            'email'         => 'required|email',
            'subject'       => 'required',
            'message'       => 'required',
            'phoneno'       => 'required|numeric',
        ],[
            'phoneno.required' => 'The phone number field is required.'
        ]);

        $crud = new AdminContact;
        $crud->name = $request->name;
        $crud->email = $request->email;
        $crud->ip_address = $request->ip();
        $crud->message = $request->message;
        $crud->subject = $request->subject;
        $crud->phoneno = $request->phoneno;
        $crud->inserted_date = date('Y-m-d H:i:s');
        $crud->viewed = '0';
        $crud->status = '1';
        $crud->save();
        // Store your user in database

        // MailController::sendContactMail($crud->id);
       Session::flash('success_message', "Your Message Has Been Sucessfully Saved");
        return redirect(route('contactus'));


    }

    public function newsLetter(Request $request){
        if (!empty($request->all())) {
            $validator = Validator::make($request->all(),
                [
                    'email'             => 'required|email|unique:tbl_newsletter_list,email',
                ],
                [
                    'email.unique'       => 'Email has already been registered',
                ]
            );
            if ($validator->passes()) {
                $newsletter = new NewsLetter;
                $newsletter->email = $request->email;
                $newsletter->status = '1';
                $newsletter->save();
                // Store your user in database
                $result = array(
                    'error'     => false,
                    'message'   => "We've received your email. Thank You !!!",
                );

                return response()->json($result,200);
            }else{
                $result = array(
                    'error'     => true,
                    'errors'    => $validator->errors()->first()
                );
                return response()->json($result,200);
            }
        }else{
            $result = array(
                'error'     => true,
                'errors'    => 'Unauthorized Access',
            );
            return response()->json($result,200);
        }
    }

    public function getSearchData(Request $request){
         $this->validate($request, [
            'title'          => 'required',
        ]);
    // search in post, service and project
        $title = $request->title;
        // $projects = AdminProject::where('status', 1)
        //             ->where('title', 'like', '%'.$title.'%')
        //             ->orderBy('sort_order')
        //             ->paginate(1);

        // $news = AdminPosts::where('status', 1)
        //         ->where('title', 'like', '%'.$title.'%')
        //         ->orderBy('published_date', 'asc')
        //         ->paginate(1);
        // $services = AdminService::where('status', 1)
        //             ->where('title', 'like', '%'.$title.'%')
        //             ->orderBy('sort_order')
        //             ->paginate(1);

        // $recent_post = Home::getPostList($limit=5);
        // $result = [
        //     'projects'          => $projects,
        //     'news'              => $news,
        //     'services'          => $services,
        //     'recent_post'       => $recent_post,
        //     'page_header'       => 'Search Result Of '.$title,
        // ];

        return redirect(route('searchdatapage').'?search='.$title);
    }

    public function getSearchDataPage(){

    // search in post, service and project
        $title = isset($_GET['search']) ? $_GET['search'] : '';
        $projects = [];

        $news = AdminPosts::where('status', 1)
                ->where('title', 'like', '%'.$title.'%')
                ->orderBy('published_date', 'asc')
                ->paginate(5);
        $services = [];
        $recent_post = Home::getPostList($limit=5);
        $result = [
            'projects'          => $projects,
            'news'              => $news,
            'services'          => $services,
            'recent_post'       => $recent_post,
            'page_header'       => 'Search Result Of '.$title,
        ];

        return view('site.single.search', $result);


    }

    public function postUserService(Request $request){


            $validator = Validator::make($request->all(),[
                'name'          => 'required',
                'email'         => 'required|email',
                // 'subject'       => 'required',
                'message'       => 'required',
                'address'       => 'required',
                'phoneno'       => 'required|numeric',
            ]);
            if ($validator->passes()) {
                $crud = new AdminUserService;
                $crud->name = $request->name;
                $crud->email = $request->email;
                $crud->ip_address = $request->ip();
                $crud->message = $request->message;
                // $crud->subject = $request->subject;
                $crud->address = $request->address;
                $crud->phoneno = $request->phoneno;
                $crud->service_id = $request->service_id;
                $crud->inserted_date = date('Y-m-d H:i:s');
                $crud->viewed = '0';
                $crud->status = '1';
                $crud->save();
                // Store your user in database

                Session::flash('success_message', 'Your Message Has Been Successfully sent');
                $service_detail = AdminService::where('id', $request->service_id)->first();
                $result = array(
                    'error'         => false,
                    'message'       => "Your message has been sent. Thank you!",
                    'route_data'    => route('service.detail', $service_detail->slug),
                );

                return response()->json($result,200);
                // return redirect()->back();
            }else{
                $result = array(
                    'error'     => true,
                    'errors'    => $validator->errors()
                );
                return response()->json($result,200);
            }

    }


    // unused



    public function pageDetail($slug){

        $detail = Home::getPageDetail($slug);
        if (!empty($detail)) {
            $result = array(
                'page_header'   => $detail->title,
                'detail'        => $detail,
            );
            // return $result;
            return view('site.single.pagedetail', $result);
        } else{
            // return 'abc';
            return view('errors.404');
        }
    }


























}
