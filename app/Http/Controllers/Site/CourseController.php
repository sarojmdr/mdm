<?php

namespace App\Http\Controllers\Site;
use App\Http\Controllers\Controller;
use App\Model\admin\AdminBookPages;
use App\Model\admin\AdminChapter;
use App\Model\admin\AdminContact;
use App\Model\admin\AdminCourses;
use App\Model\admin\AdminLesson;
use App\Model\admin\AdminLessonComment;
use App\Model\admin\AdminQuizAnswer;
use App\Model\admin\AdminQuizQuestion;
use App\Model\admin\AdminReviews;
use App\Model\admin\NewsLetter;
use App\Model\admin\RelStudentCourses;
use App\Model\admin\RelStudentLessonHistory;
use App\Model\admin\TrainingParticipant;
use App\Model\admin\UserBookHistory;
use App\Model\site\Home;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller {

    public function showList(Request $request)
    {
        $list = AdminCourses::where('status', 1)->paginate(PAGES);
        foreach ($list as $k => $item){
            $lessons = AdminLesson::select('id', 'title')->where('course_id', $item->id)->where('status', 1)->get();
            $item->lessons = $lessons;
            $students = RelStudentCourses::where('course_id', $item->id)->select('id')->get();
            $item->students = $students;
            $review_count = AdminReviews::where('course_id', $item->id)->where('status', 1)->count();
            $item->review_count = $review_count;
        }
        // return $list;
        $result = array(
            'page_header'   => 'Course',
            'list' => $list,
        );
        return view('site.course.courselist',$result);
    }

    public function showCourseDetail($slug){
        $detail = AdminCourses::where('status', 1)->where('slug', $slug)->first();
        if($detail){  
            $chapters = AdminChapter::where('status', 1)->where('course_id', $detail->id)->select('id','title')->orderBy('sort_order', 'asc')->get();
            foreach ($chapters as $k => $val) {
                $lessons = AdminLesson::where('status', 1)->where(['course_id' => $detail->id, 'chapter_id' => $val->id])->select('id', 'title', 'duration', 'slug')->orderBy('sort_order', 'asc')->get();
                $val->lessons = $lessons;

            }
            $total_duration = Home::getTotalDurationByCourseId($detail->id);
            $detail->total_duration = $total_duration;
            $detail->chapters = $chapters;
            $students = RelStudentCourses::where('course_id', $detail->id)->select('id')->get();
            $detail->students = $students;
            $total_lesson = count(AdminLesson::where('course_id', $detail->id)->get());
            $reviews = AdminReviews::where('course_id', $detail->id)->orderBy('created_at', 'desc')->get();
            $result = array(
                'page_header' => $detail->title, 
                'detail' => $detail, 
                'reviews' => $reviews, 
                'total_lessons_count' => $total_lesson,
            );
            if(Auth::check()){
                $student_course = RelStudentCourses::where(['course_id' => $detail->id, 'user_id' => Auth::id()])->first();
                if($student_course){
                    $read_lesson_list = RelStudentLessonHistory::where('student_course_id', $student_course->id)->get();
                    $read_lessons = count($read_lesson_list);
                    foreach ($chapters as $chapter){
                        foreach ($chapter->lessons as $k => $lesson){
                            $lesson->read_status = 0;
                            if($read_lesson_list->contains('id',$lesson->id)){
                                $lesson->read_status = 1;
                            }
                        }
                    }
                    // return $chapters;
                }else{
                    $read_lessons = 0;
                }
                $result['read_lessons_count'] = $read_lessons;
            }
            // return $result;
            return view('site.course.course_detail', $result);
        }else{
            return view('errors.404');
        }
    }

    public function showLessonDetail($course_slug, $lesson_slug){
        if(Auth::check()){
            $course = AdminCourses::where('status', 1)->where('slug', $course_slug)->first();
            if($course){
                $detail = AdminLesson::where('status', 1)->where('slug', $lesson_slug)->select('id', 'title', 'description', 'course_id', 'slug', 'file', 'ppt_file')->first();
                if($detail){  
                    $student_course = RelStudentCourses::firstOrCreate(['course_id' => $detail->course_id, 'user_id' => Auth::id()]);
                    if(!empty($student_course->status)){
                        $student_lesson = RelStudentLessonHistory::firstOrCreate(['student_course_id' => $student_course->id, 'lesson_id' => $detail->id, 'status' => 1]);
                        // $lesson_order = getLessonOrder($detail->course_id);

                        $chapters = AdminChapter::where('status', 1)->where('course_id', $detail->course_id)->select('id','title')->orderBy('sort_order', 'asc')->get();
                        foreach ($chapters as $k => $val) {
                            $lessons = AdminLesson::where('status', 1)->where(['course_id' => $detail->course_id, 'chapter_id' => $val->id])->select('id', 'title', 'duration', 'slug', 'course_id')->orderBy('sort_order', 'asc')->get();
                            $val->lessons = $lessons;
                        }
                        $comments = AdminLessonComment::where('lesson_id' ,$detail->id)->orderBy('created_at', 'desc')->get();
                        $quizes = AdminQuizQuestion::where('course_id', $course->id)->whereNotNull('correct_answer')->get();
                        // return $quiz;
                        $result = array(
                            'page_header'   => $detail->title, 
                            'chapters'      => $chapters, 
                            'comments'      => $comments, 
                            'quizes'        => $quizes, 
                            'detail'        => $detail, 
                        );
                        // return $result;
                        return view('site.course.lesson_detail', $result);
                    }else{
                        Session::flash('message', 'Payment must be made to view the course.');
                        return back();
                    }
                }else{
                    return view('errors.404');
                }
            }else{
                return view('errors.404');
            }
        }else{
            Session::flash('message', 'Please Login first.');
            return redirect(route('login'));
        }
    }

    public function courseEnroll($slug){
        try {
            if(Auth::check()){
                $course = AdminCourses::where('status', 1)->where('slug', $slug)->first();
                if($course){
                    // if there are no lessons redirect to 404 page
                    if($course->lessons->isNotEmpty()){
                        $student_course = RelStudentCourses::firstOrCreate(['course_id' => $course->id, 'user_id' => Auth::id()]);
                        if(!empty($student_course->status)){
                            $student_lesson = RelStudentLessonHistory::where('student_course_id', $student_course->id)->get();
                            //if the student has not read the lesson, get first lesson else last read lesson.
                            if($student_lesson->isEmpty()){
                                $lesson_order = getLessonOrder($course->id);
                                $lesson_slug = $lesson_order[0]->slug;
                            }else{
                                $last_read_lesson_id = $student_lesson->last()->lesson_id;
                                $lesson_slug = AdminLesson::find($last_read_lesson_id)->slug;
                            }
                            $result = array(
                                'error' => false, 
                                'route' => route('lesson.detail', [$course->slug, $lesson_slug]),
                            );
                            return response()->json($result,200);
                        }else{
                            $result = array(
                                'error' => true,
                                'error_type' => 'approve',
                                'message' => 'Payment must be made to view the course.'
                            );
                        }
                    }else{
                        $result = array(
                            'error' => true,
                            'error_type' => 'redirect',
                            'route' => route('404'),
                        );
                    }
                }else{
                    $result = array(
                        'error' => true,
                        'error_type' => 'redirect',
                        'route' => route('404'),
                    );
                }
            }else{
                $result = array(
                    'error' => true,
                    'error_type' => 'redirect',
                    'route' => route('login'),
                );
                Session::flash('message', 'Please Login first.');
            }
            return response()->json($result,200);
        }catch (Exception $e) {
            return response()->json($e->getMessage(),422);
        }
    }

    public function addReview(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'first_name'    => 'required',
                'last_name'     => 'required',
                'email'         => 'required|email',
                'review'        => 'required',
                'rating'        => 'required',
            ]);
            if($validator->passes()){
                $course = AdminCourses::where('slug', $request->c_slug)->select('id')->first();
                $review = new AdminReviews;
                $review->course_id = $course->id;
                $review->user_id = Auth::id();
                $review->first_name = $request->first_name;
                $review->last_name = $request->last_name;
                $review->email = $request->email;
                $review->phone_no = $request->phone_no;
                $review->review = $request->review;
                $review->rating = $request->rating;
                $review->status = 1;
                $review->save();
                $result = array(
                    'error'     => false, 
                    'errors'    => '', 
                    'message'   => 'Your review has been submitted.',
                );
                return response()->json($result, 200);
            }else{
                $result = array(
                    'error'     => true, 
                    'errors'    => $validator->errors(), 
                );
                return response()->json($result, 200);
            }
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
    }

    public function addComment(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'comment'    => 'required',
            ]);
            if($validator->passes()){
                $lesson = AdminLesson::where('slug', $request->lesson_slug)->select('id')->first();
                $comment = new AdminLessonComment;
                $comment->lesson_id = $lesson->id;
                $comment->user_id = Auth::id();
                $comment->comment = $request->comment;
                $comment->status = 1;
                $comment->save();
                $result = array(
                    'error'     => false, 
                    'errors'    => '', 
                    'message'   => 'Your comment has been submitted.',
                );
                return response()->json($result, 200);
            }else{
                $result = array(
                    'error'     => true, 
                    'errors'    => $validator->errors(), 
                );
                return response()->json($result, 200);
            }
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 422);
        }
    } 

    public function showQuizDetail($course_slug, $quiz_order){
        $course = AdminCourses::where('status', 1)->where('slug', $course_slug)->first();
        if($course){  
            $quiz = AdminQuizQuestion::where('course_id', $course->id)->where('sort_order', $quiz_order)->where('status', 1)->first();
            if($quiz){
                $quiz->answers = AdminQuizAnswer::where('question_id', $quiz->id)->where('status', 1)->get();
                $quizes = AdminQuizQuestion::where('course_id', $course->id)->whereNotNull('correct_answer')->get();

                // return $quiz;
                $result = array(
                    'page_header' => $course->title, 
                    'course' => $course, 
                    'quiz'  => $quiz,
                    'quizes'  => $quizes,
                );
                // return $result;
                return view('site.course.quiz_detail', $result);
            }else{
                return view('errors.404');
            }
        }else{
            return view('errors.404');
        }   
    }

    public function postQuizAnswer(Request $request, $course_slug, $quiz_order){
        try {
            $validator = Validator::make($request->all(),[
                'answer'    => 'required',
            ],['answer.required' => 'Please select an answer.']);
            if($validator->passes()){
                $course = AdminCourses::where('slug', $course_slug)->where('status', 1)->first();
                $quiz = AdminQuizQuestion::where('course_id', $course->id)->where('sort_order', $quiz_order)->first();
                if($request->answer == $quiz->correct_answer){
                    Session::flash('success_message', 'Your answer is correct.');
                }else{
                    Session::flash('error_message', 'Your answer is not correct.');
                }
                return back();
            }else{
                // $errors = $validator->errors();
                // return $errors;
                // Session::flash('error_message', $errors);
                return back();
            }
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 422);
        } 
    }

}