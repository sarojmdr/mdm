<?php

namespace App\Http\Controllers\Site;
use App\Http\Controllers\Controller;
use App\Model\admin\AdminBookPages;
use App\Model\admin\AdminBooks;
use App\Model\admin\AdminContact;
use App\Model\admin\NewsLetter;
use App\Model\admin\TrainingParticipant;
use App\Model\admin\UserBookHistory;
use App\Model\site\Home;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller {

    public function showBookDashboard(Request $request)
    {
        $detail = Auth::user();
        $result = array(
            'page_header'   => 'Home',
            'detail'   => $detail,
        );
        return view('site.dashboard.index',$result);
    }

    public function updateAccountInformation(Request $request){
        $this->validate($request, [
            'name' => 'required',
            // 'email' => 'required',
            'mobileno' => 'required',
        ],[
            'name.required' => 'The Username is required.',
            'mobileno.required' => 'The Phone no. is required.',
        ]);
        $user = User::findOrFail(Auth::id());
        $user->name = $request->name;
        $user->mobileno = $request->mobileno;
        $user->save();
        Session::flash('success_message', UPDATED);
        // return $user;
        return redirect(route('home'));
    }

    public function showBookList(Request $request)
    {
        $list = AdminBooks::getBookList();
        // return $list;
        $result = array(
            'page_header'   => 'Book Listing',
            'list' => $list,
        );
        return view('site.books.booklist',$result);
    }

    public function showBookDetail($slug){
        $detail = AdminBooks::getBookDetailBySlug($slug);
        if($detail){  
            AdminBooks::updateBookDetailViewCount($detail->id);
            $list = AdminBooks::getBook($limit=3);
            $result = array(
                'page_header' => $detail->title, 
                'detail' => $detail, 
                'list' => $list, 
            );
            // return $result;
            return view('site.books.book_detail', $result);
        }else{
            return view('errors.404');
        }
    }

    public function bookApproveStatusCheck($slug){
        if(Auth::check()){
            $user_id = Auth::id();
            $book = AdminBooks::where('slug', $slug)->first();
            $user_history = UserBookHistory::where('user_id', $user_id)
                                            ->where('book_id', $book->id)->first();
            if($user_history){
                if($user_history->approve_status == 1){
                    $result = array(
                        'error' => false, 
                        'route' => route('book.view', $book->slug),
                    );
                    return response()->json($result,200);
                }else{
                    $result = array(
                        'error' => true,
                        'error_type' => 'approve',
                        'message' => 'Payment must be made to view the book.' 
                    );
                    return response()->json($result,200);
                }
            }else{
                UserBookHistory::setBookHistoryForUser($user_id, $book->id);
                $result = array(
                    'error' => true,
                    'error_type' => 'approve',
                    'message' => 'Payment must be made to view the book.'
                );
            }
        }else{
            $result = array(
                'error' => true,
                'error_type' => 'login',
                'route' => route('login'),
                'message' => 'Please Login first.' 
            );
            Session::flash('message', 'Please Login first.');
            return response()->json($result,200);
        }
        return response()->json($result,200);
    }

    public function showBookView($slug){
        $bookdetail = AdminBooks::getBookDetailBySlug($slug);
        $pagedetail = AdminBookPages::getPagesByBookId($bookdetail->id);
        $chapterList = AdminBookPages::where(['book_id' => $bookdetail->id, 'status' => 1, ])->whereNotNull('title')->orderBy('sort_order', 'asc')->select('title', 'slug', 'sort_order')->get();
        // return $chapterList;
        if(!empty($pagedetail->items())){
            // dd($slug);
            $user_id = Auth::user()->id;
            UserBookHistory::setBookHistoryForUser($user_id, $bookdetail->id, $pagedetail);
            $result = array(
                'page_header' => $bookdetail->title,
                'bookdetail' => $bookdetail,
                'pagedetail' => $pagedetail,
                'chapterList' => $chapterList,
            );
            $user_history = UserBookHistory::where('user_id', $user_id)->where('book_id', $bookdetail->id,)->first();
            if($user_history->approve_status == 1){
                return view('site.books.book_view', $result);
            }else{
                Session::flash('message', 'Payment must be made to view the book.');
                return redirect(route('book.detail', $bookdetail->slug));
            }
        }else{
            return redirect(route('book.detail', $bookdetail->slug));
        }
    }

    public function showMyBook(){
        $user_id = Auth::user()->id;
        $bookhistory = AdminBooks::getUserBookHistory($user_id);
            
            $result = array(
                'page_header' => 'My Books',
                'bookhistory' => $bookhistory,
            );
        // return $result;
        return view('site.dashboard.mybooks', $result);
    }

    public function changePassword(Request $request)
    {
        $result = array(
            'page_header' => 'Change Password', 
        );
        
        return view('site.dashboard.changePassword',$result);
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required|min:6',
            'password' => 'required|confirmed|min:6|different:old_password',
        ]);
        if(Hash::check($request->old_password, Auth::user()->password)){
            Auth::user()->fill([
                'password' => bcrypt($request->password),
            ])->save();
            Session::flash('success_message', 'Your password has been changed.');
        }else{
            Session::flash('message', 'Your old password does not match with our records.');
        }
        // return $request;
        
        return redirect(route('change.password'));
    }


    

}