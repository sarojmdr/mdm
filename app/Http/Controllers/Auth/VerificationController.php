<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('mailVerify');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }


    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function mailVerify(Request $request)
    {   
        $user = User::where('id', $request->route('id'))->first();

        if (! hash_equals((string) $request->route('id'), (string) $user->id)) {
            return view('errors.404');
        }

        if(! hash_equals((string) $request->route('hash'), (string) sha1($user->verify_token) )){
            return view('errors.404');
        }


        if ($user->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }

        if ($user->markEmailAsVerified()) {
            $user = User::findOrFail($user->id);
            if($user){
                User::findOrFail($user->id)->
                    update([
                        'email_verify' => '1',
                        'status'       => '1',
                        'verify_token' => NULL,
                    ]);
                    // $this->guard()->login('$user');
            }
            event(new Verified($user));
        }

        Session::flash('success_message', 'You are now verified. Please enter login credentials');
        return redirect($this->redirectPath())->with('verified', true);
    }

   
}
