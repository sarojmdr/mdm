<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\AdminProjectCategory;
use App\Model\admin\RelationPostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PrintHelper;

class AdminProjectCategoryController extends Controller {

    private $title = 'Project Category';
    private $sort_by = 'sort_order';
    private $sort_order = 'asc';
    private $index_link = 'project-category.index';
    private $list_page = 'admin.project_category.list';
    private $create_form = 'admin.project_category.add';
    private $update_form = 'admin.project_category.edit';
    private $link = 'project-category';
    private $user_id;

    public function index()
    {

        $list = AdminProjectCategory::orderBy($this->sort_by, $this->sort_order)->get();
        // foreach($list as $item){
        //     $data=RelationPostCategory::where('category_id',$item->id)->first();
        //     if(!empty($data)){
        //         $item->can_delete = false;
        //     }else{
        //         $item->can_delete = true;
        //     }
        // }
        // $list = AdminProjectCategory::orderBy($this->sort_by, $this->sort_order)->paginate(PAGES);
        $result = array(
            'list'              => $list,
            'page_header'       => 'Add '.$this->title,
            'link'              => $this->link,
        );
        return view($this->list_page, $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect(route($this->index_link));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        $crud = new AdminProjectCategory;
        $crud->title = $request->title;
        $crud->status = $request->status;
        $crud->sort_order = PrintHelper::nextSortOrder('tbl_project_category');
        $crud->created_by = session('admin')['userid'];
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = AdminProjectCategory::findOrFail($id);
        $list = AdminProjectCategory::orderBy($this->sort_by, $this->sort_order)->get();
        $result = array(
            'page_header'       => 'Edit '.$this->title.' Detail',
            'record'            => $record,
            'list'              => $list,
            'link'              => $this->link,
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'      => 'required',
            'slug'       => 'required',
        ]);
        $crud = AdminProjectCategory::findOrFail($id);
        $crud->title = $request->title;
        $crud->slug = $request->slug;
        $crud->status = $request->status;
        $crud->created_by = session('admin')['userid'];
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = AdminProjectCategory::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
        return redirect(route($this->index_link));
    }
}