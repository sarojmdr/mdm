<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\AdminLoginController;
use App\Http\Controllers\site\MailController;
use App\Model\admin\AdminNewsletter;
use App\Model\admin\NewsLetter;
use App\Model\admin\NewsletterSubscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminNewsletterController extends AdminController {

    private $title = 'Newsletter';
    private $sort_by = 'created_at';
    private $sort_order = 'desc';
    private $index_link = 'newsletter.index';
    private $list_page = 'admin.newsletter.list';
    private $create_form = 'admin.newslettersubscription.add';
    private $update_form = 'admin.newsletter.edit';
    private $show_form = 'admin.newslettersubscription.show';
    private $link = 'newsletter';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if(!empty($_GET)){
            $status = $request->input('status_search');
            $date = $request->input('requested_date');
            $query = AdminNewsletter::orderBy($this->sort_by, $this->sort_order);
            if($status != ''){
                $query = $query->where('status', $status);
            }
            if($date != ''){
                $query = $query->where('created_at', 'LIKE', '%'.$date.'%');
            }
            $list = $query->paginate(PAGES);
        }else{
            $list = AdminNewsletter::orderBy($this->sort_by, $this->sort_order)->paginate(PAGES);
        }
        $result = array(
            'list'              => $list,
            'page_header'       => 'List of '.$this->title,
            'link'              => $this->link,
        );
        return view($this->list_page, $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $result = array(
            'page_header'       => 'Create '.$this->title.' Subscription',
            'link'              => $this->link,
        );
        return view($this->create_form, $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
            'title'              => 'required',
            'subject'            => 'required',
            'description'        => 'required',
        ]);
        $crud = new NewsletterSubscription;
        $crud->title = $request->title;
        $crud->subject = $request->subject;
        $crud->description = $request->description;
        // $crud->slug = uniqid();
        $crud->created_by = AdminLoginController::id();
        $crud->inserted_date = date('Y-m-d H:i:s');
        $crud->mail_status = 0;
        $crud->status = 1;
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route('newsletter.subscription'));
    }

    public function subscriptionList(){
        $list = NewsletterSubscription::orderBy($this->sort_by, $this->sort_order)->paginate(PAGES);
        $result = array(
            'list'              => $list,
            'page_header'       => 'List of Newsletter Subscription',
            'link'              => 'newsletter',
        );
        return view('admin.newslettersubscription.list', $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $record = NewsletterSubscription::findorfail($id);
        if (!empty($record)) {
            $result = array(
                'page_header'       => 'View '.$this->title.' Detail',
                'record'            => $record,
                'link'              => $this->link,
            );
            return view($this->show_form, $result);
        }else{
            return view('errors.404');            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id){
    //     $record = AdminNewsletter::findOrFail($id);
    //     $result = array(
    //         'page_header'       => 'Edit '.$this->title.' Detail',
    //         'record'            => $record,
    //         'link'              => $this->link,
    //     );
    //     return view($this->update_form, $result);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id){
    //      $this->validate($request, [
    //         'title'              => 'required',
    //         'subject'            => 'required',
    //         'description'        => 'required',
    //     ]);
    //     $user_id = AdminLoginController::id();
    //     $crud = AdminNewsletter::findOrFail($id);
    //     $crud->title = $request->title;
    //     $crud->subject = $request->subject;
    //     $crud->description = $request->description;
    //     $crud->created_by = $user_id;
    //     $crud->status = '1';
    //     $crud->save();
    //     // email send to newsletter list
    //     MailController::sendNewsLetterMail($id);
    //     Session::flash('success_message', CREATED);
    //     return redirect(route($this->index_link));
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
    }

     public function resendMail($id){
        $record = NewsletterSubscription::findOrFail($id);
        if($record->mail_status != 1){
            AdminMailController::sendNewsLetterMail($record->id);
            $record->mail_status = 1;
            $record->save();
        }else{
            Session::flash('success_message', 'Mail has already been sent.');
        }
        return redirect(route('newsletter.subscription'));
    }

    // public function emailList(){
    //     $list = NewsLetter::orderBy('email','asc')->get();
    //     $result = array(
    //         'list'          => $list,
    //         'page_header'   => 'NewsLetter Subscriber List',
    //         'link'          => $this->link,
    //     );
    //     return view('admin.newsletter.list',$result);
    // }

    public function newsletterStatusUpdate($newsletter_id){
        $record = Newsletter::findOrFail($newsletter_id);
        if ($record->status != 2) {
            $record->status = 2;
        }else{
            $record->status = 3;
        }
        $record->save();
    }
}