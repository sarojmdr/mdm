<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin\AdminController;
use App\Model\admin\AdminSetting;
use Illuminate\Http\Request;

class AdminDashboardController extends AdminController {

    public function dashboard(){
        // $new_client_request = AdminSetting::getClientRequest(1);
        // $view_client_request = AdminSetting::getClientRequest(2);
        // $blocked_client_request = AdminSetting::getClientRequest(3);

        $new_newsletter = AdminSetting::getNewsletter(1);
        $active_newsletter = AdminSetting::getNewsletter(2);
        $blocked_newsletter = AdminSetting::getNewsletter(3);

        $projects = AdminSetting::getProjects();
        $testimonial = AdminSetting::getTestimonial();

        $new_feedback = AdminSetting::getFeedback(0);
        $viewed_feedback = AdminSetting::getFeedback(1);
        $result = array(
            'page_header'               => 'Dashboard',
            // 'new_client_request'        => $new_client_request,
            // 'view_client_request'       => $view_client_request,
            // 'blocked_client_request'    => $blocked_client_request,
            'new_newsletter'            => $new_newsletter,
            'active_newsletter'         => $active_newsletter,
            'blocked_newsletter'        => $blocked_newsletter,
            'projects'                  => $projects,
            'testimonial'               => $testimonial,
            'new_feedback'              => $new_feedback,
            'viewed_feedback'           => $viewed_feedback,

        );
        return view('admin.home',$result);
    }

    public function mediaLibrary(){
    	$result = array(
            'page_header' => 'Media Library'
        );
        return view('admin.medialibrary', $result);
    }

}
