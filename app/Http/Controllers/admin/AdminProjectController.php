<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\AdminClient;
use App\Model\admin\AdminProject;
use App\Model\admin\AdminProjectCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PrintHelper;

class AdminProjectController extends Controller {

    private $title = 'Project';
    private $sort_by = 'sort_order';
    private $sort_order = 'asc';
    private $index_link = 'project.index';
    private $list_page = 'admin.project.list';
    private $create_form = 'admin.project.add';
    private $update_form = 'admin.project.edit';
    private $link = 'project';
    private $list_project_category_id;
    private $list_client_id;
    private $list_status;
    private $list_title;

    public function __construct(){
    //     $this->list_project_category_id = isset($_GET['project_category_id']) ? $_GET['project_category_id'] : '';
    //     $this->list_client_id = isset($_GET['client_id']) ? $_GET['client_id'] : '';
        $this->list_status = isset($_GET['status']) ? $_GET['status'] : '';
        $this->list_title = isset($_GET['title']) ? $_GET['title'] : '';
    }

    public function index()
    {
        $list = AdminProject::orderby($this->sort_by,$this->sort_order);

        if($this->list_status != ''){
            $list->where('status', $this->list_status);
        }
        if($this->list_title != ''){
            $list->where('title', 'like' ,'%'.$this->list_title.'%');
        }
        $list = $list->paginate(PAGES);

        $result=array(
            'list'                              => $list,
            'page_header'                       =>'List of '.$this->title,
            'link'                              => $this->link,
        );
        return view($this->list_page,$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        $result = array(
            'page_header'                       => 'Create '.$this->title.' Detail',
            'link'                              => $this->link,
        );
        return view($this->create_form, $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
            'title'                 => 'required',
            'client'                => 'required',
            'category'              => 'required',
            'website'               => 'required',
            'services'              => 'required',
            'project_date'        => 'required',
            'description'           => 'required',
            'image'                 => 'required',
        ]);

        $crud = new AdminProject;
        $crud->title = $request->title;
        $crud->client = $request->client;
        $crud->category = $request->category;
        $crud->description = $request->description;
        $crud->website = $request->website;
        $crud->services = $request->services;
        $crud->project_date =( $request->project_date != '' ? $request->project_date : date('Y-m-d'));
        $crud->image = chunkfullurl($request->image);
        $crud->sort_order = PrintHelper::nextSortOrder('tbl_project');
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $pages = AdminProject::findOrFail($id);
        $result = array(
            'page_header'               => 'Edit '.$this->title.' Detail',
            'record'                    => $pages,
            'link'                      => $this->link,
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
       $this->validate($request, [
            'title'                 => 'required',
            'client'                => 'required',
            'category'              => 'required',
            'website'               => 'required',
            'services'              => 'required',
            'project_date'        => 'required',
            'description'           => 'required',
            'image'                 => 'required',
        ]);

        $crud = AdminProject::findOrFail($id);
        $crud->title = $request->title;
        $crud->client = $request->client;
        $crud->category = $request->category;
        $crud->description = $request->description;
        $crud->website = $request->website;
        $crud->services = $request->services;
        $crud->project_date =( $request->project_date != '' ? $request->project_date : date('Y-m-d'));
        $crud->image = chunkfullurl($request->image);
        $crud->sort_order = PrintHelper::nextSortOrder('tbl_project');
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $crud = AdminProject::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
         return redirect(route($this->index_link));
    }
}
