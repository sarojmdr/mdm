<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\AdminClient;
use App\Model\admin\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PrintHelper;

class ClientController extends AdminController
{

    private $title = 'Client';
    private $sort_by = 'sort_order';
    private $sort_order = 'asc';
    private $index_link = 'client.index';
    private $list_page = 'admin.client.list';
    private $create_form = 'admin.client.add';
    private $update_form = 'admin.client.edit';
    private $link = 'client';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = AdminClient::orderby($this->sort_by,$this->sort_order)
                ->get();;
                // ->paginate(PAGES);
        $result=array(
            'list'          =>$list,
            'page_header'   =>'List of '.$this->title,
            'link'          => $this->link,
        );
        return view($this->list_page,$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $result = array(
            'page_header'       => 'Create '.$this->title.' Detail',
            'link'              => $this->link,
        );
        return view($this->create_form, $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
            'title'                 => 'required',
            'logo'                  => 'required',
            'published_date'        => 'required',
            'link'                  => 'required',
        ]);

        $crud = new AdminClient;
        $crud->title = $request->title;
        $crud->link = $request->link;
        // $crud->description = $request->description;
        $crud->published_date = $request->published_date;
        $crud->logo = chunkfullurl($request->logo);
        $crud->sort_order = PrintHelper::nextSortOrder('tbl_client');
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $pages = AdminClient::findOrFail($id);
        $result = array(
            'page_header'       => 'Edit '.$this->title.' Detail',
            'record'            => $pages,
            'link'              => $this->link,
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        
       $this->validate($request, [
            'title'                 => 'required',
            // 'logo'                  => 'required',
            // 'image_1'               => 'required',
            'published_date'        => 'required',
            'link'                  => 'required',
            'slug'                  => 'required',
        ]);

        $crud = AdminClient::findOrFail($id);
         $crud->title = $request->title;
        $crud->link = $request->link;
        // $crud->description = $request->description;
        $crud->slug = $request->slug;
        $crud->published_date = $request->published_date;
        if($request->logo){
            $crud->logo = chunkfullurl($request->logo);
        }
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $crud = AdminClient::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
        return redirect(route($this->index_link));
    }
}
