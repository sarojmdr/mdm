<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\Partner;
use App\Model\admin\PartnerType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PartnerController extends Controller {

    private $title = 'Partner';
    private $sort_by = 'title';
    private $sort_order = 'asc';
    private $index_link = 'partner.index';
    private $list_page = 'admin.partner.list';
    private $create_form = 'admin.partner.add';
    private $update_form = 'admin.partner.edit';
    private $link = 'partner';
    private $partnertypelist;
    private $list;


    public function __construct(){
        $this->partnertypelist = PartnerType::orderby('title','asc')->get();
        $this->list = Partner::with('partnertype')->orderBy($this->sort_by, $this->sort_order)->get();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = array(
            'list'              => $this->list,
            'page_header'       => 'Add '.$this->title,
            'link'              => $this->link,
            'partnertypelist'   => $this->partnertypelist,
        );
        return view($this->list_page, $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect(route($this->index_link));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'                 => 'required',
            'image'                 => 'required',
            'partner_type_id'       => 'required',
        ]);
        $crud = new Partner;
        $crud->title = $request->title;
        $crud->url = $request->url;
        $crud->image = chunkfullurl($request->image);
        $crud->partner_type_id = $request->partner_type_id;
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = Partner::findOrFail($id);
        $result = array(
            'page_header'       => 'Edit '.$this->title.' Detail',
            'record'            => $record,
            'list'              => $this->list,
            'link'              => $this->link,
            'partnertypelist'   => $this->partnertypelist,
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'                 => 'required',
            'image'                 => 'required',
            'partner_type_id'       => 'required',
        ]);
        $crud = Partner::findOrFail($id);
        $crud->title = $request->title;
        $crud->url = $request->url;
        $crud->image = chunkfullurl($request->image);
        $crud->partner_type_id = $request->partner_type_id;
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = Partner::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
        return redirect(route($this->index_link));
    }
}