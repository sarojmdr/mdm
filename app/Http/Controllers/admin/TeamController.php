<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PrintHelper;

class TeamController extends AdminController {

    private $title = 'Team';
    private $sort_by = 'sort_order';
    private $sort_order = 'asc';
    private $index_link = 'team.index';
    private $list_page = 'admin.team.list';
    private $create_form = 'admin.team.add';
    private $update_form = 'admin.team.edit';
    private $link = 'team';
    

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $list = Team::orderBy($this->sort_by,$this->sort_order)->get();;
        $result = array(
            'list'              => $list,
            'page_header'       => 'List of '.$this->title,
            'link'              => $this->link,
        );
        return view($this->list_page, $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $result = array(
            'page_header'       => 'Create '.$this->title.' Detail',
            'link'              => $this->link,
            
        );
        return view($this->create_form, $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
            'name'                  => 'required',
            'info'                  => 'required',
            'designation'           => 'required',
            'image'                 => 'required',
        ]);

        $crud = new Team;
        $crud->name = $request->name;
        $crud->designation = $request->designation;
        $crud->image = chunkfullurl($request->image);
        $crud->info = $request->info;
        $crud->facebook_link = $request->facebook_link;
        $crud->linkedin_link = $request->linkedin_link;
        $crud->twitter_link = $request->twitter_link;
        $crud->google_plus_link = $request->google_plus_link;
        $crud->email = $request->email;
        $crud->phone = $request->phone;
        $crud->status = $request->status;
        $crud->sort_order = PrintHelper::nextSortOrder('tbl_teams');
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $pages = Team::findOrFail($id);
        $result = array(
            'page_header'       => 'Edit '.$this->title.' Detail',
            'record'            => $pages,
            'link'              => $this->link,
            
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $this->validate($request, [
            'name'                  => 'required',
            'info'                  => 'required',
            'designation'           => 'required',
            'image'                 => 'required',
        ]);

        $crud = Team::findOrFail($id);
        $crud->name = $request->name;
        $crud->designation = $request->designation;
        $crud->image = chunkfullurl($request->image);
        $crud->info = $request->info;
        $crud->facebook_link = $request->facebook_link;
        $crud->linkedin_link = $request->linkedin_link;
        $crud->twitter_link = $request->twitter_link;
        $crud->google_plus_link = $request->google_plus_link;
        $crud->email = $request->email;
        $crud->phone = $request->phone;
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $crud = Team::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
        return redirect(route($this->index_link));
    }
    
}