<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\AdminAboutContent;
use App\Model\admin\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PrintHelper;

class AboutContentController extends AdminController
{

    private $title = 'About Us Content';
    private $sort_by = 'sort_order';
    private $sort_order = 'asc';
    private $index_link = 'about_content.index';
    private $list_page = 'admin.aboutus_content.list';
    private $create_form = 'admin.aboutus_content.add';
    private $update_form = 'admin.aboutus_content.edit';
    private $link = 'about_content';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = AdminAboutContent::orderby($this->sort_by,$this->sort_order)
                ->get();
                // ->paginate(PAGES);
        $result=array(
            'list'          =>$list,
            'page_header'   =>'List of '.$this->title,
            'link'          => $this->link,
        );
        return view($this->list_page,$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $result = array(
            'page_header'       => 'Create '.$this->title.' Detail',
            'link'              => $this->link,
        );
        return view($this->create_form, $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
            'title'                 => 'required',
            // 'published_date'        => 'required',
            'icon'                  => 'required',
        ]);

        $crud = new AdminAboutContent;
        $crud->title = $request->title;
        $crud->icon = $request->icon;
        // $crud->description = $request->description;
        $crud->published_date = ($request->published_date != '' ? $request->published_date : date('Y-m-d'));
        $crud->sort_order = PrintHelper::nextSortOrder('tbl_about_us_content');
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $pages = AdminAboutContent::findOrFail($id);
        $result = array(
            'page_header'       => 'Edit '.$this->title.' Detail',
            'record'            => $pages,
            'link'              => $this->link,
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
       $this->validate($request, [
            'title'                 => 'required',
            // 'published_date'        => 'required',
            'icon'                  => 'required',
            'slug'                  => 'required',
        ]);

        $crud = AdminAboutContent::findOrFail($id);
         $crud->title = $request->title;
        $crud->icon = $request->icon;
        // $crud->description = $request->description;
        $crud->slug = $request->slug;
        $crud->published_date = ($request->published_date != '' ? $request->published_date : date('Y-m-d'));
        // if(!empty($request->file('logo'))){
        //     $crud->logo = imageFilePath($request->logo, 'about_content');
        // }
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $crud = AdminAboutContent::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
        return redirect(route($this->index_link));
    }
}
