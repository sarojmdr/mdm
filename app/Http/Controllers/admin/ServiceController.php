<?php

namespace App\Http\Controllers\admin;

use PrintHelper;
use Illuminate\Http\Request;
use App\Model\admin\Testimonial;
use App\Model\admin\AdminService;
use Illuminate\Support\Facades\Session;
use App\Model\admin\AdminServiceWisePoint;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\AdminLoginController;

class ServiceController extends AdminController
{

    private $title = 'Service';
    private $sort_by = 'sort_order';
    private $sort_order = 'asc';
    private $index_link = 'service.index';
    private $list_page = 'admin.service.list';
    private $create_form = 'admin.service.add';
    private $update_form = 'admin.service.edit';
    private $link = 'service';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = AdminService::orderby($this->sort_by,$this->sort_order)
                ->get();
                // ->paginate(PAGES);
        $result=array(
            'list'          =>$list,
            'page_header'   =>'List of '.$this->title,
            'link'          => $this->link,
        );
        return view($this->list_page,$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $result = array(
            'page_header'       => 'Create '.$this->title.' Detail',
            'link'              => $this->link,
        );
        return view($this->create_form, $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
            'title'                 => 'required',
            'icon'                  => 'required',
            'image'                 => 'required',
            'short_description'     => 'required',
            // 'published_date'        => 'required',
        ]);

        $crud = new AdminService;
        $crud->title = $request->title;
        $crud->description = $request->description;
        $crud->short_description = $request->short_description;
        $crud->published_date =( $request->published_date != '' ? $request->published_date : date('Y-m-d'));
        $crud->icon = chunkfullurl($request->icon);
        $crud->image = chunkfullurl($request->image);
        $crud->sort_order = PrintHelper::nextSortOrder('tbl_services');
        $crud->status = $request->status;
        $crud->in_footer = $request->in_footer;
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $pages = AdminService::findOrFail($id);
        $result = array(
            'page_header'       => 'Edit '.$this->title.' Detail',
            'record'            => $pages,
            'link'              => $this->link,
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
       $this->validate($request, [
            'title'                     => 'required',
            // 'published_date'        => 'required',
            'icon'                      => 'required',
            'image'                     => 'required',
            'slug'                      => 'required',
            'short_description'         => 'required',
        ]);

        $crud = AdminService::findOrFail($id);
         $crud->title = $request->title;
        $crud->icon = chunkfullurl($request->icon);
        $crud->image = chunkfullurl($request->image);
        $crud->description = $request->description;
        $crud->short_description = $request->short_description;
        $crud->slug = $request->slug;
        $crud->published_date = ($request->published_date != '' ? $request->published_date : date('Y-m-d'));
        $crud->status = $request->status;
        $crud->in_footer = $request->in_footer;
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        AdminServiceWisePoint::where('service_id', $id)->delete();
        $crud = AdminService::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
        return redirect(route($this->index_link));
    }
}
