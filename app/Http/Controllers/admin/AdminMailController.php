<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\admin\AdminController;
use App\Model\admin\AdminContact;
use App\Model\admin\AdminNewsletter;
use App\Model\admin\NewsletterSubscription;
use Illuminate\Mail\Message;
use Mail;

class AdminMailController extends AdminController {

    public static function sendFeedBackReplyMail($contactid,$message_reply){
        $record = AdminContact::findOrFail($contactid);

        $data = array (
            'subject'           => 'Reply from '.SITE_NAME,
            'record'            => $record,
            'message_reply'     => $message_reply,
        );
        // return view('email.generatelogindetail', $data);
        Mail::send('email.feedbackmessagereply', $data, function (Message $mail)use ($data) {
            $mail_from = env('MAIL_USERNAME', 'noreply.candidnepal@gmail.com');
            $mail->from($mail_from, SITE_NAME);
            $mail->to($data['record']->email, $data['record']->email);
            $mail->subject($data['subject']);
            $mail->priority(3);
        });

        // check for failures
        if (Mail::failures()) {
            return false;
            // return response showing failed emails
        }else{
            return true;
        }
    }

    public static function sendNewsLetterMail($message_id) {
        $record = NewsletterSubscription::where('id',$message_id)->first();
        $mails = AdminNewsletter::where('status', 2)->get();
        $data = array (
            'subject'               => $record->subject,
            'record'                => $record,
        );
        foreach ($mails as $key => $mail_data) {
            Mail::send('email.newslettermail', $data, function (Message $mail)use ($data, $mail_data) {
                $mail_from = env('MAIL_USERNAME', 'noreply.candidnepal@gmail.com');
                $mail->from($mail_from, SITE_NAME);
                $mail->to($mail_data->email, $mail_data->email);
                $mail->subject($data['subject']);
                $mail->priority(3);
            });
        }
    }

}
