<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\AdminService;
use App\Model\admin\AdminServiceWisePoint;
use App\Model\admin\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PrintHelper;

class ServiceWisePointController extends AdminController
{

    private $title = 'Service Wise Point';
    private $sort_by = 'sort_order';
    private $sort_order = 'asc';
    private $index_link = 'service-wise-point.index';
    private $list_page = 'admin.service.service_wise_point.list';
    private $create_form = 'admin.service.service_wise_point.add';
    private $update_form = 'admin.service.service_wise_point.edit';
    private $link = 'service-wise-point';
    private $services ;
    private $list_service_id ;
    private $list_status ;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->services = AdminService::select('id', 'title')
                            ->where('status', 1)
                            ->get();
        $this->list_service_id = isset($_GET['service_id']) ? $_GET['service_id'] : '';
        $this->list_status = isset($_GET['status']) ? $_GET['status'] : '';
    }
    public function index()
    {
        $list = AdminServiceWisePoint::query();
        if($this->list_service_id != ''){
            $list->where('service_id', $this->list_service_id);
        }
        if($this->list_status != ''){
            $list->where('status', $this->list_status);
        }
        $list = $list->orderby($this->sort_by,$this->sort_order)
                ->paginate(PAGES);
        $result=array(
            'list'              =>$list,
            'page_header'       =>'List of '.$this->title,
            'link'              => $this->link,
            'services'          => $this->services,
        );
        return view($this->list_page,$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $result = array(
            'page_header'           => 'Create '.$this->title.' Detail',
            'link'                  => $this->link,
            'services'              => $this->services,
        );
        return view($this->create_form, $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validate($request, [
            'service_id'            => 'required',
            'title'                 => 'required',
            'description'           => 'required',
        ]);

        $crud = new AdminServiceWisePoint;
        $crud->service_id = $request->service_id;
        $crud->title = $request->title;
        $crud->description = $request->description;
        $crud->sort_order = PrintHelper::nextSortOrder('tbl_service_wise_point');
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $pages = AdminServiceWisePoint::findOrFail($id);
        $result = array(
            'page_header'       => 'Edit '.$this->title.' Detail',
            'record'            => $pages,
            'link'              => $this->link,
            'services'          => $this->services,
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
       $this->validate($request, [
            'service_id'                    => 'required',
            'title'                         => 'required',
            'description'                   => 'required',
        ]);

        $crud = AdminServiceWisePoint::findOrFail($id);
        $crud->service_id = $request->service_id;
        $crud->title = $request->title;
        $crud->description = $request->description;
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link).'?status='.$this->list_status.'&service_id='.$this->list_service_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $crud = AdminServiceWisePoint::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
        return redirect(route($this->index_link).'?status='.$this->list_status.'&service_id='.$this->list_service_id);
    }
}
