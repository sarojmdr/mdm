<?php

namespace App\Http\Controllers\admin;

use PrintHelper;
use Illuminate\Http\Request;
use App\Model\admin\AdminHomeIcon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\admin\AdminLoginController;

class AdminHomeIconController extends Controller {

    private $title = 'Home Icon';
    private $sort_by = 'sort_order';
    private $sort_order = 'asc';
    private $index_link = 'homeicon.index';
    private $list_page = 'admin.homeicon.list';
    private $create_form = 'admin.homeicon.add';
    private $update_form = 'admin.homeicon.edit';
    private $link = 'homeicon';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = AdminHomeIcon::orderBy($this->sort_by, $this->sort_order)->get();
        $result = array(
            'list'              => $list,
            'page_header'       => 'Add '.$this->title,
            'link'              => $this->link,
        );
        return view($this->list_page, $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect(route($this->index_link));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         => 'required',
            'icon'         => 'required',
            'description'        => 'required',
        ]);
        $crud = new AdminHomeIcon;
        $crud->title = $request->title;
        $crud->description = $request->description;
        $crud->icon = $request->icon;
        $crud->sort_order = PrintHelper::nextSortOrder('tbl_home_icon');
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = AdminHomeIcon::findOrFail($id);
        $list = AdminHomeIcon::orderBy($this->sort_by, $this->sort_order)->get();
        $result = array(
            'page_header'       => 'Edit '.$this->title.' Detail',
            'record'            => $record,
            'list'              => $list,
            'link'              => $this->link,
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'             => 'required',
            'description'       => 'required',
            'icon'              => 'required',
        ]);
        $crud = AdminHomeIcon::findOrFail($id);
        $crud->title = $request->title;
        $crud->description = $request->description;
        $crud->icon = $request->icon;
        $crud->status = $request->status;
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = AdminHomeIcon::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
        return redirect(route($this->index_link));
    }
}


