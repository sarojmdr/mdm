<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\PrimaryMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PrimaryMenuController extends Controller {

    private $title = 'Primary Menu';
    private $sort_by = 'sort_order';
    private $sort_order = 'asc';
    private $index_link = 'primary-menu.index';
    private $list_page = 'admin.primarymenu.list';
    private $create_form = 'admin.primarymenu.add';
    private $update_form = 'admin.primarymenu.edit';
    private $link = 'primary-menu';
    private $resourcepersonlist;
    private $coordinatorlist;

    public function __construct(){
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = PrimaryMenu::orderBy($this->sort_by, $this->sort_order)->get();
        $result = array(
            'list'                      => $list,
            'page_header'               => 'List of '.$this->title,
            'link'                      => $this->link,
        );
        return view($this->list_page, $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = array(
            'page_header'           => 'Add '.$this->title.' Detail',
            'link'                  => $this->link,
            'resourcepersonlist'    => $this->resourcepersonlist,
            'coordinatorlist'       => $this->coordinatorlist,
        );
        return view($this->create_form, $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'             => 'required',
            'image'             => 'required',
            'description'       => 'required',
            'inserted_date'     => 'required',
        ]);
        $user_id = AdminLoginController::id();
        $crud = new PrimaryMenu;
        $crud->title = $request->title;
        $crud->description = $request->description;
        $crud->co_ordinator = $request->co_ordinator;
        $crud->scan_pay = chunkfullurl($request->scan_pay);
        $crud->image = chunkfullurl($request->image);
        $crud->program_detail = $request->program_detail;
        $crud->inserted_date = $request->inserted_date;
        $crud->created_by = $user_id;
        $crud->status = $request->status;
        $crud->save();
        $crud->resourceperson()->sync($request->resourceperson);
        $crud->coordinator()->sync($request->coordinator);
        Session::flash('success_message', CREATED);
        return redirect(route($this->index_link));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = PrimaryMenu::findOrFail($id);
        $result = array(
            'page_header'           => 'Edit '.$this->title.' Detail',
            'record'                => $record,
            'link'                  => $this->link,
            'resourcepersonlist'    => $this->resourcepersonlist,
            'coordinatorlist'       => $this->coordinatorlist,
        );
        return view($this->update_form, $result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'             => 'required',
            'image'             => 'required',
            'description'       => 'required',
            'inserted_date'     => 'required',
        ]);

        $user_id = AdminLoginController::id();
        $crud = PrimaryMenu::findOrFail($id);
        $crud->title = $request->title;
        $crud->description = $request->description;
        $crud->co_ordinator = $request->co_ordinator;
        $crud->scan_pay = chunkfullurl($request->scan_pay);
        $crud->image = chunkfullurl($request->image);
        $crud->program_detail = $request->program_detail;
        $crud->inserted_date = $request->inserted_date;
        $crud->updated_by = $user_id;
        $crud->status = $request->status;
        $crud->save();
        $crud->resourceperson()->sync($request->resourceperson);
        $crud->coordinator()->sync($request->coordinator);
        $crud->save();
        Session::flash('success_message', UPDATED);
        return redirect(route($this->index_link));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = PrimaryMenu::findOrFail($id);
        $crud->delete();
        Session::flash('success_message', DELETED);
        return redirect(route($this->index_link));
    }


    public function participateList(Request $request){

        $traininglist = PrimaryMenu::orderBy($this->sort_by, $this->sort_order)->get();
        $fullname = $training_id = null;
        if(!empty($_GET)){
            $fullname = $request->input('fullname');
            $training_id = $request->input('training_id');
        }

        $list = PrimaryMenu::getTrainingParticipantList($fullname,$training_id);
       
        $result = array(
            'page_header'       => 'Participating List',
            'traininglist'      => $traininglist,
            'list'              => $list,
            'link'              => $this->link,
        );
        // return $result;
        return view('admin.training.participate.list',$result);
    }

    public function participateDetail($id){
        $record = TrainingParticipant::where('id',$id)->first();
        $record->viewed = 1;
        $record->save();

        $detail = PrimaryMenu::getTrainingParticipantDetail($id);
        $result = array(
            'page_header'       => 'Participating Detail',
            'detail'            => $detail,
            'link'              => $this->link,
        );
        // return $result;
        return view('admin.training.participate.detail',$result);
    }


}