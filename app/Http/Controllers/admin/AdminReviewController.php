<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\AdminCourseTeacher;
use App\Model\admin\AdminCourses;
use App\Model\admin\AdminDifficultyLevel;
use App\Model\admin\AdminReviews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminReviewController extends Controller {

    private $title = 'Reviews';
    private $sort_by = 'created_at';
    private $sort_order = 'desc';
    private $index_link = 'review.index';
    private $list_page = 'admin.reviews.list';
    private $create_form = 'admin.reviews.add';
    private $update_form = 'admin.reviews.edit';
    private $link = 'student';

    public function index($slug)
    {
        $course = AdminCourses::where('slug' , $slug)->first();
        if($course){
            $list = AdminReviews::where('course_id', $course->id)->orderBy($this->sort_by, $this->sort_order)->paginate(PAGES);
            $result = array(
                'list'              => $list,
                'link'              => $this->link,
                'page_header'       => $this->title,
            );
            // return $result;
            return view($this->list_page, $result);
        }else{
            return view('errors.404');
        }
    }
}