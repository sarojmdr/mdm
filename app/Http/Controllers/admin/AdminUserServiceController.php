<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\admin\AdminUserService;
use Illuminate\Http\Request;

class AdminUserServiceController extends Controller {
	private $title = 'User Intrested Service';
    private $sort_by = 'inserted_date';
    private $sort_order = 'desc';
    private $index_link = 'user_service.index';
    private $list_page = 'admin.user_service.list';
    private $link = 'user_service';

	public function list(Request $request){
	    if(!empty($_GET)){
            $status = $request->input('status_search');
            $query = AdminUserService::orderBy($this->sort_by, $this->sort_order);
            if($status != ''){
                $query = $query->where('status', $status);
            }
            $list = $query->paginate(PAGES);
        }else{
            $list = AdminUserService::orderBy($this->sort_by, $this->sort_order)->paginate(PAGES);
        }
	    $result = array(
	        'list'              => $list,
	        'page_header'       => 'List of '.$this->title,
	        // 'link'              => 'newsletter',
	    );
	    return view($this->list_page, $result);
	}

	public function userServiceStatusUpdate($id, Request $request){
        $record = AdminUserService::findOrFail($id);
        $record->status = $request->status;
        $record->save();
        
    }
	

}