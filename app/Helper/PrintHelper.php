<?php
use App\Http\Requests;
use App\Model\admin\AdminCategory;
use App\Model\admin\AdminChapter;
use App\Model\admin\AdminLesson;
use App\Model\admin\AdminReviews;
use App\Model\admin\RelStudentLessonHistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PrintHelper {

    public static function dragDropSorting() {
        $sort_orders = explode(',', Request::get('sort_orders'));
        $ids_order = Request::get('ids_order');


        $ids_order = str_replace('sortable[]=', '', $ids_order);
        $ids_order = substr($ids_order, 1);
        $ids_order = explode('&', $ids_order);

        // pe($ids_order);
        $count = 1;
        for ($i = 0; $i < sizeof($ids_order); $i++) {
            p($ids_order[$i].' x '.$count);
            p(Request::get('table'));
            $dataset = DB::table(Request::get('table'))
                    ->where('id', $ids_order[$i])
                    ->update(array('sort_order' => $count++));

            p($dataset);
        }
    }

    public static function nextSortOrder($table) {
        return DB::table($table)->max('sort_order') + 1;
    }

    public static function nextPageSortOrder($table, $bookid) {
        return DB::table($table)->where('book_id', $bookid)->max('sort_order') + 1;
    }

    public static function updateField() {
        $field_id = strip_tags(trim(Request::get('field_id')));
        $value = strip_tags(trim(Request::get('value')));
        $split_data = explode(':', $field_id);
        $id = $split_data[1];
        $field = $split_data[0];
        if (!empty($id) && !empty($field) && !empty($value)) {
            DB::table(Request::get('table'))
                    ->where('id', $id)
                    ->update(array($field => $value));
        }
    }

    public static function resetSortOrder($table) {
        $models = DB::table($table)->select('id')->orderBy('id', 'asc')->get();
        $i = 1;
        foreach ($models as $m) {
            DB::table($table)->where('id', $m->id)->update(array('sort_order' => $i));
            $i++;
        }
    }
}

function getCategoryNameByID($category_id){
    $category = AdminCategory::where('id',$category_id)->first();
    if($category){
        return $category->title;
    }else{
        return '';
    }
}

function p($data){
    echo "<pre>";
    print_r ($data);
    echo "</pre>";
}

function pe($data){
    echo "<pre>";
    print_r ($data);
    echo "</pre>";
    exit();
}

function str_limit($data,$limit,$end=null){
    $text = Str::limit($data, $limit);
    return $text;
}

function str_slug($data,$seperator=null){
    $seperator = ($seperator == null)?'-':$seperator;
    $text = Str::slug($data, $seperator);
    return $text;
}

function chunkfullurl($fullurl){
    if ($fullurl != '') {
        $imagepath = parse_url($fullurl);
        if (!empty($imagepath['path'])) {
            $urls = array_values(array_filter(explode('/', $imagepath['path'])));
            if(IMAGE_CHUNK_URL == 0){
                if($urls[0] != 'public'){
                    array_shift($urls);
                }
            }
            $chunkurl = implode('/', $urls);
            return $chunkurl;
        } else{
            return $fullurl;
        }
    }else{
        return null;
    }
}

function imageFilePath($fullurl, $image_title){
    $image = $fullurl;
    $extension = $image->getClientOriginalExtension();
    $bannerName = 'images/homepage/'.$image_title.'/'. time(). '.' . $extension;
    $image->move(public_path('images/homepage/'.$image_title), $bannerName);
    return $bannerName;
}

function getNewsletterStatus($status)
{
    if ($status == 1) {
        return NEW_STATUS;
    }elseif($status == 2){
        return ACTIVE_STATUS;
    }else{
        return BLOCKED;
    }
}

function getUserServiceStatus($status)
{
    if ($status == 1) {
        return NEW_STATUS;
    }elseif($status == 2){
        return VIEW_STATUS;
    }else{
        return BLOCKED;
    }
}


function getStatus($id){
    return ($id == 1)?ACTIVE_STATUS:INACTIVE_STATUS;
}

function getImage($image){
    return ($image != '')?asset($image):DEFAULT_IMG;
}

function getYesNoStatus($id){
    return ($id==1)?YES_STATUS:NO_STATUS;
}

function getImageForMobileApp($image){
return ($image != '')?INDEX_URL.'/'.$image:NULL;
}

// for apis

function responseData($status='success',$message='OK',$code='200',$result=null){
    $final = array(
        'status'        => $status,
        'status_code'   => $code,
        'message'       => $message,
        'data'          => $result,
    );

    return response()->json($final,$code);
}

function removeTagsForDescription($data){
    $content = str_replace('&nbsp;', '', str_replace('&rsquo;', "'", $data));
    $info =  preg_replace('/\r\n/i', '', strip_tags(htmlspecialchars_decode($content)));
    return $info;
}

function getApiToken($userid=null){
    if ($userid != '') {
        $encode = Str::Random(40).'-'.$userid.'-'.time();
    }else{
        $encode = Str::Random(50).'-'.uniqid().'-'.time();
    }
    return base64_encode($encode);



}







