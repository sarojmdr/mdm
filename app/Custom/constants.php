<?php
use Carbon\Carbon;
//date
define('CUR_DATE_TIME', Carbon::now());
define('CUR_MONTH', date('m',strtotime(CUR_DATE_TIME)));
define('CUR_YEAR', date('Y',strtotime(CUR_DATE_TIME)));
define('CUR_DAY', date('d',strtotime(CUR_DATE_TIME)));
define('CUR_DATE', CUR_YEAR.'-'.CUR_MONTH.'-'.CUR_DAY);

define('SITE_NAME', 'Constra');

define('RPP', 30);//admin - row per page
define('ABS', '<li>');//bredcrumb sepetator admin
define('BS', '<li>');//bredcrumb sepetator

define('POST', 'trip');//post url.. eg. post, trip, product etc.
define('POSTS', 'trips');//post url.. eg. posts, trips, products etc.

define('COMMENT_LEVEL', '3');



define('NEWSLETTER_CREATED', 'newsletter(s) has been created/sent.');
define('NEWSLETTER_UPDATED', 'newsletter(s) has been updated/sent.');

// nivaj
define('PUBLISH', 'Publish');
define('UNPUBLISH', 'UnPublish');

define('BLOCKED', '<span class="badge badge-danger">Blocked</span>');

//messages
define('CREATED', 'Record has been created.');
define('UPDATED', 'Record has been updated.');
define('DELETED', 'Record has been deleted.');
define('IMPORTED', 'Record has been imported.');
define('COMPLETED', 'task has been created.');
define('SUCCESSFULLY_UPDATED', 'Successfully Updated.');
define('OLD_PASSWORD_MESSAGE', "Old Password Doesn't Match...");
define('PASSWORD_MESSAGE', "Password successfully changed. Please Login With your new password");
define('EMAIL_VERIFICATION', 'The verification link has been sent to your email. Please verify your account.');

define('NO_RECORD', 'NO Record ...');
define('PAGES', '15');

define('ACTIVE_STATUS', '<span class="badge badge-success">Active</span>');
define('NEW_STATUS', '<span class="badge badge-primary">New</span>');
define('VIEW_STATUS', '<span class="badge badge-warning">Viewed</span>');
define('INACTIVE_STATUS', '<span class="badge badge-danger">InActive</span>');


define('YES_STATUS', '<span class="badge badge-success">YES</span>');
define('NO_STATUS', '<span class="badge badge-danger">NO</span>');

define('EDIT_ICON', '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>');
define('DELETE_ICON', '<i class="fa fa-trash" aria-hidden="true"></i>');
define('VIEW_ICON', '<i class="fa fa-eye" aria-hidden="true"></i>');
// define('LINK_ICON', '<i class="fas fa-external-link-alt"></i>');
define('ADD_ICON', '<i class="fa fa-plus" aria-hidden="true"></i>&nbsp; Add New');
define('ARROW_ICON', '<i class="fas fa-arrows-alt"></i>');
define('VIEWLIST_ICON', '<i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp; View List');
define('LINK_ICON', '<i class="fa fa-external-link" aria-hidden="true"></i>');


//array seperator
define('STRING_SEPERATOR', '@@@rraySeper#ator@@@');

if(php_sapi_name() === 'cli' OR defined('STDIN')){
    // This section of the code runs when your application is being runned from the terminal
    $url = 'localhost';
}else{
    // This section of the code run when your app is being run from the browser
    $url = $_SERVER['SERVER_NAME'];
}

if ($url == 'https://newapimpex.com.np/' || $url == 'www.newapimpex.com.np/') {
	define('BASE_URL', 'https://newapimpex.com.np/');
	define('DEFAULT_IMG', 'https://newapimpex.com.np//cnit-r1/public/logo.png');
	define('BANNER_IMG', 'https://newapimpex.com.np//cnit-r1/public/site/images/5.jpg');
	define('BREADCRUMB_IMG', 'https://newapimpex.com.np//cnit-r1/public/uploads/shares/breadcrumb-image.jpg');
	define('INDEX_URL', 'https://newapimpex.com.np/');
	define('IMAGE_CHUNK_URL', 1);
}elseif ($url == 'cnit.com.np' || $url == 'www.cnit.com.np') {
	define('BASE_URL', 'https://'.$url.'/constra-website');
	define('DEFAULT_IMG', 'https://cnit.com.np/constra-website/public/logo.png');
	define('BANNER_IMG', 'https://cnit.com.np/constra-website/public/site/images/5.jpg');
	define('BREADCRUMB_IMG', 'https://cnit.com.np/constra-website/public/uploads/shares/breadcrumb-image.jpg');
	define('INDEX_URL', 'https://cnit.com.np/constra-website');
	define('IMAGE_CHUNK_URL', 0);
}else{
	define('BASE_URL', 'http://'.$url.'/newapimpex');
	define('DEFAULT_IMG', 'http://'.$url.'/newapimpex/public/logo.png');
	define('BANNER_IMG', 'http://'.$url.'/newapimpex/public/site/images/5.jpg');
	define('BREADCRUMB_IMG', 'http://'.$url.'/newapimpex/public/uploads/shares/breadcrumb-image.jpg');
	define('INDEX_URL', 'http://'.$url.'/newapimpex');
	define('IMAGE_CHUNK_URL', 0);
}



// for aPI
define('S', 'success');
define('F', 'fail');
define('DATA_FETCHED', 'Data Fetched');
define('SYSTEM_ERROR', 'Something went Wrong... Please Try Again');


// for sending mail
define('MAIL_ADDRESS', 'nivaj@cnit.com.np');








