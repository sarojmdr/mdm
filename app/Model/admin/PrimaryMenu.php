<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PrimaryMenu extends Model {

    protected $table = 'tbl_primary_menu';
    protected $guarded = ['id'];

}
