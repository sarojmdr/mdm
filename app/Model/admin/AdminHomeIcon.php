<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class AdminHomeIcon extends Model {

    protected $table = 'tbl_home_icon';
    protected $guarded = ['id'];

}
