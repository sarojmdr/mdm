<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminUserService extends Model {

    protected $table = 'tbl_user_service';
    protected $guarded = ['id'];
    public $timestamps = false;
}
