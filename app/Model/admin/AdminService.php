<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class AdminService extends Model {

    protected $table = 'tbl_services';
    protected $guarded = ['id'];
}
