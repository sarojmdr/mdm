<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class AdminWorkingProcess extends Model
{
    protected $table='tbl_working_process';
    protected $guarded=['id'];
}
