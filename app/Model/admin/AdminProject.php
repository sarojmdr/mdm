<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class AdminProject extends Model {
	
    protected $table = 'tbl_project';
    protected $guarded = ['id'];    

    use HasSlug;

    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()
         		->generateSlugsFrom('title')
        		->saveSlugsTo('slug')
        		->doNotGenerateSlugsOnUpdate();
    }

    public function clientDetail(){
        return $this->belongsTo(AdminClient::class, 'client_id', 'id')->select('id', 'title');
    }

    public function ProjectCategoryDetail(){
        return $this->belongsTo(AdminProjectCategory::class, 'project_category_id', 'id')->select('id', 'title');
    }
}
