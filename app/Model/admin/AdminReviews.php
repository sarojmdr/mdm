<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class AdminReviews extends Model {

    protected $table = 'tbl_reviews';
    protected $guarded = ['id'];
}