<?php

namespace App\Model\admin;

use App\Http\Controllers\admin\AdminLoginController;
use App\Model\admin\AdminClient;
use App\Model\admin\AdminContact;
use App\Model\admin\AdminNewsletter;
use App\Model\admin\GetStarted;
use App\Model\admin\Testimonial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminSetting extends Model {

	protected $table = 'tbl_site_setting';
    protected $guarded = ['id'];

	public static function getPrimaryMenuListById($id){
        $data = DB::table('tbl_menu_items')
                ->where('menu',$id)
                ->where('parent',0)
                ->select('id','label','link','parent','class')
                ->orderby('sort','asc')
                ->get();
        return $data;
    }

    public static function getChildMenuListById($menuid){
        $data = DB::table('tbl_menu_items')
                ->where('parent',$menuid)
                ->select('id','label','link','parent','class')
                ->orderby('sort','asc')
                ->get();

        if (!empty($data)) {
            foreach ($data as $k => $val) {
                $child = Self::getChildMenuListById($val->id);
                $data[$k]->child = $child;
            }
        }
        return $data;
    }



	public static function getMenuNameById($menuid){
        $data = DB::table('tbl_menus')
                // ->where('status','1')
                ->where('id',$menuid)
                ->select('id','name')
                ->first();
        return $data;
    }

    public static function getClientRequest($status){
    	$data = GetStarted::where('status', $status)->count();
    	return $data;

    }

    public static function getNewsletter($status){
    	$data = AdminNewsletter::where('status', $status)->count();
    	return $data;
    }

    public static function getProjects(){
    	$data = AdminProject::where('status', 1)->count();
    	return $data;
    }

    public static function getFeedback($viewed){
    	$data = AdminContact::where('viewed', $viewed)->count();
    	return $data;
    }

    public static function getTestimonial(){
    	$data = Testimonial::where('status', 1)->count();
    	return $data;
    }



}
