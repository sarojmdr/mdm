<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class GetStarted extends Model {

    protected $table = 'tbl_get_started';
    protected $guarded = ['id'];
}