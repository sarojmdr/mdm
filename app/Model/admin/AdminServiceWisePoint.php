<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class AdminServiceWisePoint extends Model {

    protected $table = 'tbl_service_wise_point';
    protected $guarded = ['id'];
    public $timestamps = false;

}
