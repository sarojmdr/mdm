<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class NewsletterSubscription extends Model {

    protected $table = 'tbl_newsletter_subscription';
    protected $guarded = ['id'];
    public $timestamps = false;

    use HasSlug;
    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()
                ->generateSlugsFrom('title')
                ->saveSlugsTo('slug')
                ->doNotGenerateSlugsOnUpdate();
    }

    public function adminuser(){
        return $this->hasOne(AdminUser::class, 'id', 'created_by');
    }
}
