<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class AdminWebService extends Model {

    protected $table = 'tbl_web_services';
    protected $guarded = ['id'];
}
