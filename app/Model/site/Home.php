<?php
namespace App\Model\site;

use App\Model\admin\Team;
use App\Model\admin\AdminPosts;
use App\Model\admin\AdminSlider;
use App\Model\admin\Testimonial;
use App\Model\admin\AdminProject;
use App\Model\admin\AdminService;
use App\Model\admin\AdminCategory;
use App\Model\admin\AdminHomeIcon;
use App\Model\admin\AdminOurValue;
use Illuminate\Support\Facades\DB;
use App\Model\admin\AdminWebService;
use App\Model\admin\AdminAboutContent;
use Illuminate\Database\Eloquent\Model;
use App\Model\admin\AdminWorkingProcess;
use App\Model\admin\AdminProjectCategory;
use App\Model\admin\RelationPostCategory;

class Home extends Model {

    public static function getSliderDetail(){
        $sliders = AdminSlider::where('status', 1)
                    ->orderBy('sort_order', 'asc')
                    ->get();
        return $sliders;
    }

    public static function getAboutUsContent(){
        $about_content = AdminAboutContent::where('status', 1)
                        ->orderBy('sort_order', 'asc')
                        ->get();
        return $about_content;
    }

    public static function getWorkingProcessList(){
        $working_process = AdminWorkingProcess::where('status', 1)
                        ->orderBy('sort_order', 'asc')
                        ->get();
        return $working_process;
    }

    public static function getHomeIcon(){
        $home_icons = AdminHomeIcon::where('status', 1)
                        ->orderBy('sort_order', 'asc')
                        ->get();
        return $home_icons;
    }

    public static function getServiceList(){
    $services = AdminService::where('status', 1)
                    ->orderBy('sort_order', 'asc')
                    ->get();
    return $services;
    }
    public static function getWebServiceList(){
        $web_services = AdminWebService::where('status', 1)
                        ->orderBy('sort_order', 'asc')
                        ->get();
        return $web_services;
    }

    // public static function getServiceDetail($limit=0, $offset=0){
    //     $service = AdminService::where('status', 1)
    //                 ->select('id', 'icon', 'title', 'short_description', 'image', 'slug')
    //                 ->orderBy('sort_order', 'asc');
    //     if($limit == 0){
    //         $service = $service->paginate(12);
    //     }else{
    //         $service = $service->limit($limit)
    //                     ->offset($offset)
    //                     ->get();
    //     }

    //     return $service;
    // }



    public static function getClientDetail($limit=12){
        $data = DB::table('tbl_client')
                ->where('status',1)
                ->select('title','logo','link')
                ->orderBy('sort_order', 'asc')
                ->limit($limit)
                ->get();
        return $data;
    }

    public static function getProjectDetail($limit=10){
        $projects = AdminProject::where('status', 1)
                        ->orderBy('sort_order', 'asc')
                        ->get();
        return $projects;
    }
     public static function getAllProjectDetail(){
        $data = DB::table('tbl_project as tp')
                ->join('tbl_project_category as tpc', 'tpc.id', 'tp.project_category_id')
                ->where('tp.status',1)
                ->select('tp.id', 'tp.title', 'tp.slug', 'tp.image', 'tp.project_category_id', 'tpc.title as project_category')
                // ->limit($limit)
                ->get();
        return $data;
    }

    public static function getFooterService($limit = 5){
        $data = DB::table('tbl_services')
                // ->select('id', 'title', 'slug')
                ->where('status',1)
                // ->where('in_footer', 1)
                // ->orderBy('sort_order', 'asc')
                ->limit($limit)
                ->get();
        return $data;
    }

    public static function getTestimonialList(){
        $data = DB::table('tbl_testimonial')
                ->where('status',1)
                ->select('name','designation','company_name','description','image','url')
                ->orderBy('sort_order', 'asc')
                ->get();
        return $data;
    }

    public static function getPostDetail($category_slug = ''){

        $data = AdminPosts::with('category', 'authordata');
        // if($category_slug != ''){
        //     $data->whereHas('category', function($q) use($category_slug){
        //             $q->where('slug',  $category_slug);
        //         });
        // }
        // else{
        //     $data ->whereHas('category', function($q) use ($category_slug){
        //             $q->whereIn('slug',  ['news', 'blog']);
        //         });
        // }
        $data = $data->where('status', 1)
                ->orderBy('published_date', 'desc')
                ->paginate(3);
        return $data;
    }

    public static function getPostDetailInfo($post_slug){

        $data = AdminPosts::with('category', 'authordata')
                ->where('slug', $post_slug)
                ->where('status', 1)
                ->orderBy('published_date', 'desc')
                ->first();
        return $data;
    }

    public static function getPostDetailWithSlug($category_slug = ''){

        $data = AdminPosts::with('category', 'authordata');
        if($category_slug != ''){
            $data->whereHas('category', function($q) use($category_slug){
                    $q->where('slug',  $category_slug);
                });
        }
        // else{
        //     $data ->whereHas('category', function($q) use ($category_slug){
        //             $q->whereIn('slug',  ['news', 'blog']);
        //         });
        // }
        $data = $data->where('status', 1)
                ->orderBy('published_date', 'desc')
                ->paginate(3);
        return $data;
    }

    public static function getPostCategory(){
        $category = AdminCategory::where('status', 1)
                    // ->whereIn('slug', ['news', 'blog'])
                    ->orderBy('title')
                    ->get();
                    // ->random(5);
        return $category;
    }

    // public static function getRecentPost($limit=10){
    //     $data = DB::table('tbl_posts as P')
    //             ->join('rel_post_category as PC','PC.post_id','=','P.id')
    //             ->join('tbl_category as C','PC.category_id','=','C.id')
    //             ->where('C.slug', 'news')
    //             ->where('P.status',1)
    //             ->select('P.title','P.slug','P.image','P.published_date')
    //             ->orderBy('P.published_date','desc')
    //             ->limit($limit)
    //             ->get();
    //     return $data;
    // }

    // public static function getCategoryIdPostId($postid){
    //     $data = DB::table('tbl_category as C')
    //             ->join('rel_post_category as PC','PC.category_id','=','C.id')
    //             ->where('PC.post_id',$postid)
    //             ->where('C.status',1)
    //             ->select('C.id','C.title','C.slug','C.show_date')
    //             ->first();
    //     return $data;
    // }

    // public static function getRelatedPostListByCategoryId($categoryid,$postid,$limit=10){
    //     $fields = array(
    //         'P.id',
    //         'P.title',
    //         'P.slug',
    //         'P.image',
    //         'P.published_date',
    //     );
    //     $data = DB::table('tbl_posts AS P')
    //             ->join('rel_post_category as PC','PC.post_id','=','P.id')
    //             ->where('P.id','!=',$postid)
    //             ->where('P.status','1')
    //             ->where('PC.category_id',$categoryid)
    //             ->orderBy('P.published_date','desc')
    //             ->select($fields)
    //             ->limit($limit)
    //             ->get();
    //     return $data;
    // }

    public static function getPostList($limit=10){
        $fields = array(
            'P.id',
            'P.title',
            'P.slug',
            'P.description',
            'P.short_description',
            'P.image',
            'P.published_date',
            'P.author_id',
        );
        $data = DB::table('tbl_posts AS P')
                ->where('P.status','1')
                ->orderBy('P.published_date','desc')
                ->select($fields)
                ->limit($limit)
                ->get();
        foreach ($data as $key => $item) {
            $category_ids = RelationPostCategory::where('post_id',$item->id)->get('category_id');

            $data[$key]->categories = $category_ids;

        }
        return $data;
    }

    public static function updatePostsViewCount($postid){
        DB::table('tbl_posts')->where('id', $postid)->increment('viewcount', 1);
    }
    public static function updateServiceViewCount($postid){
        DB::table('tbl_services')->where('id', $postid)->increment('viewcount', 1);
    }

    public static function incrementViewCount(){
        DB::table('tbl_site_setting')->where('id', '1')->increment('total_view', 1);
    }

    public static function updatePageViewCount($pageid){
        DB::table('tbl_pages')->where('id', $pageid)->increment('viewcount', 1);
    }

    public static function getPageDetail($slug){
        $data = DB::table('tbl_pages')
                ->select('id','slug','image','published_date','title','description', 'meta_keywords', 'meta_description')
                ->where('slug',$slug)
                ->where('status','1')
                ->first();
        return $data;
    }

    public static function getTeamList(){
        $data = Team::where('status','1')
                ->orderBy('sort_order', 'asc')
                ->get();
        return $data;
    }

     public static function getFaqList(){
        $data = DB::table('tbl_faq')
                ->where('status','1')
                ->select('title','description')
                ->orderBy('sort_order', 'asc')
                ->get();
        return $data;
    }

    // unused









    public static function getMenuList($menu){
        $menu_list = DB::table('tbl_menu_items')
                    ->where('menu', 12)
                    ->orderBy('sort', 'asc')
                    ->get();
        return $menu_list;
    }
















}
