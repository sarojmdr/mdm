@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', $sitedetail->logo)
@section('fb_url', Request::url())
@section('title', $sitedetail->title)


@section('breadcrumb')
<!-- HERO
================================================== -->
<section class="page-banner-area banner-image" style=" background: url({{ asset($sitedetail->banner_image) }}) fixed 50% 50% no-repeat;">
    <div class="overlay"></div>
    <!-- Content -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-12 col-12 text-center">
                <div class="page-banner-content">
                     <h1 class="display-4 font-weight-bold">{{ "404 Page" }}</h1>
                    <nav >
                        <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ "404 Page" }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection

@section('content')
<section id="main-container" class="main-container" >
  <div class="container mt-5">

    <div class="row">

      <div class="col-12">
        <div class="error-page text-center">
          <div class="error-code">
            <h1><strong>404</strong></h1>
          </div>
          <div class="error-message">
            <h2>Oops... Page Not Found!</h2>
          </div>
          <div class="error-body">
            Try using the button below to go to main page of the site <br>
            <a href="{{ route('index') }}" class="btn btn-primary mb-5" >Back to Home Page</a>
          </div>
        </div>
      </div>

    </div><!-- Content row -->
  </div><!-- Conatiner end -->
</section><!-- Main container end -->
@endsection
