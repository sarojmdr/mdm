@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.create') }}">{!! ADD_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="sortable" class="table table-striped table-hover table-sm todo-list ui-sortable" >
                <thead class="bg-primary">
                    <tr class="nodrag nodrop">
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th class="text-center">Project Date</th>
                        <th class="text-center">Image</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $sort_orders = ''; ?>
                    @if(!$list->isEmpty())
                    @foreach ($list as $item)
                    <?php $sort_orders .= $item->sort_order . ','; ?>
                    <tr id="{{ $item->id }}">
                        <td class="move">
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                        </td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->short_description }}</td>
                        <td class='text-center'>{{ $item->published_date }}</td>
                        <td class="text-center"><a href="{{ asset($item->image) }}" target="_blank">{!! LINK_ICON !!}</a></td>
                        <td class="text-center">{!! getStatus($item->status) !!}</td>
                        <td class="text-center">
                            <a href="{{ route($link.'.edit', $item->id) }}"> {!! EDIT_ICON !!}</a>&nbsp;|
                            <a href="{{ route($link.'.delete', $item->id) }}" class="resetbtn">{!! DELETE_ICON !!} </a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr class="text-center">
                        <td colspan="7">{!! NO_RECORD !!}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if($list)
            {{  $list->links() }}
        @endif
    </div>
</div>
@push('script')
    <script type="text/javascript">
        $('#sortable').tableDnD({
        onDrop: function (table, row) {
            $.post("{{ route('ajax.sorting') }}", {ids_order: $.tableDnD.serialize(), sort_orders: '<?php echo $sort_orders; ?>', table: 'tbl_feature', _token: '{!! csrf_token() !!}'}, function(data){
                console.log(data);
            });
        }
    });
    </script>
@endpush
@endsection