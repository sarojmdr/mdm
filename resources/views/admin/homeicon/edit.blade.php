@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="row">
    <div class="col-md-9">
        @include('admin.homeicon.table')
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">{{ $page_header }}
                <div class="card-header-actions">
                    <a class="card-header-action btn btn-warning" href="{{ route($link.'.create') }}">{!! ADD_ICON !!}</a>
                </div>
            </div>
            <div class="card-body">
                <form class="" method="POST" action="{{ route($link.'.update', $record->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label for="title">Title (English) <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ $record->title }}">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description">Description<span class="text-danger">*</span></label>
                        <textarea type="text" name="description" class="form-control" id="description" >{{ $record->description }}</textarea>
                        @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="icon">Icon<span class="text-danger">*</span></label>
                        <small>[Note: Go to https://themify.me/themify-icons to select icons. e.g. ti-book]</small>
                        <input type="text" class="form-control" id="icon" name="icon" value="{{ old('icon') ?? $record->icon }}">
                        @if ($errors->has('icon'))
                            <span class="help-block">
                                <strong>{{ $errors->first('icon') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="statusid" class="form-control">
                            <option value="1" @if($record->status == '1') {{ 'selected' }} @endif >{!! PUBLISH !!}</option>
                            <option value="0" @if($record->status == '0') {{ 'selected' }} @endif >{!! UNPUBLISH !!}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('script')
    <script type="text/javascript">
         function img_pathUrl(input, $id){
            $('#'+$id)[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);

        }
    </script>
@endpush
@endsection
