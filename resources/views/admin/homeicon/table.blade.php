<div class="card">
    <div class="card-header">List of Home Icon</div>
    <div class="card-body">
        <table id="sortable" class="table table-hover table-sm dataTablePagination compact" >
            <thead class="bg-primary">
                <tr class="nodrag nodrop">
                    <th><span class="handle">{!! ARROW_ICON !!}</span></th>
                    <th>Title</th>
                    <th class="text-center">Icon</th>
                    <th class="text-center">Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $sort_orders = ''; ?>
                @foreach ($list as $item)
                    <?php $sort_orders .= $item->sort_order . ','; ?>
                    <tr id="{{ $item->id }}">
                        <td class="move">
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                        </td>
                        <td>{{ $item->title }}</td>
                        <td class="text-center"> <i class="{{ $item->icon }}"></i></td>
                        <td class="text-center">{!! getStatus($item->status) !!}</td>
                        <td width="100px" class="center-align">
                            <a href="{{ route($link.'.edit', $item->id) }}">{!! EDIT_ICON !!} </a>&nbsp;|&nbsp;
                            <a href="{{ route($link.'.delete', $item->id) }}" class="resetbtn">{!! DELETE_ICON !!}</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>
