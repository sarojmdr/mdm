@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
   {{-- <div class="card">
	<div class="card-header">{{ $page_header }}</div>
	<div class="card-body">
	    <h4>Welcome to Dashboard</h4>
	</div>
</div> --}}
   <div class="main-top-dashboard ">
      <div class="row no-gutters">
         {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 dashboard-list">
                <div class="card project-card">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="project-content">
                                <a href="{{ route('request_client.index') }}">
                                	<h6>Client Request</h6>
	                                <ul>
	                                     <li>
	                                        <strong>New Client Request</strong>
	                                        <span class='new_client_request'>{{ $new_client_request }}</span>

	                                    </li>
	                                    <li>
	                                        <strong>Viewed Client Request</strong>
	                                        <span class='view_client_request'>{{ $view_client_request }}</span>

	                                    </li>
	                                    <li>
	                                        <strong>Blocked Client Request</strong>
	                                        <span class='blocked_client_request'>{{ $blocked_client_request }}</span>

	                                    </li>

	                                </ul>
	                            </a>
                            </div>

                            <div class="ml-auto">
                                <div class="dashboard-icon icon-1">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
         <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 dashboard-list">
            <div class="card project-card">
               <div class="card-body">
                  <div class="d-flex">
                     <div class="project-content">
                        <a href="{{ route('newsletter.index') }}">
                           <h6>Newsletter</h6>
                           <ul>
                              <li>
                                 <strong>New </strong>
                                 <span>{{ $new_newsletter }}</span>
                              </li>
                              <li>
                                 <strong>Active</strong>
                                 <span>{{ $active_newsletter }}</span>
                              </li>
                              <li>
                                 <strong>Blocked</strong>
                                 <span>{{ $blocked_newsletter }}</span>
                              </li>
                           </ul>
                        </a>
                     </div>

                     <div class="ml-auto">
                        <div class="dashboard-icon icon-3">
                           <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 dashboard-list">
            <div class="card project-card">
               <div class="card-body">
                  <div class="d-flex">
                     <div class="project-content">
                        <a href="{{ route('project.index') }}">
                           <h6>Projects</h6>
                           <ul>
                              <li>
                                 <strong>Total</strong>
                                 <span>{{ $projects }}</span>
                              </li>
                           </ul>
                        </a>
                     </div>

                     <div class="ml-auto">
                        <div class="dashboard-icon icon-2">
                           <i class="fa fa-cubes" aria-hidden="true"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 dashboard-list">
            <div class="card project-card">
               <div class="card-body">
                  <div class="d-flex">
                     <div class="project-content">
                        <a href="{{ route('testimonial.index') }}">
                           <h6>Testimonial</h6>
                           <ul>
                              <li>
                                 <strong>Total</strong>
                                 <span>{{ $testimonial }}</span>
                              </li>
                           </ul>
                        </a>
                     </div>

                     <div class="ml-auto">
                        <div class="dashboard-icon icon-2">
                           <i class="fa fa-users" aria-hidden="true"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 dashboard-list">
            <div class="card project-card">
               <div class="card-body">
                  <div class="d-flex">
                     <div class="project-content">
                        <a href="{{ route('contact.index') }}" target="_blank">
                           <h6>FeedBack</h6>
                           <ul>
                              <li>
                                 <strong>New</strong>
                                 <span>{{ $new_feedback }}</span>

                              </li>
                              <li>
                                 <strong>Viewed</strong>
                                 <span>{{ $viewed_feedback }}</span>

                              </li>
                              <li>

                              </li>
                           </ul>
                        </a>
                     </div>

                     <div class="ml-auto">
                        <div class="dashboard-icon icon-3">
                           <i class="fa fa-comment" aria-hidden="true"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

@endsection
