@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)

@section('content')

<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-info confirmbtn" href="{{ route('newsletter.resendmail', $record->id) }}">Send Email</a>
            <a class="card-header-action btn btn-warning" href="{{ route('newsletter.subscription') }}">{!! VIEWLIST_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
        <table id="table-layout-fixed" class="table table-hover table-hover">
            <tr>
                <th width="100px">Title</th>
                <th>{{ $record->title }}</th>
            </tr>
            <tr>
                <th>Subject</th>
                <th>{{ $record->subject }}</th>
            </tr>
            <tr>
                <th>Sent Date</th>
                <th>{{ $record->inserted_date }}</th>
            </tr>
            <tr>
                <th>Created By</th>
                <th>{{ $record->adminuser->username }}</th>
            </tr>
            <tr>
                <th>Mail Status</th>
                <th>{!! $record->mail_status == 1 ? '<span class="badge badge-success">Mailed</span>' : '<span class="badge badge-danger">Not Mailed</span>' !!}</th>
            </tr>
            <tr>
                <th>Description</th>
                <th class="word-wrap-break-word">{!! $record->description !!}</th>
            </tr>
        </table>
    </div>
</div>
@endsection
