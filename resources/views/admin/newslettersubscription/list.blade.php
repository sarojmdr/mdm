@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.create') }}">{!! ADD_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-responsive-lg table-sm table-responsive-xl table-striped table-hover todo-list ui-sortable">
            <thead class="bg-primary">
                <tr class="nodrag nodrop">
                    <th>S.No</th>
                    <th>Title</th>
                    <th>Subject</th>
                    <th>Description</th>
                    <th>Inserted date</th>
                    <th>Mailed</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <?php $count = 1; ?>
            @foreach ($list as $item)
            <tr>
                <td>{{ $count++ }}</td>
                <td>{{ $item->title }}</td>
                <td>{{ $item->subject }}</td>
                <td>{!! str_limit($item->description, 50) !!}</td>
                <td>{{ $item->inserted_date }}</td>
                <td>{!! $item->mail_status == 1 ? '<span class="badge badge-success">Mailed</span>' : '<span class="badge badge-danger">Not Mailed</span>' !!}</td>
                <td class="text-center">
                    <a href="{{ route('newsletter-subscription.show', $item->id) }}"> {!! VIEW_ICON !!} </a>
                </td>
            </tr>
            @endforeach
        </table>
        @if($list)
            {{  $list->links() }}
        @endif
    </div>
</div>
@endsection