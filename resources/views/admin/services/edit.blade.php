@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.index') }}">{!! VIEWLIST_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">

       <form class="" method="POST" action="{{ route($link.'.update', $record->id) }}" enctype='multipart/form-data'>
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="title">Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $record->title }}" >
                                @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="slug">Slug <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="slug" name="slug" value="{{ $record->slug }}">
                                @if ($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label" for="published_date">Published Date {{-- <span class="text-danger">*</span> --}}</label>
                                <input type="text" class="form-control datepicker" id="published_date" name="published_date" value="{{ $record->published_date }}" placeholder="Published Date">
                                @if ($errors->has('published_date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('published_date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                         <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="short_description">Short Description <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="short_description" name="short_description" value="{{ $record->short_description }}" >
                                @if ($errors->has('short_description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('short_description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="description">Description</label>
                        <br>
                        <textarea id="my-editor" class="tinymce" name="description" placeholder="Place some text here" >{{ $record->description }}</textarea>
                        @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                    <div class="form-group">
                        <label class="control-label">Icon Image - Recommended Size (60*60px)</label>
                        @if(!empty($record->icon))
                            <img src="{{ asset($record->icon) }}" alt="" title="" class='fancybox' id="prev_img_icon" />
                        @elseif(!empty(old('icon')))
                            <img src="{{ old('icon') }}" alt="" title="" class='fancybox' id="prev_img_icon" />
                        @else
                            <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_icon" />
                        @endif
                        <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=icon') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
                        <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this,'prev_img_icon', 'icon')">Remove</button>
                        <input type="hidden" value="{{ isset($record->icon)?$record->icon:old('icon') }}"  name="icon" class="form-control" id="icon">
                        <br>
                        @if ($errors->has('icon'))
                            <span class="help-block">
                                <strong>{{ $errors->first('icon') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label">Featured Image - Recommended Size (750*600px)</label>
                        @if(!empty($record->image))
                            <img src="{{ asset($record->image) }}" alt="" title="" class='fancybox' id="prev_img" />
                        @elseif(!empty(old('image')))
                            <img src="{{ old('image') }}" alt="" title="" class='fancybox' id="prev_img" />
                        @else
                            <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img" />
                        @endif
                        <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=image') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
                        <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this,'prev_img', 'image')">Remove</button>
                        <input type="hidden" value="{{ isset($record->image)?$record->image:old('image') }}"  name="image" class="form-control" id="image">
                        <br>
                        @if ($errors->has('image'))
                            <span class="help-block">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="in_footer">show in footer?</label>
                        <select name="in_footer" id="in_footer" class="form-control">
                            <option value="1"  @if($record->in_footer == '1') {{ 'selected' }} @endif>Yes</option>
                            <option value="0"  @if($record->in_footer == '0') {{ 'selected' }} @endif>No</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="statusid" class="form-control">
                            <option value="1" @if($record->status == '1') {{ 'selected' }} @endif>{!! PUBLISH !!}</option>
                            <option value="0" @if($record->status == '0') {{ 'selected' }} @endif >{!! UNPUBLISH !!}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-danger resetbtn">Clear</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
{{-- @push('script')
    <script type="text/javascript">
        function img_pathUrl(input, $id){
            $('#'+$id)[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);
        }
    </script>
@endpush --}}
@endsection
