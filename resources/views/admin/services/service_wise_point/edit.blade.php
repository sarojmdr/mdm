@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
@php
    $service_id = isset($_GET['service_id']) ? $_GET['service_id'] : '';
    $status = isset($_GET['status']) ? $_GET['status'] : '';
@endphp
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.index') }}">{!! VIEWLIST_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
        
       <form class="" method="POST" action="{{ route($link.'.update', $record->id).'?status='.$status.'&service_id='.$service_id }}" enctype='multipart/form-data'>
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="service_id">Service <span class="text-danger">*</span></label>
                                <select name="service_id" class="service_id form-control select2" id="service_id">
                                    <option value=""> Select Service </option>
                                    @foreach($services as $service)
                                        <option value="{{ $service->id }}" @if($record->service_id == $service->id) {{ 'selected' }} @endif >{{ $service->title }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('service_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('service_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="title">Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $record->title }}" placeholder="Enter Title" >
                                @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="description">Short Description <span class="text-danger">*</span></label>
                                <textarea class="form-control" id="description" name="description" placeholder="Enter Description" rows="5"> {{ $record->description }}
                                </textarea>
                                @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                   
                    
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="statusid" class="form-control">
                            <option value="1" @if($record->status == 1) {{ 'selected' }} @endif>{!! PUBLISH !!}</option>
                            <option value="0" @if($record->status == 0) {{ 'selected' }} @endif >{!! UNPUBLISH !!}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-danger resetbtn">Clear</button>
                    </div>
                </div>
            
            </div>
        </form>
    </div>
</div>
{{-- @push('script')
    <script type="text/javascript">
        function img_pathUrl(input, $id){
            $('#'+$id)[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);
        }
    </script>
@endpush --}}
@endsection