@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
@php
    $service_id = isset($_GET['service_id']) ? $_GET['service_id'] : '';
    $status = isset($_GET['status']) ? $_GET['status'] : '';
@endphp
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.create') }}">{!! ADD_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
        <form class="" method="GET">
            <div class="row">
                 <div class="col-md-3">
                    <div class="form-group">
                        <label>Search By Service</label>
                        <select class="form-control" name="service_id" id="service_id">
                            <option value="">Search By Service</option>
                            @foreach($services as $service)
                                <option value="{{ $service->id }}" @if(isset($_GET['service_id']) && $_GET['service_id'] == $service->id) {{ 'selected' }} @endif>
                                    {{ $service->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Search By Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="">Search By Status</option>
                            <option value="0" @if(isset($_GET['status']) && $_GET['status'] == '0') {{ 'selected' }} @endif>{{ 'Inactive' }}</option>
                            <option value="1" @if(isset($_GET['status']) && $_GET['status'] == '1') {{ 'selected' }} @endif>{{ 'Active' }}</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="vi-hidden">x</label>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{ route($link.'.index')}} " class="btn btn-danger">Reset</a>
                    </div>
                </div>
            </div>
        </form>
        <table id="sortable" class="table table-hover table-sm compact" >
            <thead class="bg-primary">
                <tr class="nodrag nodrop">
                    <th>
                        <span class="handle">
                            <i class="fas fa-arrows-alt"></i>
                        </span>
                    </th>
                    <th>Title</th>
                    <th>Description</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $sort_orders = ''; ?>
                @if(!$list->isEmpty())
                @foreach ($list as $item)
                <?php $sort_orders .= $item->sort_order . ','; ?>
                <tr id="{{ $item->id }}" >
                    <td class="move">
                        <span class="handle">
                            <i class="fa fa-ellipsis-v"></i>
                            <i class="fa fa-ellipsis-v"></i>
                        </span>
                    </td>
                    <td>{{ $item->title }}</td>
                    <td>{!! str_limit($item->description,100) !!}</td>
                    <td class="text-center">{!! getStatus($item->status) !!}</td>
                    <td class="text-center">
                        <a href="{{ route($link.'.edit', $item->id).'?status='.$status.'&service_id='.$service_id }}"> {!! EDIT_ICON !!}</a>&nbsp;|
                        <a href="{{ route($link.'.delete', $item->id).'?status='.$status.'&service_id='.$service_id }}" class="resetbtn">{!! DELETE_ICON !!} </a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="text-center">
                    <td colspan="7">{!! NO_RECORD !!}</td>
                </tr>
                @endif
            </tbody>
        </table>
        @if($list)
            {{  $list->appends(Request::only('service_id'))
                ->appends(Request::only('status'))
                ->links()
            }}
        @endif
        
    </div>
</div>
@push('script')
    <script type="text/javascript">
        $('#sortable').tableDnD({
        onDrop: function (table, row) {
            $.post("{{ route('ajax.sorting') }}", {ids_order: $.tableDnD.serialize(), sort_orders: '<?php echo $sort_orders; ?>', table: 'tbl_service_wise_point', _token: '{!! csrf_token() !!}'}, function(data){
                console.log(data);
            });
        }
    });
    </script>
@endpush
@endsection