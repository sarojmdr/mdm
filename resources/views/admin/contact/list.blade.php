@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
@php
    $viewed = isset($_GET['viewed']) ? $_GET['viewed'] : '';
@endphp
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
        </div>
    </div>
    <div class="card-body">
        <form class="" method="GET">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Search By Status</label>
                        <select class="form-control" name="viewed" id="viewed">
                            <option value="">Search By Status</option>
                            <option value="0" @if(isset($_GET['viewed']) && $_GET['viewed'] == 0) {{ 'selected' }} @endif>{{ 'New' }}</option>
                            <option value="1" @if(isset($_GET['viewed']) && $_GET['viewed'] == 1) {{ 'selected' }} @endif>{{ 'Active' }}</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="vi-hidden">x</label>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{ route('contact.index')}} " class="btn btn-danger">Reset</a>
                    </div>
                </div>
            </div>
        </form>
        <div class="table-responsive">
            <table id="sortable" class="table table-striped table-sm table-hover todo-list ui-sortable" >
                <thead class="bg-primary">
                    <tr class="nodrag nodrop">
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Message</th>
                        <th>Ip Address</th>
                        <th>Sent date</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <?php $count = 1; ?>
                @foreach ($list as $item)
                <tr>
                    <td>{{ $count++ }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->address }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->phoneno }}</td>
                    <td>{{ str_limit($item->message,50) }}</td>
                    <td>{{ $item->ip_address }}</td>
                    <td>{{ $item->inserted_date }}</td>
                    <td class="text-center">
                        @if ($item->viewed == '0')
                        <a href="{{ route('contact.edit', $item->id).'?viewed='.$viewed }}"> {!!NEW_STATUS !!} </a>
                        @else
                        {!! VIEW_STATUS !!}
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
            @if($list)
                {{  $list->appends(Request::only('viewed'))
                    ->links()
                }}
            @endif
    </div>
</div>
@endsection