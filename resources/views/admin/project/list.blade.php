@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
@php
    $title = isset($_GET['title']) ? $_GET['title'] : '';
    $status = isset($_GET['status']) ? $_GET['status'] : '';
@endphp
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.create') }}">{!! ADD_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
         <form class="" method="GET">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Search By Title</label>
                        <input type="text" name="title" value="{{ $title }}" class="form-control" placeholder="Search By Title">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Search By Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="">Search By Status</option>
                            <option value="0" @if(isset($_GET['status']) && $_GET['status'] == '0') {{ 'selected' }} @endif>{{ 'Inactive' }}</option>
                            <option value="1" @if(isset($_GET['status']) && $_GET['status'] == '1') {{ 'selected' }} @endif>{{ 'Active' }}</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label class="vi-hidden">x</label>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{ route($link.'.index')}} " class="btn btn-danger">Reset</a>
                    </div>
                </div>
            </div>
        </form>
        <table id="sortable" class="table table-hover table-sm compact" >
            <thead class="bg-primary">
                <tr class="nodrag nodrop">
                    <th>
                        <span class="handle">
                            <i class="fas fa-arrows-alt"></i>
                        </span>
                    </th>
                    <th>Title</th>
                    <th>Project Category</th>
                    <th>Client</th>
                    <th>Website</th>
                    <th>Services</th>
                    <th>Description</th>
                    <th class="text-center">Project Date</th>
                    <th class="text-center">Image</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $sort_orders = ''; ?>
                @if(!$list->isEmpty())
                @foreach ($list as $item)
                <?php $sort_orders .= $item->sort_order . ','; ?>
                <tr id="{{ $item->id }}" >
                    <td class="move">
                        <span class="handle">
                            <i class="fa fa-ellipsis-v"></i>
                            <i class="fa fa-ellipsis-v"></i>
                        </span>
                    </td>
                    <td>{{ $item->title }}</td>

                    <td>{{ $item->category }}</td>
                    <td>{{ $item->client }}</td>
                    <td>{{ $item->website }}</td>
                    <td>{{ $item->services }}</td>
                    <td>{!! strip_tags(str_limit($item->description,80)) !!}</td>
                    <td class='text-center'>{{ substr($item->project_date,0,10) }}</td>
                    <td class="text-center">
                        <a href="{{ asset($item->image) }}" target="_blank">
                            {!! VIEW_ICON !!}
                        </a>
                    </td>
                    <td class="text-center">{!! getStatus($item->status) !!}</td>
                    <td class="text-center">
                        <a href="{{ route($link.'.edit', $item->id)}}"> {!! EDIT_ICON !!}</a>&nbsp;|
                        <a href="{{ route($link.'.delete', $item->id) }}" class="resetbtn">{!! DELETE_ICON !!} </a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="text-center">
                    <td colspan="7">{!! NO_RECORD !!}</td>
                </tr>
                @endif
            </tbody>
        </table>
        @if($list)
            {{  $list->appends(Request::only('title'))
                ->appends(Request::only('project_category_id'))
                ->appends(Request::only('status'))
                ->appends(Request::only('client_id'))
                ->links()
            }}
        @endif

    </div>
</div>
@push('script')
    <script type="text/javascript">
        $('#sortable').tableDnD({
        onDrop: function (table, row) {
            $.post("{{ route('ajax.sorting') }}", {ids_order: $.tableDnD.serialize(), sort_orders: '<?php echo $sort_orders; ?>', table: 'tbl_project', _token: '{!! csrf_token() !!}'}, function(data){
                console.log(data);
            });
        }
    });
    </script>
@endpush
@endsection
