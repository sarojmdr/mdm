@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
@php
    $project_category_id = isset($_GET['project_category_id']) ? $_GET['project_category_id'] : '';
    $client_id = isset($_GET['client_id']) ? $_GET['client_id'] : '';
    $title = isset($_GET['title']) ? $_GET['title'] : '';
    $status = isset($_GET['status']) ? $_GET['status'] : '';
@endphp
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.index') }}">{!! VIEWLIST_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">

       <form class="" method="POST" action="{{ route($link.'.update', $record->id).'?project_category_id='.$project_category_id.'&client_id='.$client_id.'&title='.$title.'&status='.$status }}" enctype='multipart/form-data'>
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="title">Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $record->title }}" placeholder="Enter Title" >
                                @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="slug">Slug <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="slug" name="slug" value="{{ $record->slug }}" placeholder="Enter slug" >
                                @if ($errors->has('slug'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('slug') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label" for="project_date">Project Date <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datepicker" id="project_date" name="project_date" value="{{ date('Y-m-d') }}" placeholder="Project Date">
                                @if ($errors->has('project_date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('project_date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="client">Client <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="client" name="client" value="{{ old('client') ?? $record->client }}" >
                                @if ($errors->has('client'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('client') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="category">Category <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="category" name="category" value="{{ old('category') ?? $record->category }}" >
                                @if ($errors->has('category'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="website">Website <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="website" name="website" value="{{ old('website') ?? $record->website}}" >
                                @if ($errors->has('website'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('website') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="services">Services <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="services" name="services" value="{{ old('services') ?? $record->services }}" >
                                @if ($errors->has('services'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('services') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="description">Description</label>
                        <br>
                        <textarea id="my-editor" class="tinymce" name="description" placeholder="Place some text here" >{{ $record->description }}</textarea>
                        @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Featured Image ( Recommended Size : 900*600px) <span class="text-danger">*</span></label>
                        @if(!empty($record->image))
                            <img src="{{ asset($record->image) }}" alt="" title="" class='fancybox' id="prev_img" />
                        @elseif(!empty(old('image')))
                            <img src="{{ old('image') }}" alt="" title="" class='fancybox' id="prev_img" />
                        @else
                            <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img" />
                        @endif
                        <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=image') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
                        <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this,'prev_img', 'image')">Remove</button>
                        <input type="hidden" value="{{ isset($record->image)?$record->image:old('image') }}"  name="image" class="form-control" id="image">
                        <br>
                        @if ($errors->has('image'))
                            <span class="help-block">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="form-group">
                        <label for="status">Status</label>
                         <select name="status" id="statusid" class="form-control">
                            <option value="1" @if($record->status == 1) {{ 'selected' }} @endif>{!! PUBLISH !!}</option>
                            <option value="0" @if($record->status == 0) {{ 'selected' }} @endif >{!! UNPUBLISH !!}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-danger resetbtn">Clear</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
{{-- @push('script')
    <script type="text/javascript">
        function img_pathUrl(input, $id){
            $('#'+$id)[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);
        }
    </script>
@endpush --}}
@endsection
