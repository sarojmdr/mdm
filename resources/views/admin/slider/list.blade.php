@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
             <a class="card-header-action btn btn-warning" href="{{ route($link.'.create') }}">{!! ADD_ICON !!}</a>
        </div>
    </div>
    <div class="card-body ">
        <div class="table-responsive">
            <table id="sortable" class="table table-hover table-sm dataTablePagination compact">
                <thead class="bg-primary">
                    <tr class="nodrag nodrop">
                        <th><span class="handle">{!! ARROW_ICON !!}</span></th>
                        <th>Title</th>
                        <th>Sub Title</th>
                        <th class="text-center">Open Image</th>
                        <th class="text-center">Published Date</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        //$count = ($list->currentpage()-1)*$list->perpage()+1; 
                        $sort_orders = '';
                    ?>
                    @if(!empty($list))
                    @foreach ($list as $item)
                    @php $sort_orders .= $item->sort_order . ',';  @endphp
                    <tr id={{ $item->id }}>
                        <td class="move">
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>
                        </td>
                        <td>
                            {{ str_limit($item->title, $limit = 50, $end = '...') }}
                        </td>
                        <td>
                            {{ str_limit($item->sub_title, $limit = 50, $end = '...') }}
                        </td>
                        <td class="text-center"><a href="{{ asset($item->image) }}" target="_blank">{!! LINK_ICON !!}</a></td>
                        <td class="text-center">{{ $item->published_date }}</td>
                        <td class="text-center">{!! getStatus($item->status) !!}</td>
                        <td class="text-center">
                            <a href="{{ route($link.'.edit', $item->id) }}"> {!! EDIT_ICON !!}</a>&nbsp;|
                            <a href="{{ route($link.'.delete', $item->id) }}" class="resetbtn">{!! DELETE_ICON !!} </a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">{!! NO_RECORD !!}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        
       {{--  {{ $list->links() }} --}}
    </div>
</div>
@push('script')
    <script type="text/javascript">
        $('#sortable').tableDnD({
            onDrop: function (table, row) {
                $.post("{{ route('ajax.sorting') }}", {ids_order: $.tableDnD.serialize(), sort_orders: '<?php echo $sort_orders; ?>', table: 'tbl_slider', _token: '{!! csrf_token() !!}'}, function(data){
                    console.log(data);
                });
            }
        });
    </script>
@endpush
@endsection