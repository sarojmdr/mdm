@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
	<form class="" method="POST" action="{{ route('update.setting') }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="card-header">{{ $page_header }}
			<div class="card-header-actions">
				<button type="submit" class="btn btn-success btn-sm">Submit</button>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col">
					<ul class="nav nav-pills mb-1" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="tab-basic-info" data-toggle="pill" href="#pills-basic" role="tab" aria-controls="pills-basic" aria-selected="true">Basic Information</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="tab-social" data-toggle="pill" href="#pills-social" role="tab" aria-controls="pills-social" aria-selected="false">Social</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="tab-metatags" data-toggle="pill" href="#pills-metatags" role="tab" aria-controls="pills-metatags" aria-selected="false">Meta Tags</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" id="tab-others" data-toggle="pill" href="#pills-others" role="tab" aria-controls="pills-others" aria-selected="false">Others</a>
						</li>
					</ul>
					<div class="tab-content" id="pills-tabContent">
						<div class="tab-pane fade show active" id="pills-basic" role="tabpanel" aria-labelledby="tab-basic-info">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="title">Title</label>
										<input type="text" class="form-control" id="title" name="title" value="{{ $settingdata->title }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="address">Address (English)</label>
										<input type="text" class="form-control" id="address" name="address" value="{{ $settingdata->address }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="mobile_no">Mobile Number</label>
										<input type="text" class="form-control" id="mobile_no" name="mobile_no" value="{{ $settingdata->mobile_no }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="phone_no">Phone Number</label>
										<input type="text" class="form-control" id="phone_no" name="phone_no" value="{{ $settingdata->phone_no }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="email">Email</label>
										<input type="text" class="form-control" id="email" name="email" value="{{ $settingdata->email }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="office_hour">Office Hour</label>
										<input type="text" class="form-control" id="office_hour" name="office_hour" value="{{ $settingdata->office_hour }}" placeholder="Eg: 9:00am - 6:00pm">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="office_day">Office Day</label>
										<input type="text" class="form-control" id="office_day" name="office_day" value="{{ $settingdata->office_day }}" placeholder="Eg: Sun - Fri">
									</div>
								</div>
								{{-- <div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="viber">Viber</label>
										<input type="text" class="form-control" id="viber" name="viber" value="{{ $settingdata->viber }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="messenger">Messenger</label>
										<input type="text" class="form-control" id="messenger" name="messenger" value="{{ $settingdata->messenger }}" >
									</div>
								</div> --}}
								{{-- <div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="whatsapp_number">Whatsapp Number</label>
										<input type="text" class="form-control" id="whatsapp_number" name="whatsapp_number" value="{{ $settingdata->whatsapp_number }}" >
									</div>
								</div> --}}


								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="map_detail">Map Area</label>
										<textarea class="form-control" id="map_detail" rows="8" cols="2" name="map_detail">{{ $settingdata->map_detail }}</textarea>
									</div>
								</div>
								{{-- <div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="office_time_txt">Office Time Text</label>
										<textarea class="form-control" id="office_time_txt" rows="8" cols="2" name="office_time_txt">{{ $settingdata->office_time_txt }}</textarea>
									</div>
								</div> --}}
								<div class="col-md-3">
									<div class="form-group">
				                        <label class="control-label">Logo</label>
					                        {{-- @if(!empty($settingdata->logo))
					                        	<img src="{{ url($settingdata->logo) }}"
					                        	alt=""
					                        	class="fancybox"
					                        	title=""
					                        	id="prev_img_logo" />
					                        @else
					                        <img src="{{ asset('admin/images/no-image.png', $secure = null) }}"
					                        alt=""
					                        class="fancybox"
					                        title=""
					                        id="prev_img_logo" />
					                        @endif
				                        <input type="file" value="{{ isset($settingdata->logo)?asset($settingdata->logo):'' }}"
				                        name="logo"
				                        class="form-control"
				                        id="logo"
				                        onChange="img_pathUrl(this, 'prev_img_logo');"> --}}
				                        @if(!empty($sitedetail->logo))
					                        <img src="{{ asset($sitedetail->logo) }}" alt="" title="" class='fancybox' id="prev_img_logo" />
					                    @elseif(!empty(old('logo')))
					                        <img src="{{ old('logo') }}" alt="" title="" class='fancybox' id="prev_img_logo" />
					                    @else
					                        <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_logo" />
					                    @endif
					                    <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=logo') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
					                    <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this, 'prev_img_logo', 'logo')">Remove</button>
					                    {{-- <button class="btn btn-warning prev_box_image" type="button" style="display: none;">Previous Image</button> --}}
					                    <input type="hidden" value="{{ isset($sitedetail->logo)?$sitedetail->logo:old('logo') }}"  name="logo" class="form-control" id="logo">

				                    </div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
				                        <label class="control-label">Fav Icon</label>
				                        @if(!empty($sitedetail->fav_icon))
					                        <img src="{{ asset($sitedetail->fav_icon) }}" alt="" title="" class='fancybox' id="prev_img_image_2" />
					                    @elseif(!empty(old('fav_icon')))
					                        <img src="{{ old('fav_icon') }}" alt="" title="" class='fancybox' id="prev_img_image_2" />
					                    @else
					                        <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_image_2" />
					                    @endif
					                    <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=image_2') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
					                    <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this, 'prev_img_image_2', 'image_2')">Remove</button>
					                    {{-- <button class="btn btn-warning prev_box_image" type="button" style="display: none;">Previous Image</button> --}}
					                    <input type="hidden" value="{{ isset($sitedetail->fav_icon)?$sitedetail->fav_icon:old('fav_icon') }}"  name="fav_icon" class="form-control" id="image_2">

				                    </div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
				                        <label class="control-label">Footer Logo
				                        [Recommended Size : 200*50]</label>
				                        @if(!empty($sitedetail->footer_logo))
					                        <img src="{{ asset($sitedetail->footer_logo) }}" alt="" title="" class='fancybox' id="prev_img_footer_logo" />
					                    @elseif(!empty(old('footer_logo')))
					                        <img src="{{ old('footer_logo') }}" alt="" title="" class='fancybox' id="prev_img_footer_logo" />
					                    @else
					                        <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_footer_logo" />
					                    @endif
					                    <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=footer_logo') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
					                    <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this, 'prev_img_footer_logo', 'footer_logo')">Remove</button>
					                    <input type="hidden" value="{{ isset($sitedetail->footer_logo)?$sitedetail->footer_logo:old('footer_logo') }}"  name="footer_logo" class="form-control" id="footer_logo">

				                    </div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
				                        <label class="control-label">Banner Image</label> [Recommended Size:1350*300]
				                        @if(!empty($sitedetail->banner_image))
					                        <img src="{{ asset($sitedetail->banner_image) }}" alt="" title="" class='fancybox' id="prev_img_image_3" />
					                    @elseif(!empty(old('banner_image')))
					                        <img src="{{ old('banner_image') }}" alt="" title="" class='fancybox' id="prev_img_image_3" />
					                    @else
					                        <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_image_3" />
					                    @endif
					                    <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=image_3') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
					                    <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this, 'prev_img_image_3', 'image_3')">Remove</button>
					                    {{-- <button class="btn btn-warning prev_box_image" type="button" style="display: none;">Previous Image</button> --}}
					                    <input type="hidden" value="{{ isset($sitedetail->banner_image)?$sitedetail->banner_image:old('banner_image') }}"  name="banner_image" class="form-control" id="image_3">

				                    </div>
								</div>
							</div>
						</div>


						<div class="tab-pane fade" id="pills-social" role="tabpanel" aria-labelledby="tab-social">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="facebook">Facebook</label>
										<input type="text" class="form-control" id="facebook" name="facebook" value="{{ $settingdata->facebook }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="twitter">Twitter</label>
										<input type="text" class="form-control" id="twitter" name="twitter" value="{{ $settingdata->twitter }}" >
									</div>
								</div>
								{{-- <div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="google_plus">Google Plus</label>
										<input type="text" class="form-control" id="google_plus" name="google_plus" value="{{ $settingdata->google_plus }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="skype">Skype</label>
										<input type="text" class="form-control" id="skype" name="skype" value="{{ $settingdata->skype }}" >
									</div>
								</div> --}}
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="linkedin">Linkedin</label>
										<input type="text" class="form-control" id="linkedin" name="linkedin" value="{{ $settingdata->linkedin }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="youtube">YouTube</label>
										<input type="text" class="form-control" id="youtube" name="youtube" value="{{ $settingdata->youtube }}" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="instagram">Instagram</label>
										<input type="text" class="form-control" id="instagram" name="instagram" value="{{ $settingdata->instagram }}" >
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
				                        <label class="control-label">FB Image</label>
				                        @if(!empty($sitedetail->fb_image))
					                        <img src="{{ asset($sitedetail->fb_image) }}" alt="" title="" class='fancybox' id="prev_img_icon" />
					                    @elseif(!empty(old('fb_image')))
					                        <img src="{{ old('fb_image') }}" alt="" title="" class='fancybox' id="prev_img_icon" />
					                    @else
					                        <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_icon" />
					                    @endif
					                    <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=icon') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
					                    <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this, 'prev_img_icon', 'icon')">Remove</button>
					                    {{-- <button class="btn btn-warning prev_box_image" type="button" style="display: none;">Previous Image</button> --}}
					                    <input type="hidden" value="{{ isset($sitedetail->fb_image)?$sitedetail->fb_image:old('fb_image') }}"  name="fb_image" class="form-control" id="icon">

				                    </div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="pills-metatags" role="tabpanel" aria-labelledby="tab-metatags">
							<div class="form-group">
								<label class="control-label" for="meta_keywords">Meta Keywords</label>
								<input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="{{ $settingdata->meta_keywords }}" >
							</div>
							<div class="form-group">
								<label class="control-label" for="meta_descriptions">Meta Descriptions</label>
								<textarea class="form-control" id="meta_descriptions" rows="10" cols="20" name="meta_descriptions">{{ $settingdata->meta_descriptions }}</textarea>
							</div>
						</div>

						<div class="tab-pane fade" id="pills-others" role="tabpanel" aria-labelledby="tab-others">
							<div class="row">

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="contact_us_heading">Contact Us Heading</label>
										<input class="form-control" id="contact_us_heading"  name="contact_us_heading" value="{{ $settingdata->contact_us_heading }}">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="banner_heading">Banner Heading</label>
										<input class="form-control" id="banner_heading"  name="banner_heading" value="{{ $settingdata->banner_heading }}">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="contact_us_txt">Contact Us text</label>
										<textarea class="form-control" id="contact_us_txt" rows="8" cols="2" name="contact_us_txt">{{ $settingdata->contact_us_txt }}</textarea>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="banner_txt">Banner text</label>
										<textarea class="form-control" id="banner_txt" rows="8" cols="2" name="banner_txt">{{ $settingdata->banner_txt }}</textarea>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="working_process_heading">Working Process Heading</label>
										<input class="form-control" id="working_process_heading"  name="working_process_heading" value="{{ $settingdata->working_process_heading }}">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="service_heading">Service Heading</label>
										<input class="form-control" id="service_heading"  name="service_heading" value="{{ $settingdata->service_heading }}">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="working_process_text">Working Process Text</label>
										<textarea class="form-control" id="working_process_text" rows="8" cols="2" name="working_process_text">{{ $settingdata->working_process_text }}</textarea>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="service_txt">Service Text</label>
										<textarea class="form-control" id="service_txt" rows="8" cols="2" name="service_txt">{{ $settingdata->service_txt }}</textarea>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="web_service_heading">Web Services Heading</label>
										<input class="form-control" id="web_service_heading"  name="web_service_heading" value="{{ $settingdata->web_service_heading }}">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="blog_heading">Blog Heading</label>
										<input class="form-control" id="blog_heading"  name="blog_heading" value="{{ $settingdata->blog_heading }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="web_service_txt">Web Services Text</label>
										<textarea class="form-control" id="web_service_txt" rows="8" cols="2" name="web_service_txt">{{ $settingdata->web_service_txt }}</textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="blog_txt">Blog Text</label>
										<textarea class="form-control" id="blog_txt" rows="8" cols="2" name="blog_txt">{{ $settingdata->blog_txt }}</textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="team_heading">Team Heading</label>
										<input class="form-control" id="team_heading"  name="team_heading" value="{{ $settingdata->team_heading }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="testimonial_heading">Testimonial Heading</label>
										<input class="form-control" id="testimonial_heading"  name="testimonial_heading" value="{{ $settingdata->testimonial_heading }}">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="testimonial_text">Testimonial Text</label>
										<textarea class="form-control" id="testimonial_text" rows="8" cols="2" name="testimonial_text">{{ $settingdata->testimonial_text }}</textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="footer_content">Footer Text</label>
										<textarea class="form-control" id="footer_content" rows="8" cols="2" name="footer_content">{{ $settingdata->footer_content }}</textarea>
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="project_right_text">Project Right Text</label>
										<textarea class="form-control" id="project_right_text" rows="8" cols="2" name="project_right_text">{{ $settingdata->project_right_text }}</textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="project_left_text">Project Left Text</label>
										<textarea class="form-control" id="project_left_text" rows="8" cols="2" name="project_left_text">{{ $settingdata->project_left_text }}</textarea>
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="single_project_heading">Single Project Heading</label>
										<textarea class="form-control" id="single_project_heading" rows="4" cols="2" name="single_project_heading">{{ $settingdata->single_project_heading }}</textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="single_project_txt">Single Project Text</label>
										<textarea class="form-control" id="single_project_txt" rows="4" cols="2" name="single_project_txt">{{ $settingdata->single_project_txt }}</textarea>
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="after_slider_btn_txt">After Slider Button Text Heading</label>
										<input class="form-control" id="after_slider_btn_txt"  name="after_slider_btn_txt" value="{{ $settingdata->after_slider_btn_txt }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="banner_button_text">Banner Button Text</label>
										<input class="form-control" id="banner_button_text"  name="banner_button_text" value="{{ $settingdata->banner_button_text }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="banner_button_link">Banner Button Link</label>
										<input class="form-control" id="banner_button_link"  name="banner_button_link" value="{{ $settingdata->banner_button_link }}">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="top_btn_txt">Top Button Text</label>
										<input class="form-control" id="top_btn_txt"  name="top_btn_txt" value="{{ $settingdata->top_btn_txt }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="top_btn_link">Top Button Link</label>
										<input class="form-control" id="top_btn_link"  name="top_btn_link" value="{{ $settingdata->top_btn_link }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="client_heading">Client Heading</label>
										<input class="form-control" id="client_heading"  name="client_heading" value="{{ $settingdata->client_heading }}">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
				                        <label class="control-label">Banner Image (Recommended Size: 500 * 600) </label>
					                        @if(!empty($sitedetail->homepage_banner_image))
						                        <img src="{{ asset($sitedetail->homepage_banner_image) }}" alt="" title="" class='fancybox' id="prev_img_image_1" />
						                    @elseif(!empty(old('homepage_banner_image')))
						                        <img src="{{ old('homepage_banner_image') }}" alt="" title="" class='fancybox' id="prev_img_image_1" />
						                    @else
						                        <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_image_1" />
						                    @endif
						                    <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=image_1') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
						                    <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this, 'prev_img_image_1', 'image_1')">Remove</button>
						                    <input type="hidden" value="{{ isset($sitedetail->homepage_banner_image)?$sitedetail->homepage_banner_image:old('homepage_banner_image') }}"  name="homepage_banner_image" class="form-control" id="image_1">
				                    </div>
								</div>
                                <div class="col-md-3">
									<div class="form-group">
				                        <label class="control-label">Service Image (Recommended Size: 750 * 1150) </label>
                                        @if(!empty($sitedetail->service_image))
                                            <img src="{{ asset($sitedetail->service_image) }}" alt="" title="" class='fancybox' id="prev_img_image_5" />
                                        @elseif(!empty(old('service_image')))
                                            <img src="{{ old('service_image') }}" alt="" title="" class='fancybox' id="prev_img_image_5" />
                                        @else
                                            <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_image_5" />
                                        @endif
                                        <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=image_5') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
                                        <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this, 'prev_img_image_5', 'image_5')">Remove</button>
                                        <input type="hidden" value="{{ isset($sitedetail->service_image)?$sitedetail->service_image:old('service_image') }}"  name="service_image" class="form-control" id="image_5">
				                    </div>
								</div>
                                <div class="col-md-3">
									<div class="form-group">
				                        <label class="control-label">Project Image (Recommended Size: 1920 * 1300) </label>
                                        @if(!empty($sitedetail->project_image))
                                            <img src="{{ asset($sitedetail->project_image) }}" alt="" title="" class='fancybox' id="prev_img_image_6" />
                                        @elseif(!empty(old('project_image')))
                                            <img src="{{ old('project_image') }}" alt="" title="" class='fancybox' id="prev_img_image_6" />
                                        @else
                                            <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_image_6" />
                                        @endif
                                        <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=image_6') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
                                        <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this, 'prev_img_image_6', 'image_6')">Remove</button>
                                        <input type="hidden" value="{{ isset($sitedetail->project_image)?$sitedetail->project_image:old('project_image') }}"  name="project_image" class="form-control" id="image_6">
				                    </div>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@push('script')
	<script type="text/javascript">

	     function img_pathUrl(input, $id){
	        $('#'+$id)[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);

	    }
	</script>
@endpush
@endsection
