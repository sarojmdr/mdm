@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.index') }}">{!! VIEWLIST_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route($link.'.update', $record->id) }} ">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="title">Full Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ $record->name }}" >
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="designation">Designation <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="designation" name="designation" value="{{ $record->designation }}" >
                        @if ($errors->has('designation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('designation') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="email">Email <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="email" name="email" value="{{ $record->email }}" >
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="phone">Phone <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="phone" name="phone" value="{{ $record->phone }}" >
                                @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="facebook_link">Facebook Link</label>
                                <input type="text" class="form-control" id="facebook_link" name="facebook_link" value="{{ $record->facebook_link }}" >
                                @if ($errors->has('facebook_link'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('facebook_link') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="twitter_link">Twitter Link</label>
                                <input type="text" class="form-control" id="twitter_link" name="twitter_link" value="{{ $record->twitter_link }}" >
                                @if ($errors->has('twitter_link'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('twitter_link') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="info">Short Info</label>
                        <textarea id="my-editor" class="tinymce" name="info" placeholder="Place some text here" >{{ $record->info }}</textarea>
                        @if ($errors->has('info'))
                        <span class="help-block">
                            <strong>{{ $errors->first('info') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="title">Department Name <span class="text-danger">*</span></label>
                        <select class="form-control" name="department_id">
                            @foreach($departments as $department)
                            <option value="{{$department->id}}">{{$department->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('department_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('department_id') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="linkedin_link">Linkedin link</label>
                        <input type="text" class="form-control" id="linkedin_link" name="linkedin_link" value="{{ $record->linkedin_link }}" >
                        @if ($errors->has('linkedin_link'))
                        <span class="help-block">
                            <strong>{{ $errors->first('linkedin_link') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="sort_order">Sort Order</label>
                        <input type="number" min="0" class="form-control" id="sort_order" name="sort_order" value="{{ $record->sort_order }}" >
                        @if ($errors->has('sort_order'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sort_order') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label">Featured Image</label>
                        @if(!empty($record->image))
                        <img src="{{ asset($record->image) }}" alt="" title="" class='fancybox' id="prev_img" />
                        @elseif(!empty(old('image')))
                        <img src="{{ old('image') }}" alt="" title="" class='fancybox' id="prev_img" />
                        @else
                        <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img" />
                        @endif
                        <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=image') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
                        <button class="btn btn-danger remove_box_image" type="button">Remove</button>
                        <button class="btn btn-warning prev_box_image" type="button" style="display: none;">Previous Image</button>
                        <input type="hidden" value="{{ isset($record->image)?asset($record->image):old('image') }}"  name="image" class="form-control" id="image">
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="statusid" class="form-control">
                        <option value="1" @if($record->status == '1') {{ 'selected' }} @endif>{!! PUBLISH !!}</option>
                        <option value="0" @if($record->status == '0') {{ 'selected' }} @endif>{!! UNPUBLISH !!}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="reset" class="btn btn-danger resetbtn">Clear</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection