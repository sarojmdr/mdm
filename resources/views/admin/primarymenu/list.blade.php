@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.create') }}">{!! ADD_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
        <form class="" method="POST" action="{{ route('video.store') }}">
            {{ csrf_field() }}
            <table id="sortable" class="table table-striped table-hover todo-list ui-sortable" >
                <tr class="nodrag nodrop">
                    <th><span class="handle"><i class="fa fa-arrows"></i></span></th>
                    <th>Department</th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Phone</th>
                    <th class="text-center">Image</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                </tr>
                <?php $sort_orders = ''; ?>
                @foreach ($list as $item)
                <?php $sort_orders .= $item->sort_order . ','; ?>
                <tr id="{{ $item->id }}">
                    <td class="move">
                        <span class="handle">
                            <i class="fa fa-ellipsis-v"></i>
                            <i class="fa fa-ellipsis-v"></i>
                        </span>
                    </td>
                    <td>{{ $item->department->title ?? '' }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->designation }}</td>
                    <td>{{ $item->phone }}</td>
                    <td class="text-center"><a href="{{ asset($item->image) }}" target="_blank">{!! LINK_ICON !!}</a></td>
                    <td class="text-center">{!! getStatus($item->status) !!}</td>
                    <td class="text-center">
                        <a href="{{ route($link.'.edit', $item->id) }}"> {!! EDIT_ICON !!}</a>&nbsp;|
                        <a href="{{ route($link.'.delete', $item->id) }}" class="resetbtn">{!! DELETE_ICON !!} </a>
                    </td>
                </tr>
                @endforeach
            </table>
        </form>
    </div>
</div>
@push('script')
<script type="text/javascript">
$('#sortable').tableDnD({
    onDrop: function (table, row) {
        $.post("{{ route('ajax.sorting') }}", 
            {
                ids_order: $.tableDnD.serialize(), 
                sort_orders: '<?php echo $sort_orders; ?>', 
                table: 'tbl_teams', _token: '{!! csrf_token() !!}'
            },
            function(data, textStatus, xhr) {
                // console.log('resp');
                // console.log(data);
            });
    }
});

</script>
@endpush
@endsection