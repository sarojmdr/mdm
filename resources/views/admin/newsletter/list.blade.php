@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
        </div>
    </div>
    <div class="card-body">
        
         <form class="" method="GET">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Search By Status</label>
                        <select class="form-control" name="status_search" id="status_search">
                            <option value="">Search By All</option>
                            <option value="1" @if(isset($_GET['status_search']) && $_GET['status_search'] == 1) {{ 'selected' }} @endif>{{ 'New' }}</option>
                            <option value="2" @if(isset($_GET['status_search']) && $_GET['status_search'] == 2) {{ 'selected' }} @endif>{{ 'Active' }}</option>
                            <option value="3" @if(isset($_GET['status_search']) && $_GET['status_search'] == 3) {{ 'selected' }} @endif>{{ 'Blocked' }}</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Request Date</label>
                        <input type="text" class="form-control datepicker" id="requested_date" name="requested_date" value="{{ isset($_GET['requested_date'])?$_GET['requested_date']:'' }}" placeholder="Search By Request Date">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="vi-hidden">x</label>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{ route('newsletter.index')}} " class="btn btn-danger">Reset</a>
                    </div>
                </div>
            </div>
        </form>
        <div class="table-responsive">
            <table id="sortable" class="table table-striped table-hover table-sm todo-list ui-sortable" >
                <thead class="bg-primary">
                    <tr class="nodrag nodrop">
                        <th>S.No</th>
                        <th>Email</th>
                        <th>Sent date</th>
                         <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <?php $count = 1; ?>
                @foreach ($list as $item)
                <tr>
                    <td>{{ $count++ }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->created_at }}</td>
                    <td class="text-center">
                            {!! getNewsletterStatus($item->status) !!}
                    </td>
                    <td class="text-center">
                        @if($item->status == 1)
                        <span class="font-weight-bold m-0 p-0">New</span>
                        @else
                        <span @if($item->status == 3) class="font-weight-bold" @endif>Block</span>
                        @endif
                        <div class="checkbox d-inline">
                            <label class="switch">
                                <input type="checkbox" class="newsletter_status_change" {{ ($item->status  == 2)?'checked':null }} value="{{ $item->id }}">
                                <span class="slider round"></span>
                            </label>
                            <input type="hidden" name="status" id="status" value="{{ $item->status }}">
                        </div>
                        <span @if($item->status == 2) class="font-weight-bold" @endif>Active</span>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
         @if($list)
            {{  $list->appends(Request::only('status_search'))
                ->appends(Request::only('requested_date'))
                ->links()
            }}
        @endif
    
    </div>
</div>
    @push('script')
        <script type="text/javascript">
            $('.newsletter_status_change').on('change', function () {
                baseurl = $('#baseurl').val();
                change = $(this).val();
                $.ajax({
                    url: baseurl + "/u-admin/newsletter/status-change/" + change ,
                    type: "get",
                });
                // location.reload(true);
            });
        </script>
    @endpush
@endsection