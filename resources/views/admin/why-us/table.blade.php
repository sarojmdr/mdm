<div class="card">
    <div class="card-header">List of {{ $page_header }}</div>
    <div class="card-body table-responsive">
    <form class="" method="POST" action="{{ route($link.'.store') }}">
        {{ csrf_field() }}
        <table class="table table-hover table-sm dataTablePagination compact">
            <thead class="bg-primary">
                <tr>
                    <th><span class="handle"><i class="fa fa-arrows"></i></span></th>
                    <th>Title (English)</th>
                    <th>Description</th>
                    <th width="50px">Status</th>
                    <th width="50px">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($list as $item)
                <tr id="{{ $item->id }}">
                    <td class="move">
                        <span class="handle">
                            <i class="fa fa-ellipsis-v"></i>
                            <i class="fa fa-ellipsis-v"></i>
                        </span>
                    </td>
                    <td>{{ $item->title }}</td>
                    <td>{!! $item->description !!}</td>
                    <td class="text-center">{!! getStatus($item->status) !!}</td>
                    <td width="100px" class="text-center">
                        <a href="{{ route($link.'.edit', $item->id) }}">{!! EDIT_ICON !!} </a>&nbsp;|&nbsp;
                        <a href="{{ route($link.'.delete', $item->id) }}" class="resetbtn">{!! DELETE_ICON !!}</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>