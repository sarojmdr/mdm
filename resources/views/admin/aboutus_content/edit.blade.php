@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.index') }}">{!! VIEWLIST_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
        
       <form class="" method="POST" action="{{ route($link.'.update', $record->id) }}" enctype='multipart/form-data'>
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="title">Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $record->title }}" >
                                @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="slug">Slug <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="slug" name="slug" value="{{ $record->slug }}">
                                @if ($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    
                    
                        <div class="col-md-6">
                             <div class="form-group">

                                <label class="control-label" for="icon">Icon <span class="text-danger">*</span></label> [ <a href="https://fontawesome.com/?utm_source=v4_homepage&utm_medium=display&utm_campaign=fa5_released&utm_content=banner" target="_blank">Get Icon</a> ] [Hints: Use Class only ]
                                <input type="text" class="form-control" id="icon" name="icon" value="{{ $record->icon }}" placeholder="Eg: fas fa-trophy">
                                @if ($errors->has('icon'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('icon') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label" for="published_date">Published Date {{-- <span class="text-danger">*</span> --}}</label>
                                <input type="text" class="form-control datepicker" id="published_date" name="published_date" value="{{ $record->published_date }}" placeholder="Published Date">
                                @if ($errors->has('published_date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('published_date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                   
                    {{-- <div class="form-group">
                        <label class="control-label" for="description">Description</label>
                        <br>
                        <textarea id="my-editor" class="tinymce" name="description" placeholder="Place some text here" >{{ $record->description }}</textarea>
                        @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif
                    </div> --}}
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                
                    {{-- <div class="form-group">
                        <label class="control-label">Image <span class="text-danger">*</span></label>
                         @if(!empty($record->image))
                            <img src="{{ url($record->image) }}" alt="" class="fancybox" title="" id="prev_img_image" />
                        @else
                            <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class="fancybox" title="" id="prev_img_image" />
                        @endif
                            <input type="file" value="{{ isset($record->image)?asset($record->image):'' }}"  name="image" class="form-control" id="image" onChange="img_pathUrl(this, 'prev_img_image');">
                            <br>
                        @if ($errors->has('image'))
                            <span class="help-block">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    
                    </div> --}}

                    
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="statusid" class="form-control">
                            <option value="1" @if($record->status == '1') {{ 'selected' }} @endif>{!! PUBLISH !!}</option>
                            <option value="0" @if($record->status == '0') {{ 'selected' }} @endif >{!! UNPUBLISH !!}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-danger resetbtn">Clear</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
{{-- @push('script')
    <script type="text/javascript">
        function img_pathUrl(input, $id){
            $('#'+$id)[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);
        }
    </script>
@endpush --}}
@endsection