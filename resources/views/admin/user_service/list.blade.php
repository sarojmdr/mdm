@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
        </div>
    </div>
    <div class="card-body">
        
         <form class="" method="GET">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Search By Status</label>
                        <select class="form-control" name="status_search" id="status_search">
                            <option value="">Search By All</option>
                            <option value="1" @if(isset($_GET['status_search']) && $_GET['status_search'] == 1) {{ 'selected' }} @endif>{{ 'New' }}</option>
                            <option value="2" @if(isset($_GET['status_search']) && $_GET['status_search'] == 2) {{ 'selected' }} @endif>{{ 'Active' }}</option>
                            <option value="3" @if(isset($_GET['status_search']) && $_GET['status_search'] == 3) {{ 'selected' }} @endif>{{ 'Blocked' }}</option>
                        </select>
                    </div>
                </div>

                {{-- <div class="col-md-3">
                    <div class="form-group">
                        <label>Request Date</label>
                        <input type="text" class="form-control datepicker" id="requested_date" name="requested_date" value="{{ isset($_GET['requested_date'])?$_GET['requested_date']:'' }}" placeholder="Search By Request Date">
                    </div>
                </div> --}}
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="vi-hidden">x</label>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{ route('user_service.index')}} " class="btn btn-danger">Reset</a>
                    </div>
                </div>
            </div>
        </form>
        <div class="table-responsive">
            <table id="sortable" class="table table-striped table-sm table-hover todo-list ui-sortable" >
                <thead class="bg-primary">
                    <tr class="nodrag nodrop">
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Message</th>
                        <th>Contact No.</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Sent date</th>
                         <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <?php $count = 1; ?>
                @foreach ($list as $item)
                <tr>
                    <td>{{ $count++ }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->message }}</td>
                    <td>{{ $item->phoneno }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->address }}</td>
                    <td>{{ $item->inserted_date }}</td>
                    <td class="text-center">
                            {!! getUserServiceStatus($item->status) !!}
                    </td>
                    <td class="text-center">
                        <select class="form-control client_request_status" id="client_request_status"{{ $item->id }} onchange="updateUserServiceStatus(this,{{  $item->id }})">
                            @if($item->status == 1)
                                <option value = "1" @if($item->status == 1) {{ 'selected' }} @endif>New</option>
                            @endif
                            <option value="2" @if($item->status == 2) {{ 'selected' }} @endif>Viewed</option>
                            <option value="3" @if($item->status == 3) {{ 'selected' }} @endif>Blocked</option>
                            
                        </select>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        @if($list)
            {{  $list->appends(Request::only('status_search'))
                ->links()
            }}
        @endif
    </div>
</div>
    @push('script')
        <script type="text/javascript">
            function updateUserServiceStatus(obj, id){
                baseurl = $('#baseurl').val();
                status = $(obj).val();
                $.ajax({
                    url: baseurl + "/u-admin/user-service/status-change/" + id ,
                    type: "get",
                    data : {'status':status}
                });
                location.reload(true);
            }
        </script>
    @endpush
@endsection