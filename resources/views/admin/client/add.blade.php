@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')

<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.index') }}">{!! VIEWLIST_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route($link.'.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="title">Full name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" >
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label" for="Link">Link <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="link" name="link" value="{{ old('link') }}" >
                                @if ($errors->has('link'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('link') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label" for="published_date">Published Date <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datepicker" id="published_date" name="published_date" value="{{date('Y-m-d') }}" placeholder="Published Date">
                                @if ($errors->has('published_date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('published_date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                    <div class="form-group">
                        <label class="control-label">Logo - Recommended Size (140*80px)<span class="text-danger">*</span></label>
                        @if(!empty(old('logo')))
                            <img src="{{ old('logo') }}" alt="" title="" class='fancybox' id="prev_img_logo" />
                        @else
                            <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_logo" />
                        @endif
                        <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=logo') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
                       <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this,'prev_img_logo', 'logo')">Remove</button>
                        <input type="hidden" value="{{ isset($record->logo)?$record->logo:old('logo') }}"  name="logo" class="form-control" id="logo">

                        <br>
                        @if ($errors->has('logo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('logo') }}</strong>
                        </span>
                        @endif

                    </div>


                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="statusid" class="form-control">
                            <option value="1">{!! PUBLISH !!}</option>
                            <option value="0">{!! UNPUBLISH !!}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-danger resetbtn">Clear</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@push('script')
    <script type="text/javascript">

         function img_pathUrl(input, $id){
            $('#'+$id)[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);

        }
    </script>
@endpush
@endsection
