@extends('admin.master')
@section('title', $page_header)
@section('content-header', $page_header)
@section('content')
<div class="card">
    <div class="card-header">{{ $page_header }}
        <div class="card-header-actions">
            <a class="card-header-action btn btn-warning" href="{{ route($link.'.index') }}">{!! VIEWLIST_ICON !!}</a>
        </div>
    </div>
    <div class="card-body">

       <form class="" method="POST" action="{{ route($link.'.update', $record->id) }}" enctype='multipart/form-data'>
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="title">Full name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $record->title }}" >
                                @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="slug">Slug <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="slug" name="slug" value="{{ $record->slug }}">
                                @if ($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label" for="Link">Link <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="link" name="link" value="{{ $record->link }}" >
                                @if ($errors->has('link'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('link') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label" for="published_date">Project Date <span class="text-danger">*</span></label>
                                <input type="text" class="form-control datepicker" id="published_date" name="published_date" value="{{ $record->published_date }}" placeholder="Project Date">
                                @if ($errors->has('published_date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('published_date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                    <div class="form-group">
                        <label class="control-label">Logo - Recommended Size (140*80px)<span class="text-danger">*</span></label>
                        @if(!empty($record->logo))
                            <img src="{{ asset($record->logo) }}" alt="" title="" class='fancybox' id="prev_img_logo" />
                        @elseif(!empty(old('logo')))
                            <img src="{{ old('logo') }}" alt="" title="" class='fancybox' id="prev_img_logo" />
                        @else
                            <img src="{{ asset('admin/images/no-image.png', $secure = null) }}" alt="" class='fancybox' title="" id="prev_img_logo" />
                        @endif
                        <a href="{{ url('/uploads/filemanager/dialog.php?type=1&field_id=logo') }}" data-fancybox-type="iframe" class="btn btn-info fancy">Insert</a>
                        <button class="btn btn-danger remove_box_image" type="button" onclick="removeBoxImage(this,'prev_img_logo', 'logo')">Remove</button>
                        <input type="hidden" value="{{ isset($record->logo)?$record->logo:old('logo') }}"  name="logo" class="form-control" id="logo">
                        <br>
                        @if ($errors->has('logo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('logo') }}</strong>
                            </span>
                        @endif

                    </div>


                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="statusid" class="form-control">
                            <option value="1" @if($record->status == '1') {{ 'selected' }} @endif>{!! PUBLISH !!}</option>
                            <option value="0" @if($record->status == '0') {{ 'selected' }} @endif >{!! UNPUBLISH !!}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-danger resetbtn">Clear</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@push('script')
    <script type="text/javascript">
        function img_pathUrl(input, $id){
            $('#'+$id)[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);
        }
    </script>
@endpush
@endsection
