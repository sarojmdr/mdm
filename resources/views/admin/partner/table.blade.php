<div class="card">
    <div class="card-header">List of Partner</div>
    <div class="card-body table-responsive">
        <table class="table table-hover table-sm dataTablePagination compact">
            <thead class="bg-primary">
                <tr>
                    <th width="10px">S.No</th>
                    <th>Partner Type</th>
                    <th>Title</th>
                    <th class="text-center">Image</th>
                    <th class="text-center"width="50px">Status</th>
                    <th class="text-center"width="50px">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $count = 1; ?>
                @foreach ($list as $item)
                <tr>
                    <td>{{ $count++ }}</td>
                    <td>{{ $item->partnertype->title ?? '' }}</td>
                    <td>{{ $item->title }}</td>
                    <td class="text-center"><a href="{{ asset($item->image) }}" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                    <td class="text-center">{!! getStatus($item->status) !!}</td>
                    <td width="100px" class="text-center">
                        <a href="{{ route($link.'.edit', $item->id) }}">{!! EDIT_ICON !!} </a>&nbsp;|&nbsp;
                        <a href="{{ route($link.'.delete', $item->id) }}" class="resetbtn">{!! DELETE_ICON !!}</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>