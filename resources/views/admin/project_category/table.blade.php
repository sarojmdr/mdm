<div class="card">
    <div class="card-header">List of Category</div>
    <div class="card-body table-responsive">
        <table id="sortable" class="table table-hover table-sm dataTablePagination compact">
            <thead class="bg-primary">
                <tr class="nodrag nodrop">
                    <th><span class="handle">{!! ARROW_ICON !!}</span></th>
                    <th>Title (English)</th>
                    <th>Slug</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                 <?php $sort_orders = ''; ?>
                @if(!empty($list))
                @foreach ($list as $item)
                <?php $sort_orders .= $item->sort_order . ','; ?>
                <tr id="{{ $item->id }}">
                    <td class="move">
                        <span class="handle">
                            <i class="fa fa-ellipsis-v"></i>
                            <i class="fa fa-ellipsis-v"></i>
                        </span>
                    </td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->slug }}</td>
                    <td class="text-center">{!! getStatus($item->status) !!}</td>
                    <td  class="text-center">
                        <a href="{{ route($link.'.edit', $item->id) }}">{!! EDIT_ICON !!} </a>
                        
                        &nbsp;|&nbsp;
                        <a href="{{ route($link.'.delete', $item->id) }}" class="resetbtn">{!! DELETE_ICON !!}</a>
                       
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@push('script')
    <script type="text/javascript">
        $('#sortable').tableDnD({
        onDrop: function (table, row) {
            $.post("{{ route('ajax.sorting') }}", {ids_order: $.tableDnD.serialize(), sort_orders: '<?php echo $sort_orders; ?>', table: 'tbl_project_category', _token: '{!! csrf_token() !!}'}, function(data){
                console.log(data);
            });
        }
    });
    </script>
@endpush