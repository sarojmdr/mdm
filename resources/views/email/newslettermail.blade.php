@extends('email.master') 
@section('title', $subject)
@section('icon', asset('email_icon/circle-icon-message.png', $secure = null))
{{-- @section('date', \Carbon\Carbon::parse($sitedetail)->format('l, jS \of F Y')) --}}
{{-- @section('clickhere_url', route('newsletterdetail',$slug)) --}}
@section('main-content')
<tr>
    <td style="color: rgb(40, 40, 40); font-size: 15px; line-height: 2; font-weight: 500; font-family: lato, Helvetica, sans-serif; border-color: rgb(40, 40, 40);">
        <div style="line-height: 2;" id="detail-content">
            <span>
                <multiline>
                <br>
                <p>{!! $record->description !!}</p>
                </multiline>
            </span>
        </div>        
    </td>
</tr>
@endsection