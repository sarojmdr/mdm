@extends('email.master') 
@section('title', $subject)
@section('icon', asset('email_icon/circle-icon-message.png', $secure = null))
{{-- @section('date', \Carbon\Carbon::parse($sitedetail)->format('l, jS \of F Y')) --}}
{{-- @section('clickhere_url', route('newsletterdetail',$slug)) --}}
@section('main-content')
<tr>
    <td style="color: rgb(40, 40, 40); font-size: 15px; line-height: 2; font-weight: 500; font-family: lato, Helvetica, sans-serif; border-color: rgb(40, 40, 40);">
        <div style="line-height: 2;" id="detail-content">
            <span>
                <multiline>
                    Hi {{ $record->name }},
                </multiline>
            </span>
        </div>
        <div style="line-height: 2;" id="detail-content">
            <span>
                <multiline>
                <br>
                    Information Of New Store Pasal Client :
                    <ul>
                        <li>Name: {{ $record->name }}</li>
                        <li>Address: {{ $record->address }}</li>
                        <li>Email: {{ $record->email }}</li>
                        <li>Phone: {{ $record->phoneno }}</li>
                        <li>Date: {{ $record->inserted_date }}</li>
                        <li><strong>Subject:</strong> {{ $record->subject }}</li>
                        <li><strong>Message:</strong> {{ $record->message }}</li>
                    </ul>
                </multiline>
            </span>
        </div>
        
    </td>
</tr>
@endsection