@extends('email.master') 
@section('title', $subject)
@section('icon', asset('email_icon/circle-icon-message.png', $secure = null))
{{-- @section('date', \Carbon\Carbon::parse($sitedetail)->format('l, jS \of F Y')) --}}
{{-- @section('clickhere_url', route('newsletterdetail',$slug)) --}}
@section('main-content')
<tr>
    <td style="color: rgb(253, 148, 23); font-size: 15px; line-height: 2; font-weight: 500; font-family: lato, Helvetica, sans-serif; border-color: rgb(253, 148, 23);">
        <div style="line-height: 2;" id="detail-content">
            <span>
                <multiline>
                    Hi {{ $record->name }},
                </multiline>
            </span>
        </div>
        <div style="line-height: 2;" id="detail-content">
            <span>
                <multiline>
                <br>
                    You are receiving this email because we received a password reset request for your account.
                    <br>
                    Please click the button below to reset your password.
                    <br>
                    <center>
                        <a href="{{ route('password.reset', $token) }}" class="btn btn-primary" style="color:white" role="button">Reset Password</a>
                    </center>
                </multiline>
            </span>
        </div>
        
    </td>
</tr>
@endsection
@section('link-content')
<tr>
    <td style="color: #a1a2a5; font-size: 14px;line-height: 2; font-weight: 500; font-family: lato, Helvetica, sans-serif; mso-line-height-rule: exactly;" align="left">
        <div style="line-height: 2;">
            <span class="text_container">
                <multiline>If you're having trouble cliking the "Reset Password" button, copy and paste the URL below into your web browser:  </multiline>
                <multiline><a href="{{ route('password.reset', $token) }}" target="_blank">{{ route('password.reset', $token) }}</a></multiline>
            </span>
        </div>
    </td>
</tr>
@endsection