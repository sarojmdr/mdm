@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', $sitedetail->logo)
@section('fb_url', Request::url())
@section('title', '404 || Page Not Found')

@section('breadcrumb')
 <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="{{ route('index') }}">Home</a></li>
          <li>404 || Page Not Found</li>
        </ol>

      </div>
    </section><!-- End Breadcrumbs -->
@endsection

@section('content')
	<section class="inner-page">
      <div class="container">
        <center>
        	<h1 class='text-danger'>
	          404 || Page Not Found
	        </h1>
	    </center>
      </div>
    </section>
@endsection
