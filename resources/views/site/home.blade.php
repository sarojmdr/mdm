@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', asset($sitedetail->fb_image))
@section('title', $page_header)
@section('content')
   <div class="page-content-home-page">
      <div data-elementor-type="wp-page" data-elementor-id="6194" class="elementor elementor-6194">
         <div class="elementor-inner">
            <div class="elementor-section-wrap">
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-b902da6 elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                  data-id="b902da6" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-no">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-399555d slider-height-five"
                           data-id="399555d" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <div class="elementor-element elementor-element-4b7af95 elementor-widget elementor-widget-slider"
                                    data-id="4b7af95" data-element_type="widget" id="home" data-widget_type="slider.default">
                                    <div class="elementor-widget-container">

                                       <section class="dreamit-slick-slider">
                                          <div class="default-slider slick">

                                             <div class="single-slick style-one style-six align-items-center d-flex text-left">
                                                <div class="container">
                                                   <div class="slide-img parallax-effect"
                                                      style="background:url({{ asset('site/uploads/2021/07/home-4-slide.jpg') }}) center center / cover scroll no-repeat;">
                                                   </div>
                                                   <div class="hero-text-wrap">
                                                      <div class="hero-text">
                                                         <div class="white-color">
                                                            <h4> Secure &amp; IT Services </h4>
                                                            <h1 class="title1"> Transformation is <br>with <span>Business</span>
                                                            </h1>
                                                            <p>Authoritatively transform adaptive manufactured products before
                                                               <br>networks. Authoritatively disintermediate.
                                                            </p>
                                                            <a class="btn btn-gradient btn-md btn-animated-none btn-one"
                                                               href="#">
                                                               Learn More <i class="flaticon-right-arrow-3"></i>
                                                            </a>

                                                            <div class="slider-video-icon">
                                                               <a href="../../external.html?link=https://www.youtube.com/watch?v=Wx48y_fOfiY"
                                                                  class="video-vemo-icon venobox vbox-item" data-vbtype="youtube"
                                                                  data-autoplay="true">
                                                                  <i class="fas fa-play-circle fa-solid"></i>
                                                               </a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="slider-single-image">
                                                      <img src="{{ asset('site/uploads/2021/07/home-4-single.png') }}"
                                                         alt="">
                                                   </div>
                                                </div><!-- .container -->

                                                <div class="slider-img  shape1 bounce-animate3">
                                                   <img src="{{ asset('site/assets/images/miss-11.png') }}" alt="01">
                                                </div>
                                                <div class="slider-img  shape2">
                                                   <img src="{{ asset('site/assets/images/miss12.png') }}" alt="02">
                                                </div>
                                                <div class="slider-img  shape3 bounce-animate5">
                                                   <img src="{{ asset('site/assets/images/miss-13.png') }}" alt="03">
                                                </div>
                                                <div class="slider-img  shape4 bounce-animate5">
                                                   <img src="{{ asset('site/assets/images/miss-14.png') }}" alt="04">
                                                </div>
                                                <div class="slider-img  shape5 bounce-animate3">
                                                   <img src="{{ asset('site/assets/images/icon5.png"') }}" alt="04">
                                                </div>
                                                <div class="slider-img  shape6">
                                                   <img src="{{ asset('site/assets/images/miss-15.png') }}" alt="01">
                                                </div>
                                                <div class="slider-img  shape7">
                                                   <img src="{{ asset('site/assets/images/miss-16.png') }}" alt="01">
                                                </div>
                                             </div><!-- .single-slick -->

                                          </div>
                                       </section>
                                       <script>
                                          jQuery(document).ready(function() {
                                             jQuery(".default-slider").slick({
                                                dots: false,
                                                infinite: true,
                                                centerMode: true,
                                                autoplay: true,
                                                autoplaySpeed: 7000,
                                                slidesToShow: 1,
                                                slidesToScroll: 1,
                                                centerPadding: '0',
                                                arrows: false
                                             });
                                          });
                                       </script>


                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-7dfb7b6 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="7dfb7b6" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-75bd91f"
                           data-id="75bd91f" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-4142847 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="4142847" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d1521cb"
                                             data-id="d1521cb" data-element_type="column"
                                             data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-9636df5 elementor-widget elementor-widget-iconbox"
                                                      data-id="9636df5" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box d-flex style-one">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-monitor"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>High Quality Data Secuirity </h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p>Collaboratively pursue goal is
                                                                     results whereas robust.</p>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d48d11a"
                                             data-id="d48d11a" data-element_type="column"
                                             data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-049fa53 elementor-widget elementor-widget-iconbox"
                                                      data-id="049fa53" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box d-flex style-one">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-laptop"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>IT Management</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p>Collaboratively pursue goal is
                                                                     results whereas robust.</p>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-d71e627 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="d71e627" data-element_type="section" id="about">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-33e41d6"
                           data-id="33e41d6" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <div class="elementor-element elementor-element-2bdf341 elementor-widget elementor-widget-html"
                                    data-id="2bdf341" data-element_type="widget" data-widget_type="html.default">
                                    <div class="elementor-widget-container">
                                       <div class="about-section-thumb">
                                          <img src="{{ asset('site/uploads/2021/07/miss-20.png') }}">
                                          <div class="section-img shape1 bounce-animate3">
                                             <a href="#"><img src="{{ asset('site/uploads/2021/07/social.png') }}"
                                                   alt=""></a>
                                          </div>
                                          <div class="section-img shape2 bounce-animate2">
                                             <a href="#"><img src="{{ asset('site/uploads/2021/07/social2.png') }}"
                                                   alt=""></a>
                                          </div>
                                          <div class="section-img shape3 bounce-animate5">
                                             <a href="#"><img src="{{ asset('site/uploads/2021/07/social3.png') }}"
                                                   alt=""></a>
                                          </div>
                                          <div class="section-img shape4">
                                             <img src="{{ asset('site/uploads/2021/07/miss-21.png') }}" alt="">
                                          </div>
                                          <div class="section-img shape5">
                                             <img src="{{ asset('site/uploads/2021/07/miss-3.png') }}" alt="">
                                          </div>
                                          <div class="section-img shape6">
                                             <img src="{{ asset('site/uploads/2021/07/miss-23.png') }}" alt="">
                                          </div>
                                          <div class="section-img shape7 bounce-animate5">
                                             <img src="{{ asset('site/uploads/2021/08/miss-24.png') }}" alt="">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div
                           class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-9b1cdc1"
                           data-id="9b1cdc1" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <div
                                    class="elementor-element elementor-element-853f533 elementor-widget elementor-widget-sectiontitle"
                                    data-id="853f533" data-element_type="widget" data-widget_type="sectiontitle.default">
                                    <div class="elementor-widget-container">
                                       <div class="">
                                          <div class="section-title t_left">
                                             <h5>// DISCOVER OUR COMPANY</h5>
                                             <div class="title">
                                                <h3>Bringing New IT Business </h3>
                                                <h2>Solutions <span>And Ideas</span></h2>
                                             </div>

                                             <p>Energistically implement ubiquitous mindshare without turnkey time is
                                                Efficiently aggregate empowered experiences for integrated coll
                                                idea-sharing. Proactively pursue has coming expedite user friendly
                                                supply chains via real-time human capital done.</p>

                                          </div>
                                       </div>

                                    </div>
                                 </div>
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-f537688 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="f537688" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-06b1ed2"
                                             data-id="06b1ed2" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-0d2fac3 elementor-widget elementor-widget-iconbox"
                                                      data-id="0d2fac3" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box style-two">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-growth"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>Data Protection</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p></p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-11fedb8"
                                             data-id="11fedb8" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-87ef66a elementor-widget elementor-widget-iconbox"
                                                      data-id="87ef66a" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box style-two">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-24-hours-2"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>24/7 Support</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p></p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-cd77e99"
                                             data-id="cd77e99" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-68608c4 elementor-widget elementor-widget-iconbox"
                                                      data-id="68608c4" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box style-two">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-agreement"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>Business Deal</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p></p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-abe8d85 service elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="abe8d85" data-element_type="section"
                  data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6138eb0"
                           data-id="6138eb0" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-f24d447 service elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="f24d447" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-eaf98ef"
                                             data-id="eaf98ef" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-f20cf5d elementor-widget elementor-widget-sectiontitle"
                                                      data-id="f20cf5d" data-element_type="widget"
                                                      data-widget_type="sectiontitle.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="">
                                                            <div class="section-title t_left">
                                                               <h5>// What Our Offer</h5>
                                                               <div class="title">
                                                                  <h3>Business Solution</h3>
                                                                  <h2><span></span></h2>
                                                               </div>

                                                               <p>Collaboratively reintermediate holistic markets
                                                                  restore strategic sources the world. </p>

                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-62eceaf elementor-widget elementor-widget-ditbutton"
                                                      data-id="62eceaf" data-element_type="widget"
                                                      data-widget_type="ditbutton.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="dit-button style-one">
                                                            <a href="#">View All <i aria-hidden="true"
                                                                  class="flaticon flaticon-long-arrow-pointing-to-the-right"></i></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-f0c4b6c"
                                             data-id="f0c4b6c" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-f6dbf8c elementor-widget elementor-widget-service"
                                                      data-id="f6dbf8c" data-element_type="widget"
                                                      data-widget_type="service.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="service-box style-nine">
                                                            <div class="single-service-box">

                                                               <div class="service-box-thumb">
                                                                  <img src="{{ asset('site/uploads/2021/08/icon1.png') }}"
                                                                     alt="" />
                                                               </div>
                                                               <div class="single-service-number">
                                                                  <h1>01</h1>
                                                               </div>
                                                               <div class="single-service-content">
                                                                  <div class="single-service-title">
                                                                     <h2>IT Business Deals</h2>
                                                                  </div>
                                                                  <div class="single-service-conent-text">
                                                                     <p>Progressively myocardinate team reinvent interoperable
                                                                        growthis a possible world done.</p>
                                                                  </div>
                                                                  <div class="single-service-icon-two">
                                                                     <a href="#"><i class="fas fa-arrow-right"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4e8f309"
                                             data-id="4e8f309" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-9b967a4 elementor-widget elementor-widget-service"
                                                      data-id="9b967a4" data-element_type="widget"
                                                      data-widget_type="service.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="service-box style-nine">
                                                            <div class="single-service-box">

                                                               <div class="service-box-thumb">
                                                                  <img src="{{ asset('site/uploads/2021/08/icon4.png') }}"
                                                                     alt="" />
                                                               </div>
                                                               <div class="single-service-number">
                                                                  <h1>02</h1>
                                                               </div>
                                                               <div class="single-service-content">
                                                                  <div class="single-service-title">
                                                                     <h2>Corporate Finance</h2>
                                                                  </div>
                                                                  <div class="single-service-conent-text">
                                                                     <p>Dynamically fabricate enterprise is
                                                                        improvements via stand-alone
                                                                        Compellingly reconcept.</p>
                                                                  </div>
                                                                  <div class="single-service-icon-two">
                                                                     <a href="#"><i class="fas fa-arrow-right"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-1303238 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="1303238" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2674755"
                                             data-id="2674755" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-bc9e311 elementor-widget elementor-widget-service"
                                                      data-id="bc9e311" data-element_type="widget"
                                                      data-widget_type="service.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="service-box style-nine">
                                                            <div class="single-service-box">

                                                               <div class="service-box-thumb">
                                                                  <img src="{{ asset('site/uploads/2021/08/icon3.png') }}"
                                                                     alt="" />
                                                               </div>
                                                               <div class="single-service-number">
                                                                  <h1>03</h1>
                                                               </div>
                                                               <div class="single-service-content">
                                                                  <div class="single-service-title">
                                                                     <h2>Business Growth</h2>
                                                                  </div>
                                                                  <div class="single-service-conent-text">
                                                                     <p>Progressively myocardinate team reinvent interoperable
                                                                        growthis a possible world done.</p>
                                                                  </div>
                                                                  <div class="single-service-icon-two">
                                                                     <a href="#"><i class="fas fa-arrow-right"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-53ba1f9"
                                             data-id="53ba1f9" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-988a70e elementor-widget elementor-widget-service"
                                                      data-id="988a70e" data-element_type="widget"
                                                      data-widget_type="service.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="service-box style-nine">
                                                            <div class="single-service-box">

                                                               <div class="service-box-thumb">
                                                                  <img src="{{ asset('site/uploads/2021/08/icon4.png') }}"
                                                                     alt="" />
                                                               </div>
                                                               <div class="single-service-number">
                                                                  <h1>04</h1>
                                                               </div>
                                                               <div class="single-service-content">
                                                                  <div class="single-service-title">
                                                                     <h2>Information Tech</h2>
                                                                  </div>
                                                                  <div class="single-service-conent-text">
                                                                     <p>Progressively myocardinate team reinvent interoperable
                                                                        growthis a possible world done.</p>
                                                                  </div>
                                                                  <div class="single-service-icon-two">
                                                                     <a href="#"><i class="fas fa-arrow-right"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-68678b4"
                                             data-id="68678b4" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-35b4eb8 elementor-widget elementor-widget-service"
                                                      data-id="35b4eb8" data-element_type="widget"
                                                      data-widget_type="service.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="service-box style-nine">
                                                            <div class="single-service-box">

                                                               <div class="service-box-thumb">
                                                                  <img src="{{ asset('site/uploads/2021/08/icon5.png') }}"
                                                                     alt="" />
                                                               </div>
                                                               <div class="single-service-number">
                                                                  <h1>05</h1>
                                                               </div>
                                                               <div class="single-service-content">
                                                                  <div class="single-service-title">
                                                                     <h2>Partnership System</h2>
                                                                  </div>
                                                                  <div class="single-service-conent-text">
                                                                     <p>Dynamically fabricate enterprise is
                                                                        improvements via stand-alone
                                                                        Compellingly reconcept.</p>
                                                                  </div>
                                                                  <div class="single-service-icon-two">
                                                                     <a href="#"><i class="fas fa-arrow-right"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-7d12c98 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="7d12c98" data-element_type="section"
                  data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-bbbf24d"
                           data-id="bbbf24d" data-element_type="column">
                           <div class="elementor-column-wrap">
                              <div class="elementor-widget-wrap">
                              </div>
                           </div>
                        </div>
                        <div
                           class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-ffa0574"
                           data-id="ffa0574" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <div
                                    class="elementor-element elementor-element-f1679f9 elementor-widget elementor-widget-sectiontitle"
                                    data-id="f1679f9" data-element_type="widget" data-widget_type="sectiontitle.default">
                                    <div class="elementor-widget-container">
                                       <div class="">
                                          <div class="section-title t_left">
                                             <h5>// Why Choose Us</h5>
                                             <div class="title">
                                                <h3>Some Of Reason For Choose</h3>
                                                <h2>Our <span>IT Solutions</span></h2>
                                             </div>

                                             <p>Collaboratively reconceptualize real-time meta-services before
                                                paradigms. Professionally synergize plug-and-play standardized
                                                benefits via synergistic value done.</p>

                                          </div>
                                       </div>

                                    </div>
                                 </div>
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-6a482d4 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="6a482d4" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-a84c298"
                                             data-id="a84c298" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-2933115 elementor-widget elementor-widget-textbox"
                                                      data-id="2933115" data-element_type="widget"
                                                      data-widget_type="textbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="text-box style-one">
                                                            <div class="box-wrapper">
                                                               <div class="box-content">
                                                                  <div class="title">
                                                                     <h4>Brilient Client<br>Service</h4>
                                                                  </div>
                                                                  <div class="single-btn">
                                                                     <a href="#"><i
                                                                           class="flaticon-right-arrow-forward"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-57dc2bc"
                                             data-id="57dc2bc" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-61d503a elementor-widget elementor-widget-textbox"
                                                      data-id="61d503a" data-element_type="widget"
                                                      data-widget_type="textbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="text-box style-one">
                                                            <div class="box-wrapper">
                                                               <div class="box-content">
                                                                  <div class="title">
                                                                     <h4>Corporate Design Solution</h4>
                                                                  </div>
                                                                  <div class="single-btn">
                                                                     <a href="#"><i
                                                                           class="flaticon-right-arrow-forward"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-41394c2 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="41394c2" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-76b6975"
                                             data-id="76b6975" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-2c2a39a elementor-widget elementor-widget-textbox"
                                                      data-id="2c2a39a" data-element_type="widget"
                                                      data-widget_type="textbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="text-box style-one">
                                                            <div class="box-wrapper">
                                                               <div class="box-content">
                                                                  <div class="title">
                                                                     <h4>Meeting Fixed Company</h4>
                                                                  </div>
                                                                  <div class="single-btn">
                                                                     <a href="#"><i
                                                                           class="flaticon-right-arrow-forward"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-cf8e041"
                                             data-id="cf8e041" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-727829d elementor-widget elementor-widget-textbox"
                                                      data-id="727829d" data-element_type="widget"
                                                      data-widget_type="textbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="text-box style-one">
                                                            <div class="box-wrapper">
                                                               <div class="box-content">
                                                                  <div class="title">
                                                                     <h4>Any Business<br>Deals</h4>
                                                                  </div>
                                                                  <div class="single-btn">
                                                                     <a href="#"><i
                                                                           class="flaticon-right-arrow-forward"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-c4ad085 elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                  data-id="c4ad085" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f9245f9"
                           data-id="f9245f9" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <div
                                    class="elementor-element elementor-element-00e6e8b elementor-widget elementor-widget-sectiontitle"
                                    data-id="00e6e8b" data-element_type="widget" data-widget_type="sectiontitle.default">
                                    <div class="elementor-widget-container">
                                       <div class="">
                                          <div class="section-title t_center">
                                             <h5>// Our Strategy</h5>
                                             <div class="title">
                                                <h3>Main Principles</h3>
                                                <h2><span></span></h2>
                                             </div>

                                             <p></p>

                                          </div>
                                       </div>

                                    </div>
                                 </div>
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-433878e elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="433878e" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-6768770"
                                             data-id="6768770" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-db83f3d elementor-widget elementor-widget-iconbox"
                                                      data-id="db83f3d" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box style-five">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-headphones-1"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>Truly Local Support</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p>Enthusiastically leverage other
                                                                     compelling deliverables</p>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-e26a4e9 elementor-widget elementor-widget-iconbox"
                                                      data-id="e26a4e9" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box style-five">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-bar-chart-1"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>Data Tecnologiest</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p>Uniquely unles extensive meta
                                                                     meta-services through</p>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-07f79b8 elementor-widget elementor-widget-iconbox"
                                                      data-id="07f79b8" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box style-five">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true"
                                                                     class="flaticon flaticon-technical-support"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>Super Tech Support</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p>Phosfluorescently unleash 24/7
                                                                     architectures after magist</p>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-c4f0d80"
                                             data-id="c4f0d80" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-ef28ec9 elementor-widget elementor-widget-image"
                                                      data-id="ef28ec9" data-element_type="widget"
                                                      data-widget_type="image.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="elementor-image">
                                                            <img width="517" height="517"
                                                               src="../wp-content/uploads/2021/08/principle-img.png"
                                                               class="attachment-large size-large" alt="" loading="lazy"
                                                               srcset="https://itsoft.dreamitsolution.net/wp-content/uploads/2021/08/principle-img.png 517w, https://itsoft.dreamitsolution.net/wp-content/uploads/2021/08/principle-img-300x300.png 300w, https://itsoft.dreamitsolution.net/wp-content/uploads/2021/08/principle-img-150x150.png 150w, https://itsoft.dreamitsolution.net/wp-content/uploads/2021/08/principle-img-450x450.png 450w, https://itsoft.dreamitsolution.net/wp-content/uploads/2021/08/principle-img-106x106.png 106w, https://itsoft.dreamitsolution.net/wp-content/uploads/2021/08/principle-img-80x80.png 80w"
                                                               sizes="(max-width: 517px) 100vw, 517px" />
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-9edfafe"
                                             data-id="9edfafe" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-0e58da7 elementor-widget elementor-widget-iconbox"
                                                      data-id="0e58da7" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box style-four">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-startup"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>Business Growth Up</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p>Phosfluorescently unleash 24/7
                                                                     architectures after magist </p>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-e40cfe5 elementor-widget elementor-widget-iconbox"
                                                      data-id="e40cfe5" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box style-four">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-monitor"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>Corporate Analytics</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p>Enthusiastically leverage other
                                                                     compelling deliverables</p>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-9c55d17 elementor-widget elementor-widget-iconbox"
                                                      data-id="9c55d17" data-element_type="widget"
                                                      data-widget_type="iconbox.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="icon-box style-four">
                                                            <div class="icon-box-icon">
                                                               <div class="icon">
                                                                  <i aria-hidden="true" class="flaticon flaticon-target"></i>
                                                               </div>

                                                            </div>
                                                            <div class="icon-box-content">
                                                               <div class="title">
                                                                  <h2>IT Technologyiest</h2>
                                                               </div>
                                                               <div class="description">
                                                                  <p>Phosfluorescently unleash 24/7
                                                                     architectures after magist </p>
                                                               </div>
                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-653c287 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="653c287" data-element_type="section" id="portfolio"
                  data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3d959d2"
                           data-id="3d959d2" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-088f04a elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="088f04a" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-7247868"
                                             data-id="7247868" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-6c52c22 elementor-widget elementor-widget-sectiontitle"
                                                      data-id="6c52c22" data-element_type="widget"
                                                      data-widget_type="sectiontitle.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="">
                                                            <div class="section-title t_left">
                                                               <h5>// PROJECTS OVERVIEW</h5>
                                                               <div class="title">
                                                                  <h3>Our Latest Case Studies</h3>
                                                                  <h2>for your <span>Business</span></h2>
                                                               </div>

                                                               <p></p>

                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-dfc5c78"
                                             data-id="dfc5c78" data-element_type="column">
                                             <div class="elementor-column-wrap">
                                                <div class="elementor-widget-wrap">
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                                 <div
                                    class="elementor-element elementor-element-1ed6c15 elementor-widget elementor-widget-casestudy"
                                    data-id="1ed6c15" data-element_type="widget" data-widget_type="casestudy.default">
                                    <div class="elementor-widget-container">

                                       <div class="case-study-section">
                                          <div class="case_study_three owl-loaded owl-carousel">

                                             <div class="single-case-study">
                                                <div class="case-study-img">
                                                   <img loading="lazy" width="600" height="750"
                                                      src="../wp-content/uploads/2020/09/case2.jpg"
                                                      class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                      alt=""
                                                      srcset="https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case2.jpg 600w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case2-240x300.jpg 240w"
                                                      sizes="(max-width: 600px) 100vw, 600px" />
                                                </div>
                                                <div class="case-study-content">
                                                   <h3><a href="../em_case_study/tech-conference/index.html">Tech Conference</a>
                                                   </h3>
                                                   <div class="case-category">
                                                      <p>
                                                         <span class="category-item">Development</span>
                                                      </p>
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="single-case-study">
                                                <div class="case-study-img">
                                                   <img width="600" height="750"
                                                      src="../wp-content/uploads/2020/09/case1.jpg"
                                                      class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                      alt="" loading="lazy"
                                                      srcset="https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case1.jpg 600w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case1-240x300.jpg 240w"
                                                      sizes="(max-width: 600px) 100vw, 600px" />
                                                </div>
                                                <div class="case-study-content">
                                                   <h3><a href="../em_case_study/innovative-interfaces/index.html">Data
                                                         Protection</a></h3>
                                                   <div class="case-category">
                                                      <p>
                                                         <span class="category-item">Development</span>
                                                      </p>
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="single-case-study">
                                                <div class="case-study-img">
                                                   <img width="600" height="750"
                                                      src="../wp-content/uploads/2020/09/case5.jpg"
                                                      class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                      alt="" loading="lazy"
                                                      srcset="https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case5.jpg 600w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case5-240x300.jpg 240w"
                                                      sizes="(max-width: 600px) 100vw, 600px" />
                                                </div>
                                                <div class="case-study-content">
                                                   <h3><a href="../em_case_study/research-now/index.html">Research Now</a></h3>
                                                   <div class="case-category">
                                                      <p>
                                                         <span class="category-item">Photography</span>
                                                      </p>
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="single-case-study">
                                                <div class="case-study-img">
                                                   <img width="600" height="750"
                                                      src="../wp-content/uploads/2020/09/case3.jpg"
                                                      class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                      alt="" loading="lazy"
                                                      srcset="https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case3.jpg 600w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case3-240x300.jpg 240w"
                                                      sizes="(max-width: 600px) 100vw, 600px" />
                                                </div>
                                                <div class="case-study-content">
                                                   <h3><a href="../em_case_study/product-design/index.html">Product Design</a></h3>
                                                   <div class="case-category">
                                                      <p>
                                                         <span class="category-item">Design</span>
                                                      </p>
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="single-case-study">
                                                <div class="case-study-img">
                                                   <img width="600" height="750"
                                                      src="../wp-content/uploads/2020/09/case6.jpg"
                                                      class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                      alt="" loading="lazy"
                                                      srcset="https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case6.jpg 600w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case6-240x300.jpg 240w"
                                                      sizes="(max-width: 600px) 100vw, 600px" />
                                                </div>
                                                <div class="case-study-content">
                                                   <h3><a href="../em_case_study/growth-strategies/index.html">Growth Strategies</a>
                                                   </h3>
                                                   <div class="case-category">
                                                      <p>
                                                         <span class="category-item">Design</span>
                                                      </p>
                                                   </div>
                                                </div>
                                             </div>

                                             <div class="single-case-study">
                                                <div class="case-study-img">
                                                   <img width="600" height="750"
                                                      src="../wp-content/uploads/2020/09/case4.jpg"
                                                      class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                      alt="" loading="lazy"
                                                      srcset="https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case4.jpg 600w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/case4-240x300.jpg 240w"
                                                      sizes="(max-width: 600px) 100vw, 600px" />
                                                </div>
                                                <div class="case-study-content">
                                                   <h3><a href="../em_case_study/creative-mind/index.html">Creative Mind</a></h3>
                                                   <div class="case-category">
                                                      <p>
                                                         <span class="category-item">Development</span>
                                                      </p>
                                                   </div>
                                                </div>
                                             </div>

                                          </div>
                                       </div>
                                       <script>
                                          jQuery(document).ready(function($) {
                                             "use strict";
                                             $('.case_study_three').owlCarousel({
                                                loop: true,
                                                margin: 15,
                                                dots: false,
                                                nav: false,
                                                autoplay: true,
                                                navText: ["<i class='flaticon-back''></i>", "<i class='flaticon-next-1''></i>"],
                                                responsive: {
                                                   0: {
                                                      items: 1
                                                   },
                                                   768: {
                                                      items: 2
                                                   },
                                                   992: {
                                                      items: 3
                                                   },
                                                   1000: {
                                                      items: 4
                                                   },
                                                   1500: {
                                                      items: 4
                                                   },
                                                   1920: {
                                                      items: 4
                                                   }
                                                }
                                             });
                                          });
                                       </script>

                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-d7703a6 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="d7703a6" data-element_type="section" id="team"
                  data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-ee33c5a"
                           data-id="ee33c5a" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <div
                                    class="elementor-element elementor-element-26e0229 elementor-widget elementor-widget-sectiontitle"
                                    data-id="26e0229" data-element_type="widget" data-widget_type="sectiontitle.default">
                                    <div class="elementor-widget-container">
                                       <div class="">
                                          <div class="section-title t_center">
                                             <h5>// Meet Our Team Member</h5>
                                             <div class="title">
                                                <h3>Our Leadership Team</h3>
                                                <h2><span></span></h2>
                                             </div>

                                             <p>Synergistically enhance intuitive systems vis-a-vis effective manufactured<br>
                                                envisioneer unique markets for virtual total done.</p>

                                          </div>
                                       </div>

                                    </div>
                                 </div>
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-eb2eb3f elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="eb2eb3f" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-f47a0df"
                                             data-id="f47a0df" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-c2df834 elementor-widget elementor-widget-team"
                                                      data-id="c2df834" data-element_type="widget"
                                                      data-widget_type="team.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="rs-team-grid rs-team team-grid-style8 itsoft_lite_box">
                                                            <div class="team-item">
                                                               <div class="team-inner-wrap">
                                                                  <div class="image-wrap">
                                                                     <a class="pointer-events" href="#rs_popupBox_453686"
                                                                        data-effect="mfp-zoom-in">
                                                                        <img src="{{ asset('site/uploads/2021/07/team-1-1.png') }}"
                                                                           alt="https://itsoft.dreamitsolution.net/wp-content/uploads/2021/07/team-1-1.png" />
                                                                     </a>
                                                                     <div class="social-icons1">
                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-facebook"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-twitter"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-linkedin"></i>
                                                                        </a>

                                                                     </div>
                                                                  </div>

                                                                  <div class="team-content">
                                                                     <div class="team-btn"><i class="fas fa-share-alt"></i></div>
                                                                     <div class="member-desc">
                                                                        <h3 class="team-name"><a class="pointer-events"
                                                                              href="#rs_popupBox_453686">Anjel Louppy</a></h3>
                                                                        <span class="team-title">UI/UX Designer</span>
                                                                     </div>
                                                                     <div class="social-icons">
                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-facebook"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-twitter"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-linkedin"></i>
                                                                        </a>

                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>


                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-ca66ab3"
                                             data-id="ca66ab3" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-adc4d9a elementor-widget elementor-widget-team"
                                                      data-id="adc4d9a" data-element_type="widget"
                                                      data-widget_type="team.default">
                                                      <div class="elementor-widget-container">




                                                         <div class="rs-team-grid rs-team team-grid-style8 itsoft_lite_box">
                                                            <div class="team-item">
                                                               <div class="team-inner-wrap">
                                                                  <div class="image-wrap">
                                                                     <a class="pointer-events" href="#rs_popupBox_3515636"
                                                                        data-effect="mfp-zoom-in">
                                                                        <img src="{{ asset('site/uploads/2021/07/team-2.png') }}"
                                                                           alt="https://itsoft.dreamitsolution.net/wp-content/uploads/2021/07/team-2-1.png" />
                                                                     </a>
                                                                     <div class="social-icons1">
                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-facebook"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-twitter"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-linkedin"></i>
                                                                        </a>

                                                                     </div>
                                                                  </div>

                                                                  <div class="team-content">
                                                                     <div class="team-btn"><i class="fas fa-share-alt"></i></div>
                                                                     <div class="member-desc">
                                                                        <h3 class="team-name"><a class="pointer-events"
                                                                              href="#rs_popupBox_3515636">Armand Ringo</a></h3>
                                                                        <span class="team-title">Web Developer</span>
                                                                     </div>
                                                                     <div class="social-icons">
                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-facebook"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-twitter"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-linkedin"></i>
                                                                        </a>

                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>


                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-3ae6f15"
                                             data-id="3ae6f15" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-5dbdd34 elementor-widget elementor-widget-team"
                                                      data-id="5dbdd34" data-element_type="widget"
                                                      data-widget_type="team.default">
                                                      <div class="elementor-widget-container">




                                                         <div class="rs-team-grid rs-team team-grid-style8 itsoft_lite_box">
                                                            <div class="team-item">
                                                               <div class="team-inner-wrap">
                                                                  <div class="image-wrap">
                                                                     <a class="pointer-events" href="#rs_popupBox_1766458"
                                                                        data-effect="mfp-zoom-in">
                                                                        <img src="{{ asset('site/uploads/2021/07/team-3.png') }}"
                                                                           alt="https://itsoft.dreamitsolution.net/wp-content/uploads/2021/07/team-3.png" />
                                                                     </a>
                                                                     <div class="social-icons1">
                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-facebook"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-twitter"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-linkedin"></i>
                                                                        </a>

                                                                     </div>
                                                                  </div>

                                                                  <div class="team-content">
                                                                     <div class="team-btn"><i class="fas fa-share-alt"></i></div>
                                                                     <div class="member-desc">
                                                                        <h3 class="team-name"><a class="pointer-events"
                                                                              href="#rs_popupBox_1766458">Linda Keeble</a></h3>
                                                                        <span class="team-title">App Developer</span>
                                                                     </div>
                                                                     <div class="social-icons">
                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-facebook"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-twitter"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-linkedin"></i>
                                                                        </a>

                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>


                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-16608af"
                                             data-id="16608af" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-7d7cb01 elementor-widget elementor-widget-team"
                                                      data-id="7d7cb01" data-element_type="widget"
                                                      data-widget_type="team.default">
                                                      <div class="elementor-widget-container">




                                                         <div class="rs-team-grid rs-team team-grid-style8 itsoft_lite_box">
                                                            <div class="team-item">
                                                               <div class="team-inner-wrap">
                                                                  <div class="image-wrap">
                                                                     <a class="pointer-events" href="#rs_popupBox_418704"
                                                                        data-effect="mfp-zoom-in">
                                                                        <img src="{{ asset('site/uploads/2021/07/team-4.png') }}"
                                                                           alt="https://itsoft.dreamitsolution.net/wp-content/uploads/2021/07/team-4.png" />
                                                                     </a>
                                                                     <div class="social-icons1">
                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-facebook"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-twitter"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-linkedin"></i>
                                                                        </a>

                                                                     </div>
                                                                  </div>

                                                                  <div class="team-content">
                                                                     <div class="team-btn"><i class="fas fa-share-alt"></i></div>
                                                                     <div class="member-desc">
                                                                        <h3 class="team-name"><a class="pointer-events"
                                                                              href="#rs_popupBox_418704">Erika Hedrick</a></h3>
                                                                        <span class="team-title">Web Designer</span>
                                                                     </div>
                                                                     <div class="social-icons">
                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-facebook"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-twitter"></i>
                                                                        </a>

                                                                        <a href="#" class="social-icon">
                                                                           <i class="fa fa-linkedin"></i>
                                                                        </a>

                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>


                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-f86883e elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="f86883e" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6ec3861"
                           data-id="6ec3861" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-28c5317 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="28c5317" data-element_type="section"
                                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-252426c"
                                             data-id="252426c" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-1e74082 elementor-widget elementor-widget-counter-two"
                                                      data-id="1e74082" data-element_type="widget"
                                                      data-widget_type="counter-two.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="single_counter style-2 ">
                                                            <div class="single_counter_inner">
                                                               <div class="counter_icon">
                                                               </div>
                                                               <div class="counter_content">
                                                                  <div class="countr_text">
                                                                     <h1>13</h1>
                                                                     <h3>K+</h3>
                                                                  </div>
                                                                  <div class="counter_title">
                                                                     <h4>OUR CLIENT<br>BASED AWARDS</h4>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-215dca3"
                                             data-id="215dca3" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-a5e3e73 elementor-widget elementor-widget-counter-two"
                                                      data-id="a5e3e73" data-element_type="widget"
                                                      data-widget_type="counter-two.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="single_counter style-2 ">
                                                            <div class="single_counter_inner">
                                                               <div class="counter_icon">
                                                               </div>
                                                               <div class="counter_content">
                                                                  <div class="countr_text">
                                                                     <h1>13</h1>
                                                                     <h3>K+</h3>
                                                                  </div>
                                                                  <div class="counter_title">
                                                                     <h4>OUR CLIENT<br>BASED AWARDS</h4>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-5469cda"
                                             data-id="5469cda" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-bea1629 elementor-widget elementor-widget-counter-two"
                                                      data-id="bea1629" data-element_type="widget"
                                                      data-widget_type="counter-two.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="single_counter style-2 ">
                                                            <div class="single_counter_inner">
                                                               <div class="counter_icon">
                                                               </div>
                                                               <div class="counter_content">
                                                                  <div class="countr_text">
                                                                     <h1>56</h1>
                                                                     <h3>K+</h3>
                                                                  </div>
                                                                  <div class="counter_title">
                                                                     <h4>VISITED <br>CONFERENCES</h4>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-78a3b92"
                                             data-id="78a3b92" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-7d6cf57 two elementor-widget elementor-widget-counter-two"
                                                      data-id="7d6cf57" data-element_type="widget"
                                                      data-widget_type="counter-two.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="single_counter style-2 ">
                                                            <div class="single_counter_inner">
                                                               <div class="counter_icon">
                                                               </div>
                                                               <div class="counter_content">
                                                                  <div class="countr_text">
                                                                     <h1>17</h1>
                                                                     <h3>K+</h3>
                                                                  </div>
                                                                  <div class="counter_title">
                                                                     <h4>TOTAL AWARDS<br> WINING</h4>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-8c6e50a elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="8c6e50a" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-0c3aff5"
                           data-id="0c3aff5" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <div class="elementor-element elementor-element-3c184ee elementor-widget elementor-widget-html"
                                    data-id="3c184ee" data-element_type="widget" data-widget_type="html.default">
                                    <div class="elementor-widget-container">
                                       <div class="skill-single-box">
                                          <div class="skill-thumb">
                                             <img src="{{ asset('site/uploads/2021/08/mission.png') }}" alt="">
                                          </div>
                                          <div class="skill-shape-thumb">
                                             <div class="skill-img shape1 bounce-animate3">
                                                <img src="{{ asset('site/uploads/2021/08/miss-1.png') }}" alt="">
                                             </div>
                                             <div class="skill-img shape-2">
                                                <img src="{{ asset('site/uploads/2021/08/miss-2.png') }}" alt="">
                                             </div>
                                             <div class="skill-img shape-3">
                                                <img src="{{ asset('site/uploads/2021/08/miss-22.png') }}" alt="">
                                             </div>
                                             <div class="skill-img shape-4 bounce-animate5">
                                                <img src="{{ asset('site/uploads/2021/08/miss-4.png') }}" alt="">
                                             </div>
                                             <div class="skill-img shape-5 bounce-animate3">
                                                <img src="{{ asset('site/uploads/2021/08/miss-5.png') }}" alt="">
                                             </div>
                                             <div class="skill-img shape-6 bounce-animate5">
                                                <img src="{{ asset('site/uploads/2021/08/miss-6.png') }}" alt="">
                                             </div>
                                             <div class="skill-img shape-7">
                                                <img src="{{ asset('site/uploads/2021/08/miss-7.png') }}" alt="">
                                             </div>
                                             <div class="skill-img shape-8">
                                                <img src="{{ asset('site/uploads/2021/08/miss-10-1.png') }}" alt="">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div
                           class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-2102bbe"
                           data-id="2102bbe" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <div
                                    class="elementor-element elementor-element-9326f55 elementor-widget elementor-widget-sectiontitle"
                                    data-id="9326f55" data-element_type="widget" data-widget_type="sectiontitle.default">
                                    <div class="elementor-widget-container">
                                       <div class="">
                                          <div class="section-title t_left">
                                             <h5>// Discover Our Company</h5>
                                             <div class="title">
                                                <h3>Globally transform Client</h3>
                                                <h2>based <span>Benefits</span></h2>
                                             </div>

                                             <p>Energistically implement ubiquitous mindshare without turnkey time is
                                                Efficiently aggregate empowered experiences.</p>

                                          </div>
                                       </div>

                                    </div>
                                 </div>
                                 <div
                                    class="elementor-element elementor-element-a5681e5 elementor-widget elementor-widget-progress"
                                    data-id="a5681e5" data-element_type="widget" data-widget_type="progress.default">
                                    <div class="elementor-widget-container">
                                       <span class="elementor-title">Digital Marketing</span>
                                       <div class="elementor-progress-wrapper" role="progressbar" aria-valuemin="0"
                                          aria-valuemax="100" aria-valuenow="60" aria-valuetext="">
                                          <div class="elementor-progress-bar" data-max="60">
                                             <span class="elementor-progress-text"></span>
                                             <span class="elementor-progress-percentage">60%</span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div
                                    class="elementor-element elementor-element-f12f16c elementor-widget elementor-widget-progress"
                                    data-id="f12f16c" data-element_type="widget" data-widget_type="progress.default">
                                    <div class="elementor-widget-container">
                                       <span class="elementor-title">UI/UX Design</span>
                                       <div class="elementor-progress-wrapper" role="progressbar" aria-valuemin="0"
                                          aria-valuemax="100" aria-valuenow="50" aria-valuetext="">
                                          <div class="elementor-progress-bar" data-max="50">
                                             <span class="elementor-progress-text"></span>
                                             <span class="elementor-progress-percentage">50%</span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div
                                    class="elementor-element elementor-element-21278e0 elementor-widget elementor-widget-progress"
                                    data-id="21278e0" data-element_type="widget" data-widget_type="progress.default">
                                    <div class="elementor-widget-container">
                                       <span class="elementor-title">Web Develioment</span>
                                       <div class="elementor-progress-wrapper" role="progressbar" aria-valuemin="0"
                                          aria-valuemax="100" aria-valuenow="90" aria-valuetext="">
                                          <div class="elementor-progress-bar" data-max="90">
                                             <span class="elementor-progress-text"></span>
                                             <span class="elementor-progress-percentage">90%</span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-4c6c229 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="4c6c229" data-element_type="section"
                  data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-0a60d79"
                           data-id="0a60d79" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <div
                                    class="elementor-element elementor-element-77601d6 elementor-widget elementor-widget-sectiontitle"
                                    data-id="77601d6" data-element_type="widget" data-widget_type="sectiontitle.default">
                                    <div class="elementor-widget-container">
                                       <div class="">
                                          <div class="section-title t_center">
                                             <h5>// Testimonial</h5>
                                             <div class="title">
                                                <h3>What Client Say</h3>
                                                <h2><span></span></h2>
                                             </div>

                                             <p></p>

                                          </div>
                                       </div>

                                    </div>
                                 </div>
                                 <div
                                    class="elementor-element elementor-element-6bf7083 elementor-widget elementor-widget-testimonial"
                                    data-id="6bf7083" data-element_type="widget" data-widget_type="testimonial.default">
                                    <div class="elementor-widget-container">



                                       <div class="testimonial_list owl-loaded owl-carousel curosel-style style-five">

                                          <div class="single_testimonial default-style">

                                             <div class="testi_content">
                                                <div class="testi_text">
                                                   Excellent !! </div>
                                             </div>
                                             <div class="client-details">
                                                <div class="testi_thumb">
                                                   <img src="{{ asset('site/uploads/2021/07/review-1.png') }}" alt="">
                                                </div>
                                                <div class="testi_title">
                                                   <h2>David Smith</h2><span>Designer</span>
                                                   <div class="reviews_rating">


                                                      <div class="testi-star">
                                                         <i class="fa fa-star active"></i>
                                                         <i class="fa fa-star active"></i>
                                                         <i class="fa fa-star active"></i>
                                                         <i class="fa fa-star"></i>
                                                         <i class="fa fa-star"></i>
                                                      </div>

                                                   </div><!-- .reviews_rating -->
                                                </div>
                                             </div>
                                          </div>
                                          <div class="single_testimonial default-style">

                                             <div class="testi_content">
                                                <div class="testi_text">
                                                   Very Good !!! </div>
                                             </div>
                                             <div class="client-details">
                                                <div class="testi_thumb">
                                                   <img src="{{ asset('site/uploads/2021/07/review-2.png') }}" alt="">
                                                </div>
                                                <div class="testi_title">
                                                   <h2>Lisa Palmer</h2><span>App Developer</span>
                                                   <div class="reviews_rating">


                                                      <div class="testi-star">
                                                         <i class="fa fa-star active"></i>
                                                         <i class="fa fa-star active"></i>
                                                         <i class="fa fa-star active"></i>
                                                         <i class="fa fa-star"></i>
                                                         <i class="fa fa-star"></i>
                                                      </div>

                                                   </div><!-- .reviews_rating -->
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                       <script>
                                          jQuery(document).ready(function($) {
                                             "use strict";
                                             $('.testimonial_list').owlCarousel({
                                                loop: true,
                                                autoplay: false,
                                                autoplayTimeout: 10000,
                                                dots: true,
                                                nav: false,
                                                navText: ["<i class='flaticon-back-1'></i>", "<i class='flaticon-next'></i>"],
                                                responsive: {
                                                   0: {
                                                      items: 1
                                                   },
                                                   768: {
                                                      items: 1
                                                   },
                                                   992: {
                                                      items: 1
                                                   },
                                                   1000: {
                                                      items: 1
                                                   },
                                                   1920: {
                                                      items: 1,
                                                   }
                                                }
                                             })
                                          });
                                       </script>

                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-d6afdd1 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="d6afdd1" data-element_type="section" id="blog">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9a2dae5"
                           data-id="9a2dae5" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-3e67a2e elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="3e67a2e" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-dd992c0"
                                             data-id="dd992c0" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-b92855f elementor-widget elementor-widget-sectiontitle"
                                                      data-id="b92855f" data-element_type="widget"
                                                      data-widget_type="sectiontitle.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="">
                                                            <div class="section-title t_left">
                                                               <h5>// Latest Blog Posts</h5>
                                                               <div class="title">
                                                                  <h3>Read our latest insights.</h3>
                                                                  <h2><span></span></h2>
                                                               </div>

                                                               <p></p>

                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-28af822"
                                             data-id="28af822" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-1960684 elementor-widget elementor-widget-ditbutton"
                                                      data-id="1960684" data-element_type="widget"
                                                      data-widget_type="ditbutton.default">
                                                      <div class="elementor-widget-container">

                                                         <div class="dit-button style-one">
                                                            <a href="#">View All <i aria-hidden="true"
                                                                  class="flaticon flaticon-long-arrow-pointing-to-the-right"></i></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                                 <div
                                    class="elementor-element elementor-element-58e0376 elementor-widget elementor-widget-blogpost"
                                    data-id="58e0376" data-element_type="widget" data-widget_type="blogpost.default">
                                    <div class="elementor-widget-container">
                                       <div class=" blog_style_three">
                                          <div class="blog_wrap blog_carousel owl-carousel owl-loaded curosel-style">
                                             <!-- single blog -->
                                             @if (!empty($blogs))
                                                @foreach ($blogs as $item)
                                                   <div class="col-md-12 col-xs-12 col-sm-12 ">
                                                      <div class="single_blog_adn">
                                                         <div id="post-1498"
                                                            class="post-1498 post type-post status-publish format-standard has-post-thumbnail hentry category-development">
                                                            <div class="itsoft-single-blog_adn ">
                                                               <!-- BLOG THUMB -->
                                                               <div class="itsoft-blog-thumb_adn ">
                                                                  <a href="../how-to-make-website-with-elementor/index.html"><img
                                                                        width="390" height="290"
                                                                        src="{{ asset($item->image) }}"
                                                                        class="attachment-itsoft-blog-default
                                                                  size-itsoft-blog-default wp-post-image"
                                                                        alt="{{ $item->title }}" loading="lazy" /></a>
                                                               </div>
                                                               <!-- BLOG CONTENT -->
                                                               <div class="em-blog-content-area_adn">
                                                                  <!-- BLOG META -->
                                                                  <div class="blog-date">
                                                                     <p>{{ \Carbon\Carbon::parse($item->published_date)->format('d') }}
                                                                     </p>
                                                                     <p>{{ \Carbon\Carbon::parse($item->published_date)->format('M') }}
                                                                     </p>
                                                                  </div>
                                                                  <div class="blog-category">
                                                                     <ul class="post-categories">
                                                                        <li>
                                                                           @if (!empty($item->categories))
                                                                              @foreach ($item->categories as $category)
                                                                                 <a href="../category/development/index.html"
                                                                                    rel="category tag">{{ getCategoryNameByID($category->category_id) }}</a>
                                                                              @endforeach
                                                                           @endif
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                                  <!-- BLOG TITLE -->
                                                                  <div class="blog-page-title_adn ">
                                                                     <h2><a
                                                                           href="{{ route('news.detail', $item->slug) }}">{{ $item->title ?? '' }}</a>
                                                                     </h2>
                                                                  </div>
                                                                  <!-- BLOG BUTTON -->
                                                                  <div class="itsoft-blog-readmore">
                                                                     <a href="{{ route('news.detail', $item->slug) }}"
                                                                        class="learn_btn">Read More<i aria-hidden="true"
                                                                           class="flaticon-right-arrow-3"></i></a>
                                                                  </div>
                                                               </div><!-- .em-blog-content-area_adn -->
                                                            </div>
                                                         </div> <!--  END SINGLE BLOG -->

                                                      </div>
                                                   </div>
                                                @endforeach
                                             @endif

                                          </div>
                                       </div>

                                       <script>
                                          jQuery(document).ready(function($) {
                                             "use strict";
                                             $('.blog_carousel').owlCarousel({
                                                dots: true,
                                                nav: true,
                                                autoplayTimeout: 10000,
                                                navText: ["<i class='flaticon-left-arrow-3'></i>", "<i class='flaticon-right-arrow-3''></i>"],
                                                responsive: {
                                                   0: {
                                                      items: 1
                                                   },
                                                   768: {
                                                      items: 2
                                                   },
                                                   992: {
                                                      items: 3
                                                   },
                                                   1920: {
                                                      items: 3
                                                   }
                                                }
                                             });
                                          });
                                       </script>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-673c037 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="673c037" data-element_type="section" id="contact"
                  data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-570001e"
                           data-id="570001e" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-e5c905a elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="e5c905a" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ea1c2eb"
                                             data-id="ea1c2eb" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-8065771 elementor-widget elementor-widget-sectiontitle"
                                                      data-id="8065771" data-element_type="widget"
                                                      data-widget_type="sectiontitle.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="">
                                                            <div class="section-title t_left">
                                                               <h5>Contact Details</h5>
                                                               <div class="title">
                                                                  <h3>Contact us</h3>
                                                                  <h2><span></span></h2>
                                                               </div>

                                                               <div class="bar-main">
                                                                  <div class="bar bar-big"></div>
                                                               </div>

                                                               <p>Give us a call or drop by anytime, we endeavour to answer all
                                                                  enquiries within 24 hours on business days. We will be happy to
                                                                  answer your questions.</p>

                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-30db46b elementor-position-left elementor-view-default elementor-mobile-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                      data-id="30db46b" data-element_type="widget"
                                                      data-widget_type="icon-box.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                                                               <span class="elementor-icon elementor-animation-">
                                                                  <i aria-hidden="true" class="fas fa-globe"></i> </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                               <h3 class="elementor-icon-box-title">
                                                                  <span>
                                                                     Our Address: </span>
                                                               </h3>
                                                               <p class="elementor-icon-box-description">
                                                                  {{ $sitedetail->address ?? '' }} </p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-eaa88eb elementor-position-left elementor-view-default elementor-mobile-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                      data-id="eaa88eb" data-element_type="widget"
                                                      data-widget_type="icon-box.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                                                               <span class="elementor-icon elementor-animation-">
                                                                  <i aria-hidden="true" class="far fa-envelope"></i> </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                               <h3 class="elementor-icon-box-title">
                                                                  <span>
                                                                     Our Mailbox: </span>
                                                               </h3>
                                                               <p class="elementor-icon-box-description">
                                                                  <a href="mailto:{{ $sitedetail->email }}">
                                                                     {{ $sitedetail->email ?? '' }}</a>
                                                               </p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-570bf91 elementor-position-left elementor-view-default elementor-mobile-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                      data-id="570bf91" data-element_type="widget"
                                                      data-widget_type="icon-box.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                                                               <span class="elementor-icon elementor-animation-">
                                                                  <i aria-hidden="true" class="fas fa-phone-volume"></i> </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                               <h3 class="elementor-icon-box-title">
                                                                  <span>
                                                                     Mobile: </span>
                                                               </h3>
                                                               <p class="elementor-icon-box-description">
                                                                  <a
                                                                     href="tel:{{ $sitedetail->mobile_no ?? '' }}">{{ $sitedetail->mobile_no ?? '' }}</a>
                                                               </p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-62db77b"
                                             data-id="62db77b" data-element_type="column"
                                             data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-9d92ad5 elementor-widget elementor-widget-sectiontitle"
                                                      data-id="9d92ad5" data-element_type="widget"
                                                      data-widget_type="sectiontitle.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="">
                                                            <div class="section-title t_left">
                                                               <h5>Get In Touch</h5>
                                                               <div class="title">
                                                                  <h3>Ready to Get Started?</h3>
                                                                  <h2><span></span></h2>
                                                               </div>

                                                               <div class="bar-main">
                                                                  <div class="bar bar-big"></div>
                                                               </div>

                                                               <p>Your email address will not be published. Required fields are
                                                                  marked *</p>

                                                            </div>
                                                         </div>

                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-ccfb4c6 elementor-widget elementor-widget-shortcode"
                                                      data-id="ccfb4c6" data-element_type="widget"
                                                      data-widget_type="shortcode.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="elementor-shortcode">
                                                            <div role="form" class="wpcf7" id="wpcf7-f1086-p504-o1"
                                                               lang="en-US" dir="ltr">
                                                               <div class="screen-reader-response">
                                                                  <p role="status" aria-live="polite" aria-atomic="true"></p>
                                                                  <ul></ul>
                                                               </div>
                                                               <form
                                                                  action="../../external.html?link=https://itsoft.dreamitsolution.net/landing-page-01/#wpcf7-f1086-p504-o1"
                                                                  method="post" class="wpcf7-form init"
                                                                  novalidate="novalidate" data-status="init">
                                                                  <div style="display: none;">
                                                                     <input type="hidden" name="_wpcf7" value="1086" />
                                                                     <input type="hidden" name="_wpcf7_version"
                                                                        value="5.6.3" />
                                                                     <input type="hidden" name="_wpcf7_locale"
                                                                        value="en_US" />
                                                                     <input type="hidden" name="_wpcf7_unit_tag"
                                                                        value="wpcf7-f1086-p504-o1" />
                                                                     <input type="hidden" name="_wpcf7_container_post"
                                                                        value="504" />
                                                                     <input type="hidden" name="_wpcf7_posted_data_hash"
                                                                        value="" />
                                                                  </div>
                                                                  <div class="form-area contact-form">
                                                                     <div class="form-inner">
                                                                        <div class="form-controls row">
                                                                           <div class="col-md-12">
                                                                              <div class="form-group">
                                                                                 <span class="wpcf7-form-control-wrap"
                                                                                    data-name="sub"><input type="text"
                                                                                       name="sub" value=""
                                                                                       size="40"
                                                                                       class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                                                       aria-required="true" aria-invalid="false"
                                                                                       placeholder="Your Name" /></span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="col-md-12">
                                                                              <div class="form-group">
                                                                                 <span class="wpcf7-form-control-wrap"
                                                                                    data-name="sub2"><input type="email"
                                                                                       name="sub2" value=""
                                                                                       size="40"
                                                                                       class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                                                       aria-required="true" aria-invalid="false"
                                                                                       placeholder="Your Email" /></span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="col-md-12">
                                                                              <div class="form-group">
                                                                                 <span class="wpcf7-form-control-wrap"
                                                                                    data-name="sub3"><input type="text"
                                                                                       name="sub3" value=""
                                                                                       size="40"
                                                                                       class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                                                       aria-required="true" aria-invalid="false"
                                                                                       placeholder="Phone Number" /></span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="col-md-12">
                                                                              <div class="form-group">
                                                                                 <span class="wpcf7-form-control-wrap"
                                                                                    data-name="placeholder">
                                                                                    <textarea name="placeholder" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea"
                                                                                       aria-invalid="false">Your Message</textarea>
                                                                                 </span>
                                                                              </div>
                                                                           </div>
                                                                           <div class="col-md-12">
                                                                              <div class="form-group">
                                                                                 <div class="buttons">
                                                                                    <input type="submit" value="Free Consulting"
                                                                                       class="wpcf7-form-control has-spinner wpcf7-submit button" />
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="wpcf7-response-output" aria-hidden="true"></div>
                                                               </form>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section
                  class="elementor-section elementor-top-section elementor-element elementor-element-79729b0 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                  data-id="79729b0" data-element_type="section"
                  data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-container elementor-column-gap-default">
                     <div class="elementor-row">
                        <div
                           class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5b8b859"
                           data-id="5b8b859" data-element_type="column">
                           <div class="elementor-column-wrap elementor-element-populated">
                              <div class="elementor-widget-wrap">
                                 <section
                                    class="elementor-section elementor-inner-section elementor-element elementor-element-c9184bd elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                    data-id="c9184bd" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                       <div class="elementor-row">
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f36f785"
                                             data-id="f36f785" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-64ac366 elementor-widget elementor-widget-heading"
                                                      data-id="64ac366" data-element_type="widget"
                                                      data-widget_type="heading.default">
                                                      <div class="elementor-widget-container">
                                                         <h2 class="elementor-heading-title elementor-size-default">Subscribe our
                                                            newsletter</h2>
                                                      </div>
                                                   </div>
                                                   <div
                                                      class="elementor-element elementor-element-77a9db8 elementor-widget elementor-widget-heading"
                                                      data-id="77a9db8" data-element_type="widget"
                                                      data-widget_type="heading.default">
                                                      <div class="elementor-widget-container">
                                                         <h2 class="elementor-heading-title elementor-size-default">Please
                                                            Subscribe our news letter and and get update.</h2>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-13cb9f0"
                                             data-id="13cb9f0" data-element_type="column">
                                             <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                   <div
                                                      class="elementor-element elementor-element-6a161d2 elementor-widget elementor-widget-shortcode"
                                                      data-id="6a161d2" data-element_type="widget"
                                                      data-widget_type="shortcode.default">
                                                      <div class="elementor-widget-container">
                                                         <div class="elementor-shortcode">
                                                            <script>
                                                               (function() {
                                                                  window.mc4wp = window.mc4wp || {
                                                                     listeners: [],
                                                                     forms: {
                                                                        on: function(evt, cb) {
                                                                           window.mc4wp.listeners.push({
                                                                              event: evt,
                                                                              callback: cb
                                                                           });
                                                                        }
                                                                     }
                                                                  }
                                                               })();
                                                            </script>
                                                            <!-- Mailchimp for WordPress v4.8.8 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
                                                            <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-1176"
                                                               method="post" data-id="1176" data-name="Subscribe form">
                                                               <div class="mc4wp-form-fields">
                                                                  <p class="mailchimp-form">
                                                                     <input type="email" name="email"
                                                                        placeholder="Enter Your Email...." required="">
                                                                     <button type="submit"> Subscribe</button>
                                                                  </p>
                                                               </div><label style="display: none !important;">Leave this field
                                                                  empty if you're human: <input type="text"
                                                                     name="_mc4wp_honeypot" value="" tabindex="-1"
                                                                     autocomplete="off" /></label><input type="hidden"
                                                                  name="_mc4wp_timestamp" value="1663587598" /><input
                                                                  type="hidden" name="_mc4wp_form_id" value="1176" /><input
                                                                  type="hidden" name="_mc4wp_form_element_id"
                                                                  value="mc4wp-form-1" />
                                                               <div class="mc4wp-response"></div>
                                                            </form><!-- / Mailchimp for WordPress Plugin -->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
      </div>
   </div>
   @push('script')
      <script type="text/javascript">
      @endpush
   @endsection
