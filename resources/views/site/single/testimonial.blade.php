@section('content')
@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', asset($sitedetail->fb_image))
@section('title', isset($page_header) ? $page_header : $sitedetail->title)

@section('breadcrumb')
 <!-- ======= Breadcrumbs ======= -->
    <div id="banner-area" class="banner-area" style="background-image:url({{ asset($sitedetail->banner_image) }})">
      <div class="banner-text">
        <div class="container">
            <div class="row">
              <div class="col-lg-12">
                  <div class="banner-heading">
                    <h1 class="banner-title">Testimonial</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                          <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                          {{-- <li class="breadcrumb-item"><a href="#">Services</a></li> --}}
                          <li class="breadcrumb-item active" aria-current="page">Testimonial</li>
                        </ol>
                    </nav>
                  </div>
              </div><!-- Col end -->
            </div><!-- Row end -->
        </div><!-- Container end -->
      </div><!-- Banner text end -->
    </div><!-- Banner area end --> 
@endsection

@section('content')
<section id="main-container" class="main-container">
   <div class="container">
      <div class="row text-center">
         <div class="col-12">
            <h3 class="section-sub-title mb-4">{{ $sitedetail->testimonial_heading }}</h3>
         </div>
      </div>
      <!--/ Title row end -->

      <div class="row">
        @if(count($list) > 0)
          @foreach($list as $key => $item)
            <div class="col-lg-4 col-md-6">
              <div class="quote-item quote-border mt-5">
                 <div class="quote-text-border">
                    {!! $item->description !!}
                 </div>

                 <div class="quote-item-footer">
                    <img loading="lazy" class="testimonial-thumb" src="{{ asset($item->image) }}" alt="{{ $item->name }}">
                    <div class="quote-item-info">
                       <h3 class="quote-author">{{ $item->name }}</h3>
                       <span class="quote-subtext">{{ $item->designation }}, {{ $item->company_name }}</span>
                    </div>
                 </div>
              </div><!-- Quote item end -->
          </div><!-- End col md 4 -->
          @endforeach
        @endif
        
      </div><!-- Content row end -->

   </div><!-- Container end -->
</section><!-- Main container end -->
@endsection
