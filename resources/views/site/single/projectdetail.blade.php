@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', ($detail->image != '' ?  asset($detail->image) : asset($sitedetail->fb_image)))
@section('title', $detail->title)
@section('breadcrumb')
<!-- HERO
================================================== -->
<section class="page-banner-area banner-image" style=" background: url({{ asset($sitedetail->banner_image) }}) fixed 50% 50% no-repeat;">
    <div class="overlay"></div>
    <!-- Content -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-12 col-12 text-center">
                <div class="page-banner-content">
                    <h1 class="display-4 font-weight-bold">{{ $detail->title ?? '' }}</h1>
                    <nav >
                        <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('projects') }}">Projects</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $detail->title }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection

@section('content')
  <section class="section" id="single-project">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="project-lg-img">
                                <img src="{{ asset($detail->image) }}" alt="{{ $detail->title }}" class="img-fluid w-100">
                            </div>

                            <div class="project-details-info">
                                <div class="info-block-2">
                                    <h5>Client</h5>
                                    <p>{{ $detail->client ?? ''}}</p>
                                </div>
                                    <div class="info-block-2">
                                    <h5>Date</h5>
                                    <p>{{ $detail->project_date ?? ''}}</p>
                                </div>
                                <div class="info-block-2">
                                    <h5>Category</h5>
                                    <p>{{ $detail->category ?? ''}}</p>
                                </div>
                                <div class="info-block-2">
                                    <h5>Website</h5>
                                    <p>{{ $detail->website ?? '' }}</p>
                                </div>
                                <div class="info-block-2">
                                    <h5>Services</h5>
                                    <p>{{ $detail->services ?? '' }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        {!! $detail->description ?? '' !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
