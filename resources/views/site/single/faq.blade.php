@section('content')
@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', asset($sitedetail->fb_image))
@section('title', isset($page_header) ? $page_header : $sitedetail->title)

@section('breadcrumb')
 <!-- ======= Breadcrumbs ======= -->
    <div id="banner-area" class="banner-area" style="background-image:url({{ asset($sitedetail->banner_image) }})">
      <div class="banner-text">
        <div class="container">
            <div class="row">
              <div class="col-lg-12">
                  <div class="banner-heading">
                    <h1 class="banner-title">Faq</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                          <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                          {{-- <li class="breadcrumb-item"><a href="#">Services</a></li> --}}
                          <li class="breadcrumb-item active" aria-current="page">Faq</li>
                        </ol>
                    </nav>
                  </div>
              </div><!-- Col end -->
            </div><!-- Row end -->
        </div><!-- Container end -->
      </div><!-- Banner text end -->
    </div><!-- Banner area end -->
@endsection

@section('content')
<section id="main-container" class="main-container">
  <div class="container">

    <div class="row">
      <div class="col-lg-8 mb-5 mb-lg-0">
        {{-- <h3 class="border-title border-left mar-t0">Construction general</h3> --}}

        <div class="accordion accordion-group accordion-classic" id="construction-accordion">
          @if(count($list) > 0)
          @foreach($list as $key => $item)
            @if($key == 0)
              @php
                $show = 'show' ;
                $collapse = '' ;
                $value = 'true';
              @endphp
            @else
              @php
                $show = '' ;
                $collapse = 'collapsed' ;
                $value = 'false';
              @endphp
            @endif
            <div class="card">
              <div class="card-header p-0 bg-transparent" id="heading{{ $key }}">
                <h2 class="mb-0">
                  <button class="btn btn-block text-left {{ $collapse }}" type="button" data-toggle="collapse" data-target="#collapse{{ $key }}"
                    aria-expanded="{{ $value }}" aria-controls="collapse{{ $key }}">
                    {{ $item->title }}
                  </button>
                </h2>
              </div>

              <div id="collapse{{ $key }}" class="collapse {{ $show }}" aria-labelledby="heading{{ $key }}"
                data-parent="#construction-accordion">
                <div class="card-body">
                  {!! $item->description !!}
                </div>
              </div>
            </div>
          @endforeach
        @endif
        </div>
        <!--/ Accordion end -->

      </div><!-- Col end -->

      <div class="col-lg-4 mt-5 mt-lg-0">

        <div class="sidebar sidebar-right">
          <div class="widget recent-posts">
            <h3 class="widget-title">Recent Posts</h3>
            <ul class="list-unstyled">
              @if(count($recent_post) > 0)
                @foreach($recent_post as $item)
                  <li class="d-flex align-items-center">
                    <div class="posts-thumb">
                      <a href="{{ route('news.detail',$item->slug) }}"><img loading="lazy" alt="{{ $item->title }}" src="{{ asset($item->image) }}"></a>
                    </div>
                    <div class="post-info">
                      <h4 class="entry-title">
                        <a href="{{ route('news.detail',$item->slug) }}">{{ str_limit($item->short_description,100) }}</a>
                      </h4>
                    </div>
                  </li><!-- 1st post end-->
                @endforeach
              @endif


            </ul>

          </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

      </div><!-- Col end -->

    </div><!-- Content row end -->

  </div><!-- Container end -->
</section><!-- Main container end -->
@endsection
