@section('content')
@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', asset($sitedetail->fb_image))
@section('title', isset($page_header) ? $page_header : $sitedetail->title)
@php
  $title = isset($_GET['search']) ? $_GET['search'] : '';
@endphp

@section('breadcrumb')
 <!-- ======= Breadcrumbs ======= -->
    <div id="banner-area" class="banner-area" style="background-image:url({{ asset($sitedetail->banner_image) }})">
      <div class="banner-text">
        <div class="container">
            <div class="row">
              <div class="col-lg-12">
                  <div class="banner-heading">
                    <h1 class="banner-title">Search</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                          <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                          {{-- <li class="breadcrumb-item"><a href="#">Services</a></li> --}}
                          <li class="breadcrumb-item active" aria-current="page">Search</li>
                        </ol>
                    </nav>
                  </div>
              </div><!-- Col end -->
            </div><!-- Row end -->
        </div><!-- Container end -->
      </div><!-- Banner text end -->
    </div><!-- Banner area end --> 
@endsection

@section('content')
<section id="main-container" class="main-container">
  <div class="container">

    <div class="row">
      <div class="col-lg-8">
        <h3 class="border-title border-left mar-t0">{{ $page_header }}</h3>
       {{--  @if(count($projects) > 0)
          @foreach($projects as $item)
            <div class="sidebar sidebar-right">
              <div class="widget recent-posts">
                <h3 class="widget-title">Projects</h3>
                <ul class="list-unstyled">
                  <li class="d-flex align-items-center">
                    <div class="posts-thumb">
                      <a href="{{ route('project.detail',$item->slug) }}"><img loading="lazy" alt="{{ $item->title }}" src="{{ asset($item->image) }}"></a>
                    </div>
                    <div class="post-info">
                      <h4 class="entry-title">
                        <a href="{{ route('project.detail',$item->slug) }}">{{ $item->short_description }}</a>
                      </h4>
                    </div>
                  </li><!-- 1st post end-->
                </ul>

              </div><!-- Recent post end -->
            </div><!-- Sidebar end -->
          @endforeach
           <nav class="paging" aria-label="Page navigation example">
            <ul class="pagination">
              {{ $projects->links() }}
            </ul>
          </nav>
        @endif
        <br> --}}


       

        {{-- @if(count($services) > 0)
          @foreach($services as $item)
            <div class="sidebar sidebar-right">
              <div class="widget recent-posts">
                <h3 class="widget-title">Service</h3>
                <ul class="list-unstyled">
                  <li class="d-flex align-items-center">
                    <div class="posts-thumb">
                      <a href="{{ route('service.detail',$item->slug) }}"><img loading="lazy" alt="{{ $item->title }}" src="{{ asset($item->image) }}"></a>
                    </div>
                    <div class="post-info">
                      <h4 class="entry-title">
                        <a href="{{ route('service.detail',$item->slug) }}">{{ $item->short_description }}</a>
                      </h4>
                    </div>
                  </li><!-- 1st post end-->
                </ul>

              </div><!-- Recent post end -->
            </div><!-- Sidebar end -->
          @endforeach
          <nav class="paging" aria-label="Page navigation example">
            <ul class="pagination">
              {{ $services->links() }}
            </ul>
          </nav>
        @endif
        
        <br> --}}

        @if(count($news) > 0)
          
          <div class="sidebar sidebar-right">
            <div class="widget recent-posts">
              <h3 class="widget-title">News</h3>
              <ul class="list-unstyled">
                @foreach($news as $item)
                  <li class="d-flex align-items-center">
                    <div class="posts-thumb">
                      <a href="{{ route('news.detail',$item->slug) }}"><img loading="lazy" alt="{{ $item->title }}" src="{{ asset($item->image) }}"></a>
                    </div>
                    <div class="post-info">
                      <h4 class="entry-title">
                        <a href="{{ route('news.detail',$item->slug) }}">{{ $item->short_description }}</a>
                      </h4>
                    </div>
                  </li><!-- 1st post end-->
                @endforeach

              </ul>

            </div><!-- Recent post end -->
          </div><!-- Sidebar end -->
          <nav class="paging" aria-label="Page navigation example">
            <ul class="pagination">
              {{ $news->appends(['search' => $title])
                  ->links()
              }}
            </ul>
          </nav>
        @endif
        
        
        

      </div><!-- Col end -->

     <div class="col-lg-4 mt-5 mt-lg-0">

        <div class="sidebar sidebar-right">
          <div class="widget recent-posts">
            <h3 class="widget-title">Recent Posts</h3>
            <ul class="list-unstyled">
              @if(count($recent_post) > 0)
                @foreach($recent_post as $item)
                  <li class="d-flex align-items-center">
                    <div class="posts-thumb">
                      <a href="{{ route('news.detail',$item->slug) }}"><img loading="lazy" alt="{{ $item->title }}" src="{{ asset($item->image) }}"></a>
                    </div>
                    <div class="post-info">
                      <h4 class="entry-title">
                        <a href="{{ route('news.detail',$item->slug) }}">{{ str_limit($item->short_description,100) }}</a>
                      </h4>
                    </div>
                  </li><!-- 1st post end-->
                @endforeach
              @endif
              

            </ul>

          </div><!-- Recent post end -->
        </div><!-- Sidebar end -->

      </div><!-- Col end -->

    </div><!-- Content row end -->

  </div><!-- Container end -->
</section><!-- Main container end -->
@endsection
