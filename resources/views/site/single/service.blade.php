@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', $sitedetail->logo)
@section('fb_url', Request::url())
@section('title', $sitedetail->title)

@section('breadcrumb')
<!-- HERO
================================================== -->
<section class="page-banner-area banner-image" style=" background: url({{ asset($sitedetail->banner_image) }}) fixed 50% 50% no-repeat;">
    <div class="overlay"></div>
    <!-- Content -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-12 col-12 text-center">
                <div class="page-banner-content">
                     <h1 class="display-4 font-weight-bold">{{ $page_header }}</h1>
                    <nav >
                        <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $page_header }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection

@section('content')
    <section class="section" id="service">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-lg-7 pl-4 text-center">
                    <div class="service-heading">
                        <h1>{{ $sitedetail->service_heading ?? '' }}</h1>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                @if (!empty($services))
                    @foreach ($services as $item)
                        <div class="col-lg-4 col-md-6">
                            <div class="service-block media">
                                <div class="service-icon">
                                    <i class="{{ $item->icon }}"></i>
                                </div>
                                <div class="service-inner-content media-body">
                                    <h4>{{ $item->title ?? '' }}</h4>
                                    <p>{{ strip_tags(str_limit($item->description,200)) }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

    <section class="section" id="services-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 text-center">
                    <div class="section-heading">
                        <!-- Heading -->
                        <h2 class="section-title mb-2 text-white">
                            Web Services
                        </h2>

                        <!-- Subheading -->
                        <p class="mb-5 text-white">
                            Rapoo can be used to create anything from a small marketing page to a sophisticated website.
                        </p>
                    </div>
                </div>
            </div> <!-- / .row -->

            <div class="row">
                @if (!empty($web_services))
                    @foreach ($web_services as $item)
                        <div class="col-lg-4 col-sm-6 col-md-6 mb-30">
                            <div class="web-service-block">
                                <i class="{{ $item->icon }}"></i>
                                <h3>{{ $item->title }}</h3>
                                <p>{{ strip_tags(str_limit($item->description,200)) }}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection
