@extends('site.master')
@section('description', ($detail->meta_description != '' ? $detail->meta_description : $sitedetail->meta_descriptions))
@section('keywords', ($detail->meta_keywords != '' ? $detail->meta_keywords : $sitedetail->meta_keywords))
@section('fb_image', ($detail->image != '' ? asset($detail->image) : asset($sitedetail->fb_image)))
@section('title', $page_header)

@section('breadcrumb')
<!-- HERO
================================================== -->
<section class="page-banner-area banner-image" style=" background: url({{ asset($sitedetail->banner_image) }}) fixed 50% 50% no-repeat;">
    <div class="overlay"></div>
    <!-- Content -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-12 col-12 text-center">
                <div class="page-banner-content">
                    <h1 class="display-4 font-weight-bold">{{ $page_header }}</h1>
                    <nav >
                        <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $page_header }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection

@section('content')
   <section class="section" id="process">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 text-center">
                    <div class="section-heading">
                        <!-- Heading -->
                        <h2 class="section-title">
                            {{ $sitedetail->working_process_heading ?? '' }}
                        </h2>

                        <!-- Subheading -->
                        <p>
                            {{ $sitedetail->working_process_text ?? '' }}
                        </p>

                    </div>
                </div>
            </div> <!-- / .row -->

            <div class="row justify-content-center">
                @if (!empty($working_process))
                    @foreach ($working_process as $item)
                        <div class="col-lg-4 col-sm-6 col-md-6">
                            <div class="process-block">
                                <img src="{{ asset($item->image) }}" alt="{{ $item->title }}" class="img-fluid">

                                <h3>{{ $item->title }}</h3>
                                <p>{{ strip_tags(str_limit($item->description,200)) }}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

    <section class="section" id="projects-wrap">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-heading">
                        <h1 class="text-white">{{ $sitedetail->project_left_text ?? '' }}</h1>
                    </div>
                </div>
                <div class="col-lg-6">
                    <p class="lead text-white">{{ $sitedetail->project_right_text ?? '' }}</p>
                </div>
            </div>
        </div>
    </section>

    <section id="projects" class="section-bottom">
        <div class="container">
            <div class="row justify-content-center">
                @if (!empty($projects))
                    @foreach ($projects as $item)
                        <div class="col-lg-4 col-md-6 col-sm-6 mb-5">
                            <div class="single-project">
                                <img src="{{ asset($item->image) }}" alt="{{ $item->title }}" class="img-fluid">
                                <div class="project-content">
                                    <h4>{{ $item->title ?? '' }}</h4>
                                    <p>{{ strip_tags(str_limit($item->description,300)) }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>

            <div class="row py-4">
                <div class="col-lg-7 col-md-12 col-sm-12 ">
                    <div class="single-project">
                        <img src="{{ asset($sitedetail->project_image) }}" alt="{{ $sitedetail->title }}" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 ">
                    <div class="project-content-block">
                        <h2>{{ $sitedetail->single_project_heading ?? ''}}</h2>
                        <p>{{ $sitedetail->single_project_txt ?? '' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section" id="section-testimonial">
        <div class="container">
           <div class="row align-items-center">
                <div class="col-lg-4 col-sm-12 col-md-12">
                    <div class="section-heading testimonial-heading">
                        <h1>{{ $sitedetail->testimonial_heading ?? ''}}</h1>
                        <p>{{ $sitedetail->testimonial_text ?? '' }}</p>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-12 col-md-12">
                    <div class="row">
                        @if (!empty($testimonials))
                            @foreach ($testimonials as $item)
                                <div class="col-lg-6">
                                    <div class="test-inner ">
                                    <div class="test-author-thumb d-flex">
                                        <img src="{{ asset($item->image) }}" alt="Testimonial author" class="img-fluid">
                                        <div class="test-author-info">
                                            <h4>{{ $item->name }}</h4>
                                            <h6>{{ $item->designation }}</h6>
                                        </div>
                                    </div>
                                        {{ strip_tags(str_limit($item->description,200)) }}
                                        <i class="fa fa-quote-right"></i>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
 <section class="section" id="blog">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 text-center">
                    <div class="section-heading">
                        <!-- Heading -->
                        <h2 class="section-title">
                            {{ $sitedetail->blog_heading ?? '' }}
                        </h2>

                        <!-- Subheading -->
                        <p>
                           {{ $sitedetail->blog_txt ?? '' }}
                        </p>
                    </div>
                </div>
            </div> <!-- / .row -->

            <div class="row justify-content-center">
                @if (!empty($news))
                    @foreach ($news as $item)
                        <div class="col-lg-4 col-md-6">
                            <div class="blog-box">
                                <div class="blog-img-box">
                                    <img src="{{ asset($item->image) }}" alt="{{ $item->title }}" class="img-fluid blog-img">
                                </div>
                                <div class="single-blog">
                                    <div class="blog-content">
                                        <h6> {{ $item->published_date }}</h6>
                                        <a href="#">
                                            <h3 class="card-title">{{ $item->title }}</h3>
                                        </a>
                                        <p>{{ strip_tags(str_limit($item->description,200)) }}</p>
                                        <a href="#" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif


            </div>
        </div>
    </section>

  </section><!-- Main container end -->


@endsection
