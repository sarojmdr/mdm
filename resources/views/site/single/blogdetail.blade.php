@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', $detail->image != '' ? asset($detail->image) : asset($sitedetail->fb_image))
@section('title', $detail->title)
@section('breadcrumb')
   <div class="breadcumb-area">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="breadcumb-inner">
                  <h2>Single Blog</h2>
                  <ul>
                     <li><a href="{{ route('index') }}">Home</a></li>
                     <li><i class="fa fa-angle-right"></i></li>
                     <li>{{ $detail->title ?? '' }}</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
@endsection
@section('content')
   <div class="itsoft-blog-area  em-single-page-comment single-blog-details">
      <div class="container">
         <div class="row">
            <div class="col-lg-8 col-md-12  col-sm-12 col-xs-12 blog-lr">
               <div class="itsoft-single-blog-details">
                  <div class="itsoft-single-blog--thumb">
                     <img width="720" height="520" src="../wp-content/uploads/2020/09/blog3.jpg"
                        class="attachment-itsoft-blog-single size-itsoft-blog-single wp-post-image" alt=""
                        srcset="{{ asset($detail->image) }} 720w, {{ asset($detail->image) }} 300w"
                        sizes="(max-width: 720px) 100vw, 720px" />
                  </div>
                  <div class="itsoft-single-blog-details-inner">
                     <div class="appco-single-blog-title blog-page-title">
                        <h1 class="single-blog-title">{{ $detail->title ?? '' }}</h1>
                     </div>
                     <!-- BLOG POST META  -->
                     <div class="itsoft-blog-meta txp-meta">
                        <div class="itsoft-blog-meta-left">
                           <a href="../author/itsoft/index.html"> itsoft</a>
                           <span>{{ Carbon\Carbon::parse($detail->published_date)->format('F d, Y') }}</span>
                           <a class="meta_comments" href="index.html#respond">
                              0 Comments </a>
                        </div>
                     </div>
                     <div class="itsoft-single-blog-content">
                        <div class="single-blog-content">
                           {!! $detail->description !!}
                           <div class="page-list-single">
                           </div>
                        </div>
                     </div>
                     <div class="itsoft-blog-social">
                        <div class="itsoft-single-icon">
                        </div>
                     </div>
                  </div>
               </div>
               <div id="comments" class="comments-area">
                  <div id="respond" class="comment-respond">
                     <div class="commment_title">
                        <h3> Leave Comment <small><a rel="nofollow" id="cancel-comment-reply-link" href="index.html#respond"
                                 style="display:none;">Cancel reply</a></small></h3>
                     </div>
                     <form action="../../external.html?link=https://itsoft.dreamitsolution.net/wp-comments-post.php" method="post"
                        id="commentform" class="comment-form" novalidate>
                        <div class="comment_forms from-area">
                           <div class="comment_forms_inner">
                              <div class="comment_field">
                                 <div class="row">
                                    <div class="col-md-6 form-group">
                                       <input id="name" class="form-control" name="author" type="text"
                                          placeholder="Your Name" />
                                    </div>
                                    <div class="col-md-6 form-group">
                                       <input id="email" class="form-control" name="email" type="text"
                                          placeholder=" Email" />
                                    </div>
                                 </div>
                                 <p class="comment-form-cookies-consent"><input id="wp-comment-cookies-consent"
                                       name="wp-comment-cookies-consent" type="checkbox" value="yes" /> <label
                                       for="wp-comment-cookies-consent">Save my name, email, and website in this browser for the next
                                       time I comment.</label></p>
                                 <div class="row">
                                    <div class="col-md-6 form-group">
                                       <input id="phone" class="form-control" name="phone" type="text"
                                          placeholder="Phone" />
                                    </div>
                                    <div class="col-md-6 form-group">
                                       <input id="title" class="form-control" name="url" type="text"
                                          placeholder="Your Website" />
                                    </div>
                                 </div>
                              </div>
                              <div class="comment_field">
                                 <div class="form-group">
                                    <textarea name="comment" id="comment" class="form-control" cols="30" rows="6" placeholder=" Your comment..."></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <button class="wpcf7-submit button" type="submit">Post Comment</button> <input type='hidden'
                           name='comment_post_ID' value='507' id='comment_post_ID' />
                        <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                     </form>
                  </div><!-- #respond -->
               </div><!-- #comments -->
            </div>
            <div class="col-lg-4 col-md-12  col-sm-12  sidebar-right content-widget pdsr">
               <div class="blog-left-side">
                  {{-- <div id="search-2" class="widget widget_search">
                     <div class="search">
                        <form action="../../external.html?link=https://itsoft.dreamitsolution.net/" method="get">
                           <input type="text" name="s" value="" placeholder="Search Here" title="Search for:" />
                           <button type="submit" class="icons">
                              <i class="fa fa-search"></i>
                           </button>
                        </form>
                     </div>
                  </div> --}}
                  <div id="categories-3" class="widget widget_categories">
                     <h2 class="widget-title">Categories</h2>
                     <ul>
                        @if (!empty($category))
                           @foreach ($category as $item)
                              <li class="cat-item cat-item-14"><a href="../category/design/index.html">{{ $item->title ?? '' }}</a>
                              </li>
                           @endforeach
                        @endif


                     </ul>
                  </div>
                  <div id="em_recent_post_widget-3" class="widget widget_recent_data">
                     <div class="single-widget-item">
                        <h2 class="widget-title">Recent Post</h2>
                        @if (!empty($recent_post))
                           @foreach ($recent_post as $item)
                              <div class="recent-post-item">
                                 <div class="recent-post-image">
                                    <a href="{{ route('news.detail', $item->slug) }}">
                                       <img width="80" height="80" src="{{ asset($item->image) }}"
                                          class="attachment-itsoft-recent-image size-itsoft-recent-image wp-post-image"
                                          alt="" loading="lazy"
                                          srcset="{{ asset($item->image) }} 80w, {{ asset($item->image) }} 150w, {{ asset($item->image) }} 450w, {{ asset($item->image) }} 106w"
                                          sizes="(max-width: 80px) 100vw, 80px" />
                                    </a>
                                 </div>
                                 <div class="recent-post-text">
                                    <h4><a href="{{ route('news.detail', $item->slug) }}">
                                          {{ $item->title ?? '' }}
                                       </a></h4>
                                    <span
                                       class="rcomment">{{ Carbon\Carbon::parse($item->published_date)->format('F d, Y') }}</span>
                                 </div>
                              </div>
                           @endforeach
                        @endif


                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
@endsection
