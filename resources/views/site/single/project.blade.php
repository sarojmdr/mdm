@section('content')
@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', asset($sitedetail->fb_image))
@section('title', $sitedetail->title)
@push('style')
<style type="text/css">
   .banner-image {
    position: relative;
    padding: 180px 0px;
    background: url({{ $sitedetail->banner_image }}) fixed 50% 50% no-repeat;
    background-size: cover;
    }
</style>
@endpush
@section('breadcrumb')
<!-- HERO
================================================== -->
<section class="page-banner-area banner-image">
    <div class="overlay"></div>
    <!-- Content -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-12 col-12 text-center">
                <div class="page-banner-content">
                    <h1 class="display-4 font-weight-bold">{{ $page_header }}</h1>
                    <nav >
                        <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $page_header }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection

@section('content')
    <section class="section" id="process">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 text-center">
                    <div class="section-heading">
                        <!-- Heading -->
                        <h2 class="section-title">
                            {{ $sitedetail->working_process_heading ?? '' }}
                        </h2>

                        <!-- Subheading -->
                        <p>
                            {{ $sitedetail->working_process_text ?? '' }}
                        </p>

                    </div>
                </div>
            </div> <!-- / .row -->

            <div class="row justify-content-center">
                @if (!empty($working_process))
                    @foreach ($working_process as $item)
                        <div class="col-lg-4 col-sm-6 col-md-6">
                            <div class="process-block">
                                <img src="{{ asset($item->image) }}" alt="{{ $item->title }}" class="img-fluid">

                                <h3>{{ $item->title }}</h3>
                                <p>{{ strip_tags(str_limit($item->description,200)) }}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <section id="work-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-heading">
                        <h1>{{ $sitedetail->project_left_text ?? '' }}</h1>
                    </div>
                </div>
                <div class="col-lg-6">
                    <p class="lead">{{ $sitedetail->project_right_text ?? '' }}</p>
                </div>
            </div>
        </div>
    </section>
    <section id="work" class="section-bottom">
        <div class="container">
            <div class="row">
                @if (!empty($projects))
                    @foreach ($projects as $key => $item)
                        @if($loop->count >= 2)
                            <div class="col-lg-4 col-md-6 p-0">
                                <div class="work-block">
                                    <img src="{{ asset($item->image) }}" alt="{{ $item->title ?? '' }}" class="img-fluid">
                                    <div class="overlay-content-block">
                                        <h4>{{ $item->title ?? '' }}</h4>
                                        <p>{{ $item->services }}</p>
                                        <a href="{{ route("project.detail",$item->slug) }}"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif


                {{-- <div class="col-lg-3 col-md-6 p-0">
                    <div class="work-block">
                        <img src="images/work/15.jpg" alt="work-img" class="img-fluid">
                        <div class="overlay-content-block">
                            <h4>Probiz portfolio template</h4>
                            <p>Web Development</p>
                            <a href="single-project.html"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 p-0">
                    <div class="work-block">
                        <img src="images/work/14.jpg" alt="work-img" class="img-fluid">
                        <div class="overlay-content-block">
                            <h4>Probiz portfolio template</h4>
                            <p>Web Development</p>
                            <a href="single-project.html"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 p-0">
                    <div class="work-block">
                        <img src="images/work/11.jpg" alt="work-img" class="img-fluid">
                        <div class="overlay-content-block">
                            <h4>Probiz portfolio template</h4>
                            <p>Web Development</p>
                            <a href="single-project.html"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 p-0">
                        <div class="work-block">
                            <img src="images/work/10.jpg" alt="work-img" class="img-fluid">
                            <div class="overlay-content-block">
                                <h4>Probiz portfolio template</h4>
                                <p>Web Development</p>
                                <a href="single-project.html"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div> --}}
            </div>
        </div>
    </section>
@endsection
