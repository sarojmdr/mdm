@extends('site.master')
@section('description', ($detail->meta_description != '' ? $detail->meta_description : $sitedetail->meta_descriptions))
@section('keywords', ($detail->meta_keywords != '' ? $detail->meta_keywords : $sitedetail->meta_keywords))
@section('fb_image', ($detail->image != '' ? asset($detail->image) : asset($sitedetail->fb_image)))
@section('title', $detail->title)

@section('breadcrumb')
<!-- HERO
================================================== -->
<section class="page-banner-area banner-image"  style=" background: url({{ asset($sitedetail->banner_image) }}) fixed 50% 50% no-repeat;">
    <div class="overlay"></div>
    <!-- Content -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-12 col-12 text-center">
                <div class="page-banner-content">
                    <h1 class="display-4 font-weight-bold">{{ $page_header }}</h1>
                    <nav >
                        <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $page_header }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection

@section('content')
	<section class="inner-page">
      <div class="container">
       <h1>{{ $detail->title }}</h1>
        @if($detail->image != '')
          <figure>
            <img src="{{ asset($detail->image) }}" class="w-100" alt="{{ $detail->title }}">
          </figure>
        @endif
        <p>
          {!! $detail->description !!}
        </p>
      </div>
    </section>
@endsection
