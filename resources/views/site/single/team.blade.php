@extends('site.master')
@section('description',  $sitedetail->meta_descriptions)
@section('keywords',  $sitedetail->meta_keywords)
@section('fb_image',  asset($sitedetail->fb_image))
@section('title', $page_header)



@section('breadcrumb')
 <!-- ======= Breadcrumbs ======= -->
    <div id="banner-area" class="banner-area" style="background-image:url({{ asset($sitedetail->banner_image) }})">
      <div class="banner-text">
        <div class="container">
            <div class="row">
              <div class="col-lg-12">
                  <div class="banner-heading">
                    <h1 class="banner-title">{{ $page_header }}</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                          <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                          {{-- <li class="breadcrumb-item"><a href="#">Services</a></li> --}}
                          <li class="breadcrumb-item active" aria-current="page">{{ $page_header }}</li>
                        </ol>
                    </nav>
                  </div>
              </div><!-- Col end -->
            </div><!-- Row end -->
        </div><!-- Container end -->
      </div><!-- Banner text end -->
    </div><!-- Banner area end --> 
@endsection

@section('content')
  <section id="main-container" class="main-container pb-4">
    <div class="container">
      <div class="row text-center">
        <div class="col-lg-12">
          <h3 class="section-sub-title">{{ $sitedetail->team_heading }}</h3>
        </div>
      </div>
      <!--/ Title row end -->

      <div class="row justify-content-center">
        @if(count($team_list) > 0)
          @foreach($team_list as $item)
            <div class="col-lg-3 col-sm-6 mb-5">
              <div class="ts-team-wrapper">
                <div class="team-img-wrapper">
                  <img loading="lazy" src="{{ asset($item->image) }}" class="img-fluid" alt="{{ $item->name }}">
                </div>
                <div class="ts-team-content-classic">
                  <h3 class="ts-name">{{ $item->name }}</h3>
                  <p class="ts-designation">{{ $item->designation }}</p>
                  <p class="ts-description">{!! $item->info !!}</p>
                  <div class="team-social-icons">
                    @if($item->facebook_link != '')
                      <a target="_blank" href="{{ $item->facebook_link }}">
                        <i class="fab fa-facebook-f"></i>
                      </a>
                    @endif
                    @if($item->twitter_link != '')
                      <a target="_blank" href="{{ $item->twitter_link }}"><i class="fab fa-twitter"></i></a>
                    @endif
                    @if($item->linkedin_link != '')
                      <a target="_blank" href="{{ $item->linkedin_link }}"><i class="fab fa-linkedin"></i></a>
                    @endif
                    @if($item->google_plus_link != '')
                      <a target="_blank" href="{{ $item->google_plus_link }}"><i class="fab fa-google-plus"></i></a>
                    @endif
                  </div>
                  <!--/ social-icons-->
                </div>
              </div>
              <!--/ Team wrapper 1 end -->

            </div><!-- Col end -->
          @endforeach
         @endif


      </div><!-- Content row 1 end -->

      {{-- <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6 mb-5">
          <div class="ts-team-wrapper">
            <div class="team-img-wrapper">
              <img loading="lazy" src="images/team/team3.jpg" class="img-fluid" alt="team-img">
            </div>
            <div class="ts-team-content-classic">
              <h3 class="ts-name">Mark Conter</h3>
              <p class="ts-designation">Safety Officer</p>
              <p class="ts-description">Nats Stenman began his career in construction with boots on the ground</p>
              <div class="team-social-icons">
                <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                <a target="_blank" href="#"><i class="fab fa-google-plus"></i></a>
                <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
              </div>
              <!--/ social-icons-->
            </div>
          </div>
          <!--/ Team wrapper 3 end -->
        </div><!-- Col end -->

        <div class="col-lg-3 col-md-4 col-sm-6 mb-5">
          <div class="ts-team-wrapper">
            <div class="team-img-wrapper">
              <img loading="lazy" src="images/team/team4.jpg" class="img-fluid" alt="team-img">
            </div>
            <div class="ts-team-content-classic">
              <h3 class="ts-name">AYESHA STEWART</h3>
              <p class="ts-designation">Finance Officer</p>
              <p class="ts-description">Nats Stenman began his career in construction with boots on the ground</p>
              <div class="team-social-icons">
                <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
              </div>
              <!--/ social-icons-->
            </div>
          </div>
          <!--/ Team wrapper 4 end -->

        </div><!-- Col end -->

        <div class="col-lg-3 col-md-4 col-sm-6 mb-5">
          <div class="ts-team-wrapper">
            <div class="team-img-wrapper">
              <img loading="lazy" src="images/team/team5.jpg" class="img-fluid" alt="team-img">
            </div>
            <div class="ts-team-content-classic">
              <h3 class="ts-name">Dave Clarkte</h3>
              <p class="ts-designation">Civil Engineer</p>
              <p class="ts-description">Nats Stenman began his career in construction with boots on the ground</p>
              <div class="team-social-icons">
                <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                <a target="_blank" href="#"><i class="fab fa-google-plus"></i></a>
                <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
              </div>
              <!--/ social-icons-->
            </div>
          </div>
          <!--/ Team wrapper 5 end -->
        </div><!-- Col end -->

        <div class="col-lg-3 col-md-4 col-sm-6 mb-5">
          <div class="ts-team-wrapper">
            <div class="team-img-wrapper">
              <img loading="lazy" src="images/team/team6.jpg" class="img-fluid" alt="team-img">
            </div>
            <div class="ts-team-content-classic">
              <h3 class="ts-name">Elton Joe</h3>
              <p class="ts-designation">Site Supervisor</p>
              <p class="ts-description">Nats Stenman began his career in construction with boots on the ground</p>
              <div class="team-social-icons">
                <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                <a target="_blank" href="#"><i class="fab fa-google-plus"></i></a>
                <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
              </div>
              <!--/ social-icons-->
            </div>
          </div>
          <!--/ Team wrapper 6 end -->
        </div><!-- Col end -->
      </div><!-- Content row end --> --}}

    </div><!-- Container end -->
  </section><!-- Main container end -->
  
@endsection