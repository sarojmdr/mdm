@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', asset($sitedetail->fb_image))
@section('title', $sitedetail->title)

@section('breadcrumb')
 <!-- ======= Breadcrumbs ======= -->
    <div id="banner-area" class="banner-area" style="background-image:url({{ asset($sitedetail->banner_image) }})">
      <div class="banner-text">
        <div class="container">
            <div class="row">
              <div class="col-lg-12">
                  <div class="banner-heading">
                    <h1 class="banner-title">Blogs</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                          <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                          {{-- <li class="breadcrumb-item"><a href="#">Services</a></li> --}}
                          <li class="breadcrumb-item active" aria-current="page">All Blogs</li>
                        </ol>
                    </nav>
                  </div>
              </div><!-- Col end -->
            </div><!-- Row end -->
        </div><!-- Container end -->
      </div><!-- Banner text end -->
    </div><!-- Banner area end -->
@endsection

@section('content')
  <section id="main-container" class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mb-5 mb-lg-0">
          @if(count($blog_list) > 0)
            @foreach($blog_list as $item)
              <div class="post">
                <div class="post-media post-image">
                  <img loading="lazy" src="{{ asset($item->image) }}" class="img-fluid" alt="{{ $item->title }}">
                </div>

                <div class="post-body">

                  <div class="entry-header">
                    <div class="post-meta">
                      <span class="post-author">
                        <i class="far fa-user"></i>
                        {{-- <a href="#"> Admin</a> --}}
                        {{ $item->authordata->title }}
                      </span>
                      @if(count($item->category) > 0)
                      @php
                        $category_count = count($item->category);
                      @endphp
                        <span class="post-cat">
                          <i class="far fa-folder-open"></i>
                            @foreach($item->category as $key => $sub_item)
                              <a href="{{ route('news.category',$sub_item->slug) }}">
                                {{ $sub_item->title }}
                              </a>
                              @if($category_count -1 != $key)
                                ,
                              @endif

                            @endforeach
                        </span>
                      @endif
                      <span class="post-meta-date"><i class="far fa-calendar"></i> {{$item->published_date }}</span>
                    </div>
                    <h2 class="entry-title">
                      <a href="{{ route('news.detail', $item->slug) }}">{{ $item->title }}</a>
                    </h2>
                  </div><!-- header end -->

                  <div class="entry-content">
                    <p>{{ $item->short_description }}</p>
                  </div>

                  <div class="post-footer">
                    <a href="{{ route('news.detail', $item->slug) }}" class="btn btn-primary">Continue Reading</a>
                  </div>
                </div><!-- post-body end -->
              </div><!-- 1st post end -->
            @endforeach
          @endif


          <nav class="paging" aria-label="Page navigation example">
            <ul class="pagination">
              {{ $blog_list->links() }}
              {{-- <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-double-left"></i></a></li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-double-right"></i></a></li> --}}
            </ul>
          </nav>
        </div><!-- Content Col end -->

        <div class="col-lg-4">

          <div class="sidebar sidebar-right">
            <div class="widget recent-posts">
              <h3 class="widget-title">Recent Posts</h3>
              <ul class="list-unstyled">
                @if(count($recent_post) > 0)
                  @foreach($recent_post as $item)
                    <li class="d-flex align-items-center">
                      <div class="posts-thumb">
                        <a href="{{ route('news.detail',$item->slug) }}"><img loading="lazy" alt="{{ $item->title }}" src="{{ asset($item->image) }}"></a>
                      </div>
                      <div class="post-info">
                        <h4 class="entry-title">
                          <a href="{{ route('news.detail',$item->slug) }}">{{ str_limit($item->short_description,100) }}</a>
                        </h4>
                      </div>
                    </li><!-- 1st post end-->
                  @endforeach
                @endif


              </ul>

            </div><!-- Recent post end -->

            <div class="widget">
              <h3 class="widget-title">Categories</h3>
              <ul class="arrow nav nav-tabs">
                @if(count($category) > 0)
                  @foreach($category as $item)
                    <li><a href="{{ route('news.category', $item->slug) }}">{{ $item->title }}</a></li>
                  @endforeach
                @endif
              </ul>
            </div><!-- Categories end -->






          </div><!-- Sidebar end -->
        </div><!-- Sidebar Col end -->

      </div><!-- Main row end -->

    </div><!-- Container end -->
  </section><!-- Main container end -->
@endsection
