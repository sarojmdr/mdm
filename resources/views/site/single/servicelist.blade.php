@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', asset($sitedetail->fb_image))
@section('title', $sitedetail->title)

@section('breadcrumb')
 <!-- ======= Breadcrumbs ======= -->
    <div id="banner-area" class="banner-area" style="background-image:url({{ asset($sitedetail->banner_image) }})">
      <div class="banner-text">
        <div class="container">
            <div class="row">
              <div class="col-lg-12">
                  <div class="banner-heading">
                    <h1 class="banner-title">Service</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                          <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                          {{-- <li class="breadcrumb-item">Services</li> --}}
                          {{-- <li class="breadcrumb-item"><a href="#">Services</a></li> --}}
                          <li class="breadcrumb-item active" aria-current="page">All Services</li>
                        </ol>
                    </nav>
                  </div>
              </div><!-- Col end -->
            </div><!-- Row end -->
        </div><!-- Container end -->
      </div><!-- Banner text end -->
    </div><!-- Banner area end -->
@endsection

@section('content')
	<section id="main-container" class="main-container pb-2">
  <div class="container">
    <div class="row">
      @if(count($service_list) > 0)
        @foreach($service_list as $item)
          <div class="col-lg-4 col-md-6 mb-5">
            <div class="ts-service-box">
                <div class="ts-service-image-wrapper">
                  <img loading="lazy" class="w-100" src="{{ asset($item->image) }}" alt="{{ $item->title }}" height="180px">
                </div>
                <div class="d-flex">
                  {{-- <div class="ts-service-box-img">
                      <img loading="lazy" src="{{ asset($item->icon) }}" alt="{{ $item->title }}" height="60px">
                  </div> --}}
                  <div class="ts-service-info">
                      <h3 class="service-box-title"><a href="{{ route('service.detail', $item->slug) }}">{{ $item->title }}</a></h3>
                      <p>{{$item->short_description}}</p>
                      <a class="learn-more d-inline-block" href="{{ route('service.detail', $item->slug) }}" aria-label="service-details"><i class="fa fa-caret-right"></i> Learn more</a>
                  </div>
                </div>
            </div><!-- Service1 end -->
          </div><!-- Col 1 end -->
        @endforeach
      @endif



    </div><!-- Main row end -->
    {{ $service_list->links() }}
  </div><!-- Conatiner end -->
</section><!-- Main container end -->
@endsection
