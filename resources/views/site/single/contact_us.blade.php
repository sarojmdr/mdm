@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', asset($sitedetail->fb_image))
@section('title', $page_header)
@section('breadcrumb')
<!-- HERO
================================================== -->
<section class="page-banner-area banner-image" style=" background: url({{ $sitedetail->banner_image }}) fixed 50% 50% no-repeat;">
    <div class="overlay"></div>
    <!-- Content -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-12 col-12 text-center">
                <div class="page-banner-content">
                    <h1 class="display-4 font-weight-bold">{{ $page_header }}</h1>
                    <nav >
                        <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $page_header }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection

@section('content')
    <section id="contact-info">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="contact-info-block text-center">
                        <i class="pe-7s-map-marker"></i>
                        <h4>Address</h4>
                        <p class="lead">{{ $sitedetail->address ?? '' }}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="contact-info-block text-center">
                        <i class="pe-7s-mail"></i>
                        <h4>Email</h4>
                        <p class="lead">{{ $sitedetail->email ?? '' }}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="contact-info-block text-center">
                        <i class="pe-7s-phone"></i>
                        <h4>Phone Number</h4>
                        <p class="lead">{{ $sitedetail->mobile_no ?? '' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section" id="contact">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-8 col-lg-6">
                    <h5>Leave a Message</h5>
                    <!-- Heading -->
                    <h2 class="section-title mb-2 ">
                        {{ $sitedetail->contact_us_heading ?? '' }}
                    </h2>

                    <!-- Subheading -->
                    <p class="mb-5 ">
                        {{ $sitedetail->contact_us_txt ?? '' }}
                    </p>

                </div>
            </div> <!-- / .row -->

            <div class="row">
                <div class="col-lg-6">
                   <!-- form message -->
                    <div class="row">
                        <div class="col-12">
                            @if (Session::has('success_message'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ Session::get('success_message') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- end message -->
                    <!-- Contacts Form -->
                    <form class="contact_form" action="{{ route('post.contact_info') }}" method="post" role="form">
                        @csrf
                        <div class="row">
                            <!-- Input -->
                            <div class="col-sm-6 mb-6">
                                <div class="form-group">
                                    <label class="h6 small d-block text-uppercase">
                                        Your name
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="input-group">
                                        <input class="form-control" name="name" id="name" placeholder="Your Name" type="text">
                                    </div>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- End Input -->

                            <!-- Input -->
                            <div class="col-sm-6 mb-6">
                                <div class="form-group">
                                    <label class="h6 small d-block text-uppercase">
                                        Your email address
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="input-group ">
                                        <input class="form-control" name="email" id="email" placeholder="Email" type="email">
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!-- End Input -->

                            <div class="w-100"></div>

                            <!-- Input -->
                            <div class="col-sm-6 mb-6">
                                <div class="form-group">
                                    <label class="h6 small d-block text-uppercase">
                                        Subject
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="input-group">
                                        <input class="form-control" name="subject" id="subject" placeholder="Subject" type="text">
                                    </div>
                                    @if ($errors->has('subject'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('subject') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!-- End Input -->

                            <!-- Input -->
                            <div class="col-sm-6 mb-6">
                                <div class="form-group">
                                    <label class="h6 small d-block text-uppercase">
                                        Your Phone Number
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="input-group ">
                                        <input class="form-control" id="phone" name="phoneno" placeholder="Phone Number" type="text">
                                    </div>
                                    @if ($errors->has('phoneno'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('phoneno') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!-- End Input -->
                        </div>

                        <!-- Input -->
                        <div class="form-group mb-5">
                            <label class="h6 small d-block text-uppercase">
                                Message
                                <span class="text-danger">*</span>
                            </label>

                            <div class="input-group">
                                <textarea class="form-control" rows="4" name="message" id="message" placeholder="Your Message"></textarea>
                            </div>
                            @if ($errors->has('message'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- End Input -->

                        <div class="">
                            <input name="submit" type="submit" class="btn btn-primary btn-circled" value="Send Message">
                            {{-- <p class="small pt-3">We'll get back to you in 1-2 business days.</p> --}}
                        </div>
                    </form>
                    <!-- End Contacts Form -->
                </div>

                <div class="col-lg-6 col-md-6">
                    <!-- START MAP -->
                <div id="map" >
                    <div class="google-map">
                        {{--  <div id="map" class="map" data-latitude="40.712776" data-longitude="-74.005974" data-marker="images/marker.png" data-marker-name="Constra"></div> --}}
                        <div class="mapouter map">
                            <div class="gmap_canvas">
                            <iframe width="750" height="400" id="gmap_canvas" src="{{ $sitedetail->map_detail }}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            <br>
                            <style>.mapouter{position:relative;text-align:right;height:400px;width:800px;}</style>
                            <style>.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:800px;}</style>
                            </div>
                        </div>
                    </div>
                <!-- END MAP -->
                </div>
            </div>
        </div>
    </section>
@endsection
