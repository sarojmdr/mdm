<!DOCTYPE html>
<html lang="en">
@include('site.layouts.meta')

<body data-rsssl=1
   class="page-template page-template-page-templates page-template-template-default page-template-page-templatestemplate-default-php page page-id-6194 ehf-template-itsoft ehf-stylesheet-itsoft mega-menu-menu-1 elementor-default elementor-kit-8 elementor-page elementor-page-6194">
   <div class="wrapper">
      @include('site.layouts.header')
      @section('breadcrumb')
      @show
      <div class="template-home-wrapper">
         @section('content')
         @show
      </div>
      @include('site.layouts.footer')
      @include('site.layouts.script')
      @stack('script')
   </div>
</body>

</html>
