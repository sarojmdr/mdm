<div class="em40_header_area_main">

   <!-- HEADER TOP AREA -->


   <div class="itsoft-header-top">
      <div class="container">

         <!-- STYLE 1 LEFT ADDRESS RIGHT ICON  -->

         <div class="row">
            <!-- TOP LEFT -->
            <div class="col-xs-12 col-md-8 col-sm-8">
               <div class="top-address">
                  <p>
                     <a href="mailto:{{ $sitedetail->email }}"><i class="fa fa-envelope-o"></i>{{ $sitedetail->email }}</a>

                     <span><i class="fa fa-street-view"></i>{{ $sitedetail->address ?? '' }}</span>

                     <a href="tel:{{ $sitedetail->mobile_no ?? '' }}"><i
                           class="fa fa-phone"></i>{{ $sitedetail->mobile_no ?? '' }}</a>

                  </p>
               </div>
            </div>
            <!-- TOP RIGHT -->
            <div class="col-xs-12 col-md-4 col-sm-4">
               <div class="top-right-menu">
                  <ul class="social-icons text-right">
                     <li><a class="facebook social-icon" href="{{ $sitedetail->facebook }}" target="__blank" title="Facebook"><i
                              class="fa fa-facebook"></i></a></li>
                     <li><a class="twitter social-icon" href="{{ $sitedetail->linkedin ?? '' }}" title="Twitter"
                           target="__blank"><i class="fa fa-linkedin"></i></a>
                     </li>
                     <li><a class="instagram social-icon" href="{{ $sitedetail->instagram ?? '' }}" title="Instagram"
                           target="__blank"><i class="fa fa-instagram"></i></a></li>
                     {{-- <li><a class="dribbble social-icon" href="#" title="Dribbble" "><i class="fa fa-dribbble"></i></a>
                     </li> --}}
                  </ul>
               </div>
            </div>
         </div>
         <!-- STYLE 2 lEFT ICON RIGHT MENU  -->

      </div>
   </div>



   <div class="mobile_logo_area d-sm-block d-md-block d-lg-none">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="mobile_menu_logo text-center">
                  <a href="{{ route('index') }}" title="{{ $sitedetail->title }}">
                     <img src="{{ asset($sitedetail->logo) }}" alt="{{ $sitedetail->title }}" />
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- START HEADER MAIN MENU AREA -->

   <!-- HEADER TRANSPARENT MENU -->
   <!-- HEADER MANU AREA -->
   <div class="itsoft-main-menu one_page d-md-none d-lg-block d-sm-none d-none">
      <div class="itsoft_nav_area scroll_fixed">
         <div class="container">
            <div class="row logo-left align-items-center">
               <div class="col-md-3 col-sm-3 col-xs-4">


                  <div class="logo">
                     <a class="main_sticky_main_l" href="{{ route('index') }}" title="{{ $sitedetail->title }}">
                        <img src="{{ asset($sitedetail->logo) }}" alt="{{ $sitedetail->title }}" />
                     </a>
                     <a class="main_sticky_l" href="{{ route('index') }}" title="{{ $sitedetail->title }}">
                        <img src="{{ asset($sitedetail->logo) }}" alt="{{ $sitedetail->title }}" />
                     </a>
                  </div>

               </div>
               <div class="col-md-9 col-sm-9 col-xs-8">
                  <nav class="itsoft_menu">
                     <ul id="menu-one-page-menu" class="sub-menu nav_scroll">
                        <li id="menu-item-6199" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6199"><a
                              href="#home">Home</a></li>
                        <li id="menu-item-6198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6198"><a
                              href="#about">About</a></li>
                        <li id="menu-item-6200" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6200"><a
                              href="#service">Services</a></li>
                        <li id="menu-item-6201" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6201"><a
                              href="#portfolio">Portfolio</a></li>
                        <li id="menu-item-6202" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6202"><a
                              href="#team">Team</a></li>
                        <li id="menu-item-6203" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6203"><a
                              href="#blog">Blog</a></li>
                        <li id="menu-item-6204" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6204"><a
                              href="#contact">Contact</a></li>
                     </ul>
                  </nav>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- HEADER ONEPAGE TRANSPARENT MENU -->

   <!-- MOBILE MENU AREA -->
   <div class="home-2 mbm d-sm-block d-md-block d-lg-none header_area main-menu-area">
      <div class="menu_area mobile-menu trp_nav_area">
         <nav>
            <ul id="menu-main-menu" class="main-menu clearfix">
               <li id="menu-item-6119"
                  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-6119">
                  <a href="../index.html">Home</a>
                  <ul class="sub-menu">
                     <li id="menu-item-9006" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9006">
                  </ul>
               </li>
               <li id="menu-item-12"
                  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-12"><a
                     href="#">Company</a>
                  <ul class="sub-menu">
                     <li id="menu-item-1476" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1476"><a
                           href="../about-us/index.html">About Us</a></li>
                     <li id="menu-item-1477" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1477"><a
                           href="../contact-us/index.html">Contact Us</a></li>
                  </ul>
               </li>
               <li id="menu-item-1821" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1821"><a
                     href="../our-services/index.html">Services</a></li>
               <li id="menu-item-1537"
                  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1537"><a
                     href="../portfolio/index.html">Portfolio</a>
                  <ul class="sub-menu">
                     <li id="menu-item-1535" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1535"><a
                           href="../portfolio-2column/index.html">Portfolio-2Column</a></li>
                  </ul>
               </li>
               <li id="menu-item-1507"
                  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1507"><a
                     href="../blog-list/index.html">Blogs</a>
                  <ul class="sub-menu">
                     <li id="menu-item-1506" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1506"><a
                           href="../blog-list/index.html">Blog List</a></li>
                  </ul>
               </li>
               <li id="menu-item-1516" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1516"><a
                     href="../contact-us/index.html">Contact</a></li>
            </ul>
         </nav>
      </div>
   </div>
   <!-- END MOBILE MENU AREA  -->
</div>
