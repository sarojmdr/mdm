<script type="text/javascript">
   window.RS_MODULES = window.RS_MODULES || {};
   window.RS_MODULES.modules = window.RS_MODULES.modules || {};
   window.RS_MODULES.waiting = window.RS_MODULES.waiting || [];
   window.RS_MODULES.defered = true;
   window.RS_MODULES.moduleWaiting = window.RS_MODULES.moduleWaiting || {};
   window.RS_MODULES.type = 'compiled';
</script>
<div class="preloader-plus">
   <div class="preloader-content"> <img class="preloader-custom-img" src="{{ asset('site/uploads/2022/07/prealoader-img.gif') }}" />
   </div>
</div>
<script>
   (function() {
      function maybePrefixUrlField() {
         if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
            this.value = "http://" + this.value;
         }
      }

      var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
      if (urlFields) {
         for (var j = 0; j < urlFields.length; j++) {
            urlFields[j].addEventListener('blur', maybePrefixUrlField);
         }
      }
   })();
</script>
<link rel='stylesheet' id='e-animations-css'
   href='{{ asset('site/plugins/elementor/assets/lib/animations/animations.minf3df.css') }}' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css' href='{{ asset('site/plugins/revslider/public/assets/css/rs6b134.css') }}'
   type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
   #rs-demo-id {}
</style>
<script type='text/javascript' src='{{ asset('site/plugins/contact-form-7/includes/swv/js/index42f0.js') }}' id='swv-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
   /* <![CDATA[ */
   var wpcf7 = {
      "api": {
         "root": "https:\/\/itsoft.dreamitsolution.net\/wp-json\/",
         "namespace": "contact-form-7\/v1"
      }
   };
   /* ]]> */
</script>
<script type='text/javascript' src='{{ asset('site/plugins/contact-form-7/includes/js/index42f0.js') }}' id='contact-form-7-js'>
</script>
<script type='text/javascript' src='{{ asset('site/plugins/revslider/public/assets/js/rbtools.minb134.js') }}' defer async
   id='tp-tools-js'></script>
<script type='text/javascript' src='{{ asset('site/plugins/revslider/public/assets/js/rs6.minb134.js') }}' defer async
   id='revmin-js'></script>
<script type='text/javascript' src='{{ asset('site/widgets/js/jquery.magnific-popup.min4963.js') }}' id='script-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/modernizr.custom.79639e1e3.js') }}' id='modernizrs-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/jquery.directional-hover.mine1e3.js') }}' id='mouse-directions-js'>
</script>
<script type='text/javascript' src='{{ asset('site/assets/js/vendor/modernizr-2.8.3.minf7ff.js') }}' id='modernizr-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/bootstrap.min46df.js') }}' id='bootstrap-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/imagesloaded.mineda1.js') }}' id='imagesloaded-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/jquery.meanmenu8a54.js') }}' id='meanmenu-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/isotope.pkgd.min8a54.js') }}' id='isotope-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/owl.carousel.min5b21.js') }}' id='owl-carousel-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/jquery.scrollUpe1e3.js') }}' id='scrollup-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/jquery.nivo.slider.packe1e3.js') }}' id='nivo-slider-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/headroom.mine1e3.js') }}' id='headroom-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/parallax.mine1e3.js') }}' id='paralax-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/jquery.counterup.mine1e3.js') }}' id='jquery-counterup-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/slick.mine1e3.js') }}' id='slick-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/jquery.nave1e3.js') }}' id='jquery-nav-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/headlinee1e3.js') }}' id='animate-text-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/wowe1e3.js') }}' id='wow-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/jquery-scrolltofixed-mine1e3.js') }}' id='jquery-scrolltofixed-js'>
</script>
<script type='text/javascript' src='{{ asset('site/venobox/venobox.mine1e3.js') }}' id='venobox-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/waypoints.mine1e3.js') }}' id='waypoints-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/navigation4a7d.js') }}' id='itsoft-navigation-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/skip-link-focus-fix4a7d.js') }}' id='itsoft-skip-link-focus-fix-js'>
</script>
<script type='text/javascript' src='{{ asset('site/assets/js/themee1e3.js') }}' id='itsoft-theme-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/hoverIntent.min3e5a.js') }}' id='hoverIntent-js'></script>
<script type='text/javascript' id='megamenu-js-extra'>
   /* <![CDATA[ */
   var megamenu = {
      "timeout": "300",
      "interval": "100"
   };
   /* ]]> */
</script>
<script type='text/javascript' src='{{ asset('site/plugins/megamenu/js/maxmegamenu4315.js') }}' id='megamenu-js'></script>
<script type='text/javascript' defer src='{{ asset('site/plugins/mailchimp-for-wp/assets/js/forms9537.js') }}'
   id='mc4wp-forms-api-js'></script>
<script type='text/javascript' src='{{ asset('site/plugins/elementor/assets/js/webpack.runtime.minf3df.js') }}'
   id='elementor-webpack-runtime-js'></script>
<script type='text/javascript' src='{{ asset('site/plugins/elementor/assets/js/frontend-modules.minf3df.js') }}'
   id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='{{ asset('site/plugins/elementor/assets/lib/waypoints/waypoints.min05da.js') }}'
   id='elementor-waypoints-js'></script>
<script type='text/javascript' src='{{ asset('site/assets/js/jquery/ui/core.min0028.js') }}' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='{{ asset('site/plugins/elementor/assets/lib/swiper/swiper.min48f5.js') }}' id='swiper-js'>
</script>
<script type='text/javascript' src='{{ asset('site/plugins/elementor/assets/lib/share-link/share-link.minf3df.js') }}'
   id='share-link-js'></script>
<script type='text/javascript' src='{{ asset('site/plugins/elementor/assets/lib/dialog/dialog.mind227.js') }}'
   id='elementor-dialog-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
   var elementorFrontendConfig = {
      "environmentMode": {
         "edit": false,
         "wpPreview": false,
         "isScriptDebug": false
      },
      "i18n": {
         "shareOnFacebook": "Share on Facebook",
         "shareOnTwitter": "Share on Twitter",
         "pinIt": "Pin it",
         "download": "Download",
         "downloadImage": "Download image",
         "fullscreen": "Fullscreen",
         "zoom": "Zoom",
         "share": "Share",
         "playVideo": "Play Video",
         "previous": "Previous",
         "next": "Next",
         "close": "Close"
      },
      "is_rtl": false,
      "breakpoints": {
         "xs": 0,
         "sm": 480,
         "md": 768,
         "lg": 1025,
         "xl": 1440,
         "xxl": 1600
      },
      "responsive": {
         "breakpoints": {
            "mobile": {
               "label": "Mobile",
               "value": 767,
               "default_value": 767,
               "direction": "max",
               "is_enabled": true
            },
            "mobile_extra": {
               "label": "Mobile Extra",
               "value": 880,
               "default_value": 880,
               "direction": "max",
               "is_enabled": false
            },
            "tablet": {
               "label": "Tablet",
               "value": 1024,
               "default_value": 1024,
               "direction": "max",
               "is_enabled": true
            },
            "tablet_extra": {
               "label": "Tablet Extra",
               "value": 1200,
               "default_value": 1200,
               "direction": "max",
               "is_enabled": false
            },
            "laptop": {
               "label": "Laptop",
               "value": 1366,
               "default_value": 1366,
               "direction": "max",
               "is_enabled": false
            },
            "widescreen": {
               "label": "Widescreen",
               "value": 2400,
               "default_value": 2400,
               "direction": "min",
               "is_enabled": false
            }
         }
      },
      "version": "3.7.2",
      "is_static": false,
      "experimentalFeatures": {
         "e_import_export": true,
         "e_hidden_wordpress_widgets": true,
         "landing-pages": true,
         "elements-color-picker": true,
         "favorite-widgets": true,
         "admin-top-bar": true
      },
      "urls": {
         "assets": "https:\/\/itsoft.dreamitsolution.net\/wp-content\/plugins\/elementor\/assets\/"
      },
      "settings": {
         "page": [],
         "editorPreferences": []
      },
      "kit": {
         "active_breakpoints": ["viewport_mobile", "viewport_tablet"],
         "global_image_lightbox": "yes",
         "lightbox_enable_counter": "yes",
         "lightbox_enable_fullscreen": "yes",
         "lightbox_enable_zoom": "yes",
         "lightbox_enable_share": "yes",
         "lightbox_title_src": "title",
         "lightbox_description_src": "description"
      },
      "post": {
         "id": 6194,
         "title": "Landing%20Page%2001%20%E2%80%93%20IT-Soft",
         "excerpt": "",
         "featuredImage": false
      }
   };
</script>
<script type='text/javascript' src='{{ asset('site/plugins/elementor/assets/js/frontend.minf3df.js') }}'
   id='elementor-frontend-js'></script>
<script type='text/javascript' src='{{ asset('site/plugins/elementor/assets/js/preloaded-modules.minf3df.js') }}'
   id='preloaded-modules-js'></script>
@stack('script')
