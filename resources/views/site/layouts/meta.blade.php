<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <title>@yield('title') || {{ $sitedetail->title }} ||</title>

   <meta name="theme-color" content="#ffffff">
   <meta name="keywords" content="@yield('keywords')" />
   <meta name="description" content="@yield('description')" />
   <link rel="canonical" href="{{ Request::url() }}" />
   <meta name="Classification" content="website">

   <meta property="fb:app_id" content="329023262190838" />
   <meta property="og:locale" content="en_US" />
   <meta property="og:type" content="article" />
   <meta property="og:title" content="|| @yield('title') || {{ $sitedetail->title }}" />
   <meta property="og:description" content="@yield('description')" />
   <meta property="og:url" content="{{ Request::url() }}" />
   <meta property="og:site_name" content="{{ Request::url() }}" />
   <meta property="og:image" content="@yield('fb_image')" />

   <meta property="article:section" content="Featured" />
   <meta property="article:published_time" content="{{ date('Y-m-d H:i:s') }}" />

   <meta name="twitter:card" content="summary" />
   <meta name="twitter:description" content="@yield('description')" />
   <meta name="twitter:title" content="|| @yield('title') || {{ $sitedetail->title }}" />
   <meta name="twitter:site" content="{{ $sitedetail->twitter }}" />
   <meta name="twitter:domain" content="{{ Request::url() }}" />
   <meta name="twitter:image" content="@yield('fb_image')" />
   <meta name="twitter:creator" content="{{ $sitedetail->twitter }}" />

   {{-- <meta name="Language" content="English"> --}}
   <meta http-equiv="cache-control" content="private">
   <meta name="Copyright" content="Miracle Digital Media Pvt. Ltd">
   <meta name="Publisher" content="Miracle Digital Media Pvt. Ltd.">

   <meta name="coverage" content="Worldwide">
   <meta name="distribution" content="Global">
   <meta name="rating" content="General">
   <meta name="revisit-after" content="7 days">
   <meta http-equiv="Expires" content="0">
   <meta http-equiv="Pragma" content="no-cache">
   <meta http-equiv="Cache-Control" content="no-cache">

   <meta name="csrf-token" content="{{ csrf_token() }}">

   <!-- Favicon
================================================== -->
   <link rel="icon" type="image/png"
      href="{{ $sitedetail->fav_icon != '' ? asset($sitedetail->fav_icon) : asset($sitedetail->logo) }}" sizes="any" />

   </style>
   <link rel='stylesheet' id='wp-block-library-css' href='{{ asset('site/assets/css/style.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='wp-block-library-css' href='{{ asset('site/assets/css/custom.css') }}' type='text/css'
      media='all' />
   @include('site.layouts.css')
   <link rel='stylesheet' id='contact-form-7-css' href='{{ asset('site/plugins/contact-form-7/includes/css/styles42f0.css') }}'
      type='text/css' media='all' />
   <link rel='stylesheet' id='preloader-plus-css'
      href='{{ asset('site/plugins/preloader-plus/assets/css/preloader-plus.min77e6.css') }}' type='text/css' media='all' />

   <link rel='stylesheet' id='hfe-style-css'
      href='{{ asset('site/plugins/header-footer-elementor/assets/css/header-footer-elementor1f62.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='elementor-icons-css'
      href='{{ asset('site/plugins/elementor/assets/lib/eicons/css/elementor-icons.min91ce.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='elementor-frontend-legacy-css'
      href='{{ asset('site/plugins/elementor/assets/css/frontend-legacy.minf3df.css') }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-frontend-css'
      href='{{ asset('site/plugins/elementor/assets/css/frontend.minf3df.css') }}' type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-post-8-css' href='{{ asset('site/uploads/elementor/css/post-8d2e5.css') }}'
      type='text/css' media='all' />
   <link rel='stylesheet' id='widget-css-css' href='{{ asset('site/widgets/css/widgets-style4963.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='font-awesome-5-all-css'
      href='{{ asset('site/plugins/elementor/assets/lib/font-awesome/css/all.minf3df.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='font-awesome-4-shim-css'
      href='{{ asset('site/plugins/elementor/assets/lib/font-awesome/css/v4-shims.minf3df.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='elementor-post-6194-css' href='{{ asset('site/uploads/elementor/css/post-619411a3.css') }}'
      type='text/css' media='all' />
   <link rel='stylesheet' id='hfe-widgets-style-css'
      href='{{ asset('site/plugins/header-footer-elementor/inc/widgets-css/frontend1f62.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='megamenu-css' href='{{ asset('site/uploads/maxmegamenu/style1a0c.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='dashicons-css' href='{{ asset('site/assets/css/dashicons.min5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='IstokWeb-css'
      href='https://fonts.googleapis.com/css2?family=Istok+Web%3Aital%2Cwght%400%2C400%3B0%2C700%3B1%2C400%3B1%2C700&amp;display=swap&amp;ver=6.0.2'
      type='text/css' media='all' />
   <link rel='stylesheet' id='bootstrap-css' href='{{ asset('site/assets/css/bootstrap.min5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='itsoft-fonts-css'
      href='https://fonts.googleapis.com/css?family=Fira+Sans%3A300%2C400%2C500%2C600%2C700%2C800%2C900%7CRubik%3A300%2C400%2C500%2C600%2C700%2C800%2C900&amp;subset=latin%2Clatin-ext&amp;ver=6.0.2'
      type='text/css' media='all' />
   <link rel='stylesheet' id='venobox-css' href='{{ asset('site/venobox/venobox5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='nivo-css' href='{{ asset('site/assets/css/nivo-slider5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='animate-css' href='{{ asset('site/assets/css/animate5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='slick-css' href='{{ asset('site/assets/css/slick5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='owl-carousel-css' href='{{ asset('site/assets/css/owl.carousel5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='owl-transitions-css' href='{{ asset('site/assets/css/owl.transitions5b21.css') }}'
      type='text/css' media='all' />
   <link rel='stylesheet' id='fontawesome-css' href='{{ asset('site/assets/css/font-awesome.min5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='meanmenu-css' href='{{ asset('site/assets/css/meanmenu.min5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='itsoft-theme-default-css' href='{{ asset('site/assets/css/theme-default5b21.css') }}'
      type='text/css' media='all' />
   <link rel='stylesheet' id='itsoft-widget-css' href='{{ asset('site/assets/css/widget5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='itsoft-unittest-css' href='{{ asset('site/assets/css/unittest5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='itsoft-style-css' href='{{ asset('site/assets/css/theme-default5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='itsoft-responsive-css' href='{{ asset('site/assets/css/responsive5b21.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='itsoft-breadcrumb-css' href='{{ asset('site/assets/css/em-breadcrumb5b21.css') }}'
      type='text/css' media='all' />
   <style id='itsoft-breadcrumb-inline-css' type='text/css'>
      .logo img {
         ;
         ;
      }

      .logo a {
         margin-top: 0px
      }

      .mean-container .mean-bar::before {
         content: "Miracle Digital Media"
      }
   </style>
   <link rel='stylesheet' id='google-fonts-1-css'
      href='../../external.html?link=https://fonts.googleapis.com/css?family=Fira+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRubik%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&amp;display=auto&amp;ver=6.0.2'
      type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-icons-shared-0-css'
      href='{{ asset('site/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min52d5.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='elementor-icons-fa-solid-css'
      href='{{ asset('site/plugins/elementor/assets/lib/font-awesome/css/solid.min52d5.css') }}' type='text/css'
      media='all' />
   <link rel='stylesheet' id='elementor-icons-restly-flaticon-css' href='{{ asset('site/assets/css/flaticon8a54.css') }}'
      type='text/css' media='all' />
   <link rel='stylesheet' id='elementor-icons-fa-regular-css'
      href='{{ asset('site/plugins/elementor/assets/lib/font-awesome/css/regular.min52d5.css') }}' type='text/css'
      media='all' />
   <script type='text/javascript' src='{{ asset('site/assets/js/jquery/jquery.minaf6c.js') }}' id='jquery-core-js'></script>
   <script type='text/javascript' src='{{ asset('site/assets/js/jquery/jquery-migrate.mind617.js') }}' id='jquery-migrate-js'>
   </script>
   <script type='text/javascript' id='preloader-plus-js-extra'>
      /* <![CDATA[ */
      var preloader_plus = {
         "animation_delay": "500",
         "animation_duration": "1000"
      };
      /* ]]> */
   </script>
   <script type='text/javascript' src='{{ asset('site/plugins/preloader-plus/assets/js/preloader-plus.min77e6.js') }}'
      id='preloader-plus-js'></script>
   <script type='text/javascript' src='{{ asset('site/plugins/elementor/assets/lib/font-awesome/js/v4-shims.minf3df.js') }}'
      id='font-awesome-4-shim-js'></script>
   {{-- <link rel="https://api.w.org/" href="../wp-json/index.html" />
   <link rel="alternate" type="application/json" href="../wp-json/wp/v2/pages/6194.json" />
   <link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc0db0.html" />
   <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="../wp-includes/wlwmanifest.xml" />
   <meta name="generator" content="WordPress 6.0.2" />
   <link rel="canonical" href="index.html" />
   <link rel='shortlink' href='../indexac60.html' />
   <link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embed1d05.json" />
   <link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embed898b" />
   <meta name="generator" content="Redux 4.3.17" />
   <meta name="generator"
      content="Powered by Slider Revolution 6.5.7 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
   <link rel="icon" href="../wp-content/uploads/2020/09/cropped-main-logo-32x32.png" sizes="32x32" />
   <link rel="icon" href="../wp-content/uploads/2020/09/cropped-main-logo-192x192.png" sizes="192x192" />
   <link rel="apple-touch-icon" href="../wp-content/uploads/2020/09/cropped-main-logo-180x180.png" />
   <meta name="msapplication-TileImage"
      content="https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/cropped-main-logo-270x270.png" /> --}}
   <script type="text/javascript">
      function setREVStartSize(e) {
         //window.requestAnimationFrame(function() {
         window.RSIW = window.RSIW === undefined ? window.innerWidth : window.RSIW;
         window.RSIH = window.RSIH === undefined ? window.innerHeight : window.RSIH;
         try {
            var pw = document.getElementById(e.c).parentNode.offsetWidth,
               newh;
            pw = pw === 0 || isNaN(pw) ? window.RSIW : pw;
            e.tabw = e.tabw === undefined ? 0 : parseInt(e.tabw);
            e.thumbw = e.thumbw === undefined ? 0 : parseInt(e.thumbw);
            e.tabh = e.tabh === undefined ? 0 : parseInt(e.tabh);
            e.thumbh = e.thumbh === undefined ? 0 : parseInt(e.thumbh);
            e.tabhide = e.tabhide === undefined ? 0 : parseInt(e.tabhide);
            e.thumbhide = e.thumbhide === undefined ? 0 : parseInt(e.thumbhide);
            e.mh = e.mh === undefined || e.mh == "" || e.mh === "auto" ? 0 : parseInt(e.mh, 0);
            if (e.layout === "fullscreen" || e.l === "fullscreen")
               newh = Math.max(e.mh, window.RSIH);
            else {
               e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
               for (var i in e.rl)
                  if (e.gw[i] === undefined || e.gw[i] === 0) e.gw[i] = e.gw[i - 1];
               e.gh = e.el === undefined || e.el === "" || (Array.isArray(e.el) && e.el.length == 0) ? e.gh : e.el;
               e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
               for (var i in e.rl)
                  if (e.gh[i] === undefined || e.gh[i] === 0) e.gh[i] = e.gh[i - 1];

               var nl = new Array(e.rl.length),
                  ix = 0,
                  sl;
               e.tabw = e.tabhide >= pw ? 0 : e.tabw;
               e.thumbw = e.thumbhide >= pw ? 0 : e.thumbw;
               e.tabh = e.tabhide >= pw ? 0 : e.tabh;
               e.thumbh = e.thumbhide >= pw ? 0 : e.thumbh;
               for (var i in e.rl) nl[i] = e.rl[i] < window.RSIW ? 0 : e.rl[i];
               sl = nl[0];
               for (var i in nl)
                  if (sl > nl[i] && nl[i] > 0) {
                     sl = nl[i];
                     ix = i;
                  }
               var m = pw > (e.gw[ix] + e.tabw + e.thumbw) ? 1 : (pw - (e.tabw + e.thumbw)) / (e.gw[ix]);
               newh = (e.gh[ix] * m) + (e.tabh + e.thumbh);
            }
            var el = document.getElementById(e.c);
            if (el !== null && el) el.style.height = newh + "px";
            el = document.getElementById(e.c + "_wrapper");
            if (el !== null && el) {
               el.style.height = newh + "px";
               el.style.display = "block";
            }
         } catch (e) {
            console.log("Failure at Presize of Slider:" + e)
         }
         //});
      };
   </script>
   <style id="itsoft_opt-dynamic-css" title="dynamic-css" class="redux-options-output">
      body,
      p {
         font-display: swap;
      }

      h1,
      h2,
      h3,
      h4,
      h5,
      h6 {
         font-display: swap;
      }

      h1 {
         font-display: swap;
      }

      h2 {
         font-display: swap;
      }

      h3 {
         font-display: swap;
      }

      h4 {
         font-display: swap;
      }

      h5 {
         font-display: swap;
      }

      h6 {
         font-display: swap;
      }

      .itsoft_menu>ul>li>a,
      .heading_style_2 .itsoft_menu>ul>li>a,
      .heading_style_3 .itsoft_menu>ul>li>a,
      .heading_style_4 .itsoft_menu>ul>li>a,
      .heading_style_3.tr_btn .itsoft_menu>ul>li>a,
      .heading_style_3.tr_white_btn .itsoft_menu>ul>li>a,
      .heading_style_5 .itsoft_menu>ul>li>a {
         font-display: swap;
      }

      .itsoft_nav_area.prefix,
      .hbg2 {
         background-color: #ffffff;
      }

      .itsoft_menu ul .sub-menu li a {
         font-display: swap;
      }

      .breadcumb-area,
      .breadcumb-blog-area {
         background-repeat: no-repeat;
         background-attachment: scroll;
         background-position: center center;
         background-image: url('../wp-content/uploads/2020/10/brd-img.jpg');
         background-size: cover;
      }

      .brpt h2,
      .breadcumb-inner h2 {
         color: #ffffff;
      }

      .breadcumb-inner ul,
      .breadcumb-inner li,
      .breadcumb-inner li a {
         color: #ffffff;
         font-display: swap;
      }

      .breadcumb-inner li:nth-last-child(-n+1) {
         color: #ffffff;
      }

      .single-video::before {
         background-color: rgba(0, 0, 0, 0.3);
      }
   </style>
   <style type="text/css">
      /** Mega Menu CSS: fs **/
   </style>
   @stack('style')


</head>
