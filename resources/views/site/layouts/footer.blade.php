<div class="footer-middle">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 col-md-6 col-lg-3 ">
            <div id="about_us-widget-2" class="widget about_us">
               <h2 class="widget-title"> </h2> <!-- About Widget -->
               <div class="about-footer">
                  <div class="footer-widget address">
                     <div class="footer-logo">
                        <img src="{{ asset($sitedetail->logo) }}" alt="{{ $sitedetail->title ?? 'Miracle Digital Media' }}">

                        <p>{{ $sitedetail->footer_content ?? '' }}</p>
                     </div>
                     <div class="footer-address">
                        <div class="footer_s_inner">
                           <div class="footer-sociala-icon">
                              <i class="fa fa-home"></i>
                           </div>
                           <div class="footer-sociala-info">
                              <p> </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </div>
            <div id="nav_menu-2" class="widget widget_nav_menu">
               <div class="menu-social-menu-container">
                  <ul id="menu-social-menu" class="menu">
                     <li id="menu-item-1213" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1213"><a
                           href="{{ $sitedetail->facebook ?? '' }}" target="__blank"><i class="fa fa-facebook"></i></a></li>
                     <li id="menu-item-1214" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1214"><a
                           href="{{ $sitedetail->twitter ?? '' }}" target="__blank"><i class="fa fa-twitter"></i></a></li>
                     <li id="menu-item-1216" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1216"><a
                           href="{{ $sitedetail->linkedin ?? '' }}" target="__blank"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
               </div>
            </div>
         </div>

         <div class="col-sm-12 col-md-6 col-lg-3 ">
            <div id="nav_menu-3" class="widget widget_nav_menu">
               <h2 class="widget-title">Help Links</h2>
               <div class="menu-help-link-container">
                  <ul id="menu-help-link" class="menu">
                     <li id="menu-item-1220" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1220"><a
                           href="#">Customers Services</a></li>
                     <li id="menu-item-1221" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1221"><a
                           href="#">IT Department</a></li>
                     <li id="menu-item-1222" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1222"><a
                           href="#">About Our Company</a></li>
                     <li id="menu-item-1223" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1223"><a
                           href="#">Business Growth</a></li>
                     <li id="menu-item-1224" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1224"><a
                           href="#">Make An Appointment</a></li>
                  </ul>
               </div>
            </div>
         </div>

         <div class="col-sm-12 col-md-6 col-lg-3 ">
            <div id="about_us-widget-3" class="widget about_us">
               <h2 class="widget-title">Contact Information</h2> <!-- About Widget -->
               <div class="about-footer">
                  <div class="footer-widget address">
                     <div class="footer-logo">
                     </div>
                     <div class="footer-address">
                        <div class="footer_s_inner">
                           <div class="footer-sociala-icon">
                              <i class="fa fa-home"></i>
                           </div>
                           <div class="footer-sociala-info">
                              <p><b>Adress:</b> {{ $sitedetail->address ?? '' }}</p>
                           </div>
                        </div>
                        <div class="footer_s_inner">
                           <div class="footer-sociala-icon">
                              <i class="fa fa-phone"></i>
                           </div>
                           <div class="footer-sociala-info">
                              <p><b>Phone:</b><a href="tel::{{ $sitedetail->phone_no ?? '' }}">
                                    {{ $sitedetail->phone_no ?? '' }}</a>
                                 <br> <b>Mobile:</b>
                                 <a href="tel:{{ $sitedetail->mobile_no ?? '' }}">
                                    {{ $sitedetail->mobile_no ?? '' }}</a>
                              </p>
                           </div>
                        </div>

                        <div class="footer_s_inner">
                           <div class="footer-sociala-icon">
                              <i class="fa fa-globe"></i>
                           </div>
                           <div class="footer-sociala-info">
                              <p><b>Email:</b> <a href="mailto:{{ $sitedetail->email }}"> {{ $sitedetail->email ?? '' }}
                                 </a><b>Website:</b> miracledigitalmedia.com</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </div>
         </div>

         <div class="col-sm-12 col-md-6 col-lg-3 last">
            <div id="em_recent_post_widget-2" class="widget widget_recent_data">
               <div class="single-widget-item">
                  <h2 class="widget-title">Recent Posts</h2>
                  <div class="recent-post-item">
                     <div class="recent-post-image">
                        <a href="../how-to-make-website-with-elementor/index.html">

                           <img width="80" height="80" src="../wp-content/uploads/2020/09/blog3-80x80.jpg"
                              class="attachment-itsoft-recent-image size-itsoft-recent-image wp-post-image" alt=""
                              loading="lazy"
                              srcset="https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/blog3-80x80.jpg 80w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/blog3-150x150.jpg 150w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/blog3-450x450.jpg 450w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/blog3-106x106.jpg 106w"
                              sizes="(max-width: 80px) 100vw, 80px" />
                        </a>

                     </div>
                     <div class="recent-post-text">
                        <h4><a href="../how-to-make-website-with-elementor/index.html">
                              How to Make Website With Elementor?
                           </a></h4>
                        <span class="rcomment">October 22, 2020</span>
                     </div>
                  </div>

                  <div class="recent-post-item">
                     <div class="recent-post-image">
                        <a href="../the-next-big-challenge-for-content-marketer/index.html">

                           <img width="80" height="80" src="../wp-content/uploads/2020/09/blog2-1-80x80.jpg"
                              class="attachment-itsoft-recent-image size-itsoft-recent-image wp-post-image" alt=""
                              loading="lazy"
                              srcset="https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/blog2-1-80x80.jpg 80w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/blog2-1-150x150.jpg 150w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/blog2-1-450x450.jpg 450w, https://itsoft.dreamitsolution.net/wp-content/uploads/2020/09/blog2-1-106x106.jpg 106w"
                              sizes="(max-width: 80px) 100vw, 80px" />
                        </a>

                     </div>
                     <div class="recent-post-text">
                        <h4><a href="../the-next-big-challenge-for-content-marketer/index.html">
                              The Next Big Challenge for Content Marketer
                           </a></h4>
                        <span class="rcomment">September 24, 2020</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
</div>

<!-- FOOTER COPPYRIGHT SECTION -->

<div class="footer-bottom">
   <div class="container">
      <div class="row">
         <div class="col-md-6  col-sm-12">
            <div class="copy-right-text">
               <!-- FOOTER COPYRIGHT TEXT -->
               <p>
                  Copyright &copy; {{ date('Y') }} {{ $sitedetail->title ?? '' }} </p>

            </div>
         </div>
         <div class="col-md-6  col-sm-12">
            <div class="footer-menu">
               <!-- FOOTER COPYRIGHT MENU -->
               <ul id="menu-footer-menu" class="text-right">
                  <li id="menu-item-1225" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1225"><a
                        href="#">About Us</a></li>
                  <li id="menu-item-1226" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1226"><a
                        href="#">Privacy Policy</a></li>
                  <li id="menu-item-1227" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1227"><a
                        href="#">Services</a></li>
               </ul>
            </div>
         </div>
         <!-- FOOTER COPYRIGHT STYLE 3 -->

      </div>
   </div>
</div>
<!-- DEFAULT STYLE IF NOT ACTIVE THEME OPTION  -->
