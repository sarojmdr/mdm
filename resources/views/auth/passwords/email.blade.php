@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', $sitedetail->logo)
@section('fb_url', Request::url())
@section('title', $page_header)
@section('content')
<main>
  <section class="main__content">
    <div class="login__wrap">
      <div class="container">
        <div class="row">
            <div class="col* col-md-8 col-lg-6 col-xl-5 mid__card">
                @if (Session::has('message'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
                @endif
                @if (Session::has('success_message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('success_message') }}
                </div>
                @endif
            </div>
        </div>
        <div class="row">
          <div class="col* col-md-8 col-lg-6 col-xl-5 mid__card">
            <div class="login__tabs">
              <h2 class="login__head">{{ $page_header }}</h2>
              <form action="{{ route('password.email') }}" method="POST">
                @csrf
                <div class="form-group">
                  <label class="login__title">Email <span class="astrick">*</span></label>
                  <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="Enter your Email here">
                  @if ($errors->has('email'))
                      <span class="text-danger">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>                
                <div class="form-group">
                  <button type="submit" value="" class="btn btn-1">Submit</button>
                </div>
                
                <div class="form-group">
                  <p><a href="{{ route('login') }}">Back to Login</a></p>
                </div>                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
@endsection