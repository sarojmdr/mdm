@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', $sitedetail->logo)
@section('fb_url', Request::url())
@section('title', $page_header)
@section('content')
<main>
  <section class="main__content">
    <div class="login__wrap">
      <div class="container">
        <div class="row">
            <div class="col* col-md-8 col-lg-6 col-xl-5 mid__card">
                @if (Session::has('message'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
                @endif
                @if (Session::has('success_message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('success_message') }}
                </div>
                @endif
            </div>
        </div>
        <div class="row">
          <div class="col* col-md-8 col-lg-6 col-xl-5 mid__card">
            <div class="login__tabs">
              <h2 class="login__head">Sign in</h2>
              <form action="{{ route('login') }}" method="POST">
                @csrf
                <div class="form-group">
                  <label class="login__title">Email <span class="astrick">*</span></label>
                  <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="">
                  @if ($errors->has('email'))
                      <span class="text-danger">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                
                <div class="form-group">
                  <label class="login__title">Password <span class="astrick">*</span></label>
                  <div class="password__eye">
                    <input id="password-field" type="password" name="password" value="" class="form-control password-field" placeholder="">
                    <span toggle=".password-field" class="ri-eye-off-line field-icon toggle-password"></span>
                    @if ($errors->has('password'))
                        <span class="text-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="login__flex">
                    {{-- <div class="loginLeft__flex">
                      <div class="custom__control custom-control-inline">
                        <label class="ckbox">
                          <input type="checkbox">
                          <span> Remember Me</span>
                        </label>
                      </div>
                    </div> --}}
                    <div class="loginRight__flex">
                      <a href="{{ route('password.request') }}">Forget Password ?</a>
                    </div>
                  </div>
                  
                </div>
                
                <div class="form-group">
                  <button type="submit" value="" class="btn btn-1">Log In</button>
                </div>
                
                <div class="form-group">
                  <p>Are you a New Member ? <a href="{{ route('register') }}">Register Here</a></p>
                </div>
                
                
                {{-- <p class="my-4 text-muted">--- Or connect with ---</p>
                
                <div class="login__social___buttons">
                  <a href="#" id="login-with-facebook" data-network="Facebook" class="social__account__button facebook__button">
                    <i class="fab fa-facebook-f"></i>
                    <span> Sign In with Facebook</span>
                  </a>
                  
                  <a href="#"  id="login-with-google" data-network="google" class="social__account__button google__button">
                    <i class="fab fa-google"></i>
                    <span> Sign In with Google</span>
                  </a>
                </div> --}}
                
              </form>
              
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
@endsection