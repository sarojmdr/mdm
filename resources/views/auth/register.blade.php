@extends('site.master')
@section('description', $sitedetail->meta_descriptions)
@section('keywords', $sitedetail->meta_keywords)
@section('fb_image', $sitedetail->logo)
@section('fb_url', Request::url())
@section('title', $page_header)
@section('content')
<section class="main__content">
  <div class="login__wrap">
    <div class="container">
      <div class="row">
        <div class="col* col-md-8 col-lg-6 col-xl-5 mid__card">
          <div class="login__tabs">
            <h2 class="login__head">Create Account</h2>
            <form action="{{ route('auth.register') }}" method="POST">
              @csrf
              <div class="form-group">
                <label class="login__title">Username <span class="astrick">*</span></label>
                <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Enter your name here">
                @if($errors->has('name'))
                  <span class="text-danger">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
                @endif
              </div>
              <div class="form-group">
                <label class="login__title">Email <span class="astrick">*</span></label>
                <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="Enter valid email here">
              @if ($errors->has('email'))
                  <span class="text-danger">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
              </div>
              <div class="form-group">
                <label class="login__title">Mobile No. <span class="astrick">*</span></label>
                <input type="text" name="mobileno" value="{{ old('mobileno') }}" class="form-control" placeholder="Enter your mobile number here">
                @if($errors->has('mobileno'))
                  <span class="text-danger">
                      <strong>{{ $errors->first('mobileno') }}</strong>
                  </span>
                @endif
              </div>
              <div class="form-group">
                <label class="login__title">Address <span class="astrick">*</span></label>
                <input type="text" name="address" value="{{ old('address') }}" class="form-control" placeholder="Enter your address here">
                @if($errors->has('address'))
                  <span class="text-danger">
                      <strong>{{ $errors->first('address') }}</strong>
                  </span>
                @endif
              </div>
              
              <div class="form-group">
                <label class="login__title">Password <span class="astrick">*</span></label>
                <div class="password__eye">
                  <input id="" type="password" name="password" value="" class="form-control password-field" placeholder="Enter Password">
                  <span toggle=".password-field" class="far fa-eye-slash field-icon toggle-password"></span>
                  @if($errors->has('password'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                <label class="login__title">Confirm Password <span class="astrick">*</span></label>
                <div class="password__eye">
                  <input id="" type="password" name="password_confirmation" value="" class="form-control password-field" placeholder="Enter Password for confirmation">
                  <span toggle=".password-field" class="far fa-eye-slash field-icon toggle-password"></span>
                  @if($errors->has('password_confirmation'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              
              <div class="form-group">
                <div class="login__flex">
                  <div class="loginLeft__flex">
                    <div class="custom__control custom-control-inline">
                      <label class="ckbox">
                        <input type="checkbox" name="terms_of_service">
                        <span>Accept the Terms of Service</span>
                      </label>
                      @if($errors->has('terms_of_service'))
                        <span class="text-danger">
                            <strong>{{ $errors->first('terms_of_service') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                  
                </div>
                
              </div>
              
              <div class="form-group">
                <button type="submit" value="" class="btn btn-1">Register</button>
              </div>
              
              <div class="form-group">
                <p>Go to <a href="{{ route('login') }}">Login</a></p>
              </div>
              
              {{-- <p class="my-4 text-muted">--- Or connect with ---</p>
              
              <div class="login__social___buttons">
                <a href="#" id="login-with-facebook" data-network="Facebook" class="social__account__button facebook__button">
                  <i class="fab fa-facebook-f"></i>
                  <span> Sign In with Facebook</span>
                </a>
                
                <a href="#"  id="login-with-google" data-network="google" class="social__account__button google__button">
                  <i class="fab fa-google"></i>
                  <span> Sign In with Google</span>
                </a>
              </div> --}}
              
            </form>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection